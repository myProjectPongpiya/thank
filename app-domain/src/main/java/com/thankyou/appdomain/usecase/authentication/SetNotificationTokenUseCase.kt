package com.thankyou.appdomain.usecase.authentication

import com.thankyou.appdata.repository.api.authentication.AuthenticationApi
import com.thankyou.appdata.repository.response.authentication.NotificationTokenRequest
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class SetNotificationTokenUseCase(
    private val repository: AuthenticationApi
) : BaseUseCase<NotificationTokenRequest, Unit>() {
    override fun onBuild(params: NotificationTokenRequest): Flow<Unit> {
        return flowSingle { repository.notificationToken(params) }
    }
}