package com.thankyou.appdomain.usecase.authentication

import com.thankyou.appdata.repository.api.authentication.AuthenticationApi
import com.thankyou.appdata.repository.response.authentication.PinCodeRequest
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class SetPinCodeUseCase(
    private val repository: AuthenticationApi
) : BaseUseCase<PinCodeRequest, Unit>() {
    override fun onBuild(params: PinCodeRequest): Flow<Unit> {
        return flowSingle { repository.setPinCode(params) }
    }
}