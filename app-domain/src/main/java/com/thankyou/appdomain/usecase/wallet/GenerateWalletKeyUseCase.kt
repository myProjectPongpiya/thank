package com.thankyou.appdomain.usecase.wallet

import com.thankyou.appdata.repository.api.wallet.WalletInvokeApi
import com.thankyou.appdata.repository.response.wallet.GenerateWalletResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GenerateWalletKeyUseCase(
    private val repository: WalletInvokeApi
) : BaseUseCase<Unit, GenerateWalletResponse>() {
    override fun onBuild(params: Unit): Flow<GenerateWalletResponse> {
        return flowSingle { repository.generateWalletKey() }
    }
}