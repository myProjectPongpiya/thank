package com.thankyou.appdomain.usecase.authentication

import com.thankyou.appdata.repository.api.authentication.AuthenticationApi
import com.thankyou.appdata.repository.response.authentication.ChangePinCodeRequest
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class ChangePinCodeUseCase(
    private val repository: AuthenticationApi
) : BaseUseCase<ChangePinCodeRequest, Unit>() {
    override fun onBuild(params: ChangePinCodeRequest): Flow<Unit> {
        return flowSingle { repository.changePinCode(params) }
    }
}