package com.thankyou.appdomain.usecase.state

import com.thankyou.appdata.repository.api.state.StateApi
import com.thankyou.appdata.repository.response.state.SetStateRequest
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class SetStateUseCase(
    private val repository: StateApi
) : BaseUseCase<SetStateRequest, Unit>() {
    override fun onBuild(params: SetStateRequest): Flow<Unit> {
        return flowSingle { repository.setState(params) }
    }
}