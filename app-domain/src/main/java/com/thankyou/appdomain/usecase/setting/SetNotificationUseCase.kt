package com.thankyou.appdomain.usecase.setting

import com.thankyou.appdata.repository.api.setting.SettingApi
import com.thankyou.appdata.repository.response.setting.NotificationRequest
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class SetNotificationUseCase(
    private val repository: SettingApi
) : BaseUseCase<NotificationRequest, Unit>() {
    override fun onBuild(params: NotificationRequest): Flow<Unit> {
        return flowSingle { repository.setNotification(params) }
    }
}