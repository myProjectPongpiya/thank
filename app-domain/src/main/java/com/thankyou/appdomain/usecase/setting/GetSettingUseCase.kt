package com.thankyou.appdomain.usecase.setting

import com.thankyou.appdata.repository.api.setting.SettingApi
import com.thankyou.appdata.repository.response.setting.SettingResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetSettingUseCase(
    private val repository: SettingApi
) : BaseUseCase<Unit, SettingResponse>() {
    override fun onBuild(params: Unit): Flow<SettingResponse> {
        return flowSingle { repository.getSetting() }
    }
}