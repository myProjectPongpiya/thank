package com.thankyou.appdomain.usecase.authentication

import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.data.AppPersistentSession
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf

class SaveBiometricsUuidUseCase(
  private val persistentSession: AppPersistentSession
) : BaseUseCase<SaveBiometricsUuidUseCase.Params, String?>() {
    override fun onBuild(params: Params): Flow<String?> {
        persistentSession.encryptedBiometricUuid = params.encryptedBiometricsUuid
        return flowOf(params.encryptedBiometricsUuid)
    }

    data class Params(val encryptedBiometricsUuid: String?)
}
