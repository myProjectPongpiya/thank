package com.thankyou.appdomain.usecase.authentication

import com.thankyou.appdata.repository.api.authentication.AuthenticationApi
import com.thankyou.appdata.repository.response.authentication.ForgotPinCodeRequest
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class ForgotPinCodeUseCase(
    private val repository: AuthenticationApi
) : BaseUseCase<ForgotPinCodeRequest, Unit>() {
    override fun onBuild(params: ForgotPinCodeRequest): Flow<Unit> {
        return flowSingle { repository.forgotPinCode(params) }
    }
}