package com.thankyou.appdomain.usecase.wallet

import com.thankyou.appdata.repository.api.wallet.WalletInvokeApi
import com.thankyou.appdata.repository.response.wallet.GetWalletInvitationForUpdateRequest
import com.thankyou.appdata.repository.response.wallet.GetWalletInvitationResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetWalletInvitationForUpdateUseCase(
    private val repository: WalletInvokeApi
) : BaseUseCase<GetWalletInvitationForUpdateRequest, GetWalletInvitationResponse>() {
    override fun onBuild(params: GetWalletInvitationForUpdateRequest): Flow<GetWalletInvitationResponse> {
        return flowSingle { repository.getWalletInvitation(params) }
    }
}