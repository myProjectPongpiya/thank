package com.thankyou.appdomain.usecase.dashboard

import com.thankyou.appdata.repository.api.dashboard.DashboardApi
import com.thankyou.appdata.repository.response.dashboard.ContentResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetConsentContentUseCase(
  private val repository: DashboardApi
) : BaseUseCase<Unit, ContentResponse>() {
    override fun onBuild(params: Unit): Flow<ContentResponse> {
        return flowSingle { repository.getConsentContent() }
    }
}
