package com.thankyou.appdomain.usecase.setting

import com.thankyou.appdata.repository.api.setting.SettingApi
import com.thankyou.appdata.repository.response.setting.ContractResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetContractUseCase(
    private val repository: SettingApi
) : BaseUseCase<Unit, ContractResponse>() {
    override fun onBuild(params: Unit): Flow<ContractResponse> {
        return flowSingle { repository.getContract() }
    }
}