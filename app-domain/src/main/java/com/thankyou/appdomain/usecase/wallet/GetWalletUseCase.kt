package com.thankyou.appdomain.usecase.wallet

import com.thankyou.appdata.repository.api.wallet.WalletInvokeApi
import com.thankyou.appdata.repository.response.wallet.GetWalletResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetWalletUseCase(
    private val repository: WalletInvokeApi
) : BaseUseCase<Unit, GetWalletResponse>() {
    override fun onBuild(params: Unit): Flow<GetWalletResponse> {
        return flowSingle { repository.getWallet() }
    }
}