package com.thankyou.appdomain.usecase.authentication

import com.thankyou.appdata.repository.api.authentication.AuthenticationApi
import com.thankyou.appdata.repository.response.authentication.BiometricUuidRequest
import com.thankyou.appdata.repository.response.authentication.BiometricUuidResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetBiometricsUuidUseCase(
  private val repository: AuthenticationApi
) : BaseUseCase<BiometricUuidRequest, BiometricUuidResponse>() {
    override fun onBuild(params: BiometricUuidRequest): Flow<BiometricUuidResponse> {
        return flowSingle { repository.getBiometricUuid(params) }
    }
}
