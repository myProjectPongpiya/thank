package com.thankyou.appdomain.usecase.profile

import com.thankyou.appdata.repository.api.profile.ProfileApi
import com.thankyou.appdata.repository.response.profile.OtpVerifyRequest
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetOtpVerifyUseCase(
    private val repository: ProfileApi
) : BaseUseCase<OtpVerifyRequest, Unit>() {
    override fun onBuild(params: OtpVerifyRequest): Flow<Unit> {
        return flowSingle { repository.getVerifyOtp(params) }
    }
}