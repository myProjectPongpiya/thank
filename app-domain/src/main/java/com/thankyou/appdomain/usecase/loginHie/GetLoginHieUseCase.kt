package com.thankyou.appdomain.usecase.loginHie

import com.thankyou.appdata.repository.api.loginHie.LoginHieApi
import com.thankyou.appdata.repository.response.loginHie.LoginHieRequest
import com.thankyou.appdata.repository.response.loginHie.LoginHieResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetLoginHieUseCase(
  private val repository: LoginHieApi
) : BaseUseCase<LoginHieRequest, LoginHieResponse>() {
    override fun onBuild(params: LoginHieRequest): Flow<LoginHieResponse> {
        return flowSingle { repository.getLoginHie(params) }
    }
}
