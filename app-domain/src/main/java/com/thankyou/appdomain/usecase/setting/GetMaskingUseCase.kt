package com.thankyou.appdomain.usecase.setting

import com.thankyou.appdata.repository.api.setting.SettingApi
import com.thankyou.appdata.repository.model.setting.MaskingModel
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetMaskingUseCase(
    private val repository: SettingApi
) : BaseUseCase<MaskingModel, Unit>() {
    override fun onBuild(params: MaskingModel): Flow<Unit> {
        return flowSingle { repository.setMasking(params) }
    }
}