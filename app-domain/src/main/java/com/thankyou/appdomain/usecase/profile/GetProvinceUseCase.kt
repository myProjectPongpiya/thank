package com.thankyou.appdomain.usecase.profile

import com.thankyou.appdata.repository.api.profile.ProfileApi
import com.thankyou.appdata.repository.response.profile.ProvinceResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetProvinceUseCase(
    private val repository: ProfileApi
) : BaseUseCase<Unit, ProvinceResponse>() {
    override fun onBuild(params: Unit): Flow<ProvinceResponse> {
        return flowSingle { repository.getProvince() }
    }
}