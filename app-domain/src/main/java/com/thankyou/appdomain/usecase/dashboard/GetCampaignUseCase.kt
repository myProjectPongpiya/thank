package com.thankyou.appdomain.usecase.dashboard

import com.thankyou.appdata.repository.api.home.HomeApi
import com.thankyou.appdata.repository.response.home.CampaignResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetCampaignUseCase(
    private val repository: HomeApi
) : BaseUseCase<Unit, CampaignResponse>() {
    override fun onBuild(params: Unit): Flow<CampaignResponse> {
        return flowSingle { repository.getCampaign() }
    }
}