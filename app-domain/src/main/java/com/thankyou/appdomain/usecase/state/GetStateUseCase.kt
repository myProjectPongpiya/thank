package com.thankyou.appdomain.usecase.state

import com.thankyou.appdata.repository.api.state.StateApi
import com.thankyou.appdata.repository.response.state.CheckStateResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetStateUseCase(
    private val repository: StateApi
) : BaseUseCase<Unit, CheckStateResponse>() {
    override fun onBuild(params: Unit): Flow<CheckStateResponse> {
        return flowSingle { repository.getState() }
    }
}