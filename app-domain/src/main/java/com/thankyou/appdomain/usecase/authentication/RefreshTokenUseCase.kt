package com.thankyou.appdomain.usecase.authentication

import com.thankyou.appdata.repository.api.authentication.AuthenticationApi
import com.thankyou.appfoundation.authentication.RefreshTokenRequest
import com.thankyou.appfoundation.authentication.TokenResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class RefreshTokenUseCase(
  private val repository: AuthenticationApi
) : BaseUseCase<RefreshTokenRequest, TokenResponse>() {
    override fun onBuild(params: RefreshTokenRequest): Flow<TokenResponse> {
        return flowSingle { repository.refreshPinToken(params) }
    }
}
