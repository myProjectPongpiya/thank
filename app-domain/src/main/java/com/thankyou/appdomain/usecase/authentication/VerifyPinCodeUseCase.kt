package com.thankyou.appdomain.usecase.authentication

import com.thankyou.appdata.repository.api.authentication.AuthenticationApi
import com.thankyou.appdata.repository.response.authentication.VerifyPinCodeRequest
import com.thankyou.appfoundation.authentication.TokenResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class VerifyPinCodeUseCase(
    private val repository: AuthenticationApi
) : BaseUseCase<VerifyPinCodeRequest, TokenResponse>() {
    override fun onBuild(params: VerifyPinCodeRequest): Flow<TokenResponse> {
        return flowSingle { repository.verifyPinCode(params) }
    }
}
