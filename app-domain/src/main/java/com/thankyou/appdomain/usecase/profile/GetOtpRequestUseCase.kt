package com.thankyou.appdomain.usecase.profile

import com.thankyou.appdata.repository.api.profile.ProfileApi
import com.thankyou.appdata.repository.response.profile.OtpRequest
import com.thankyou.appdata.repository.response.profile.OtpResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetOtpRequestUseCase(
    private val repository: ProfileApi
) : BaseUseCase<OtpRequest, OtpResponse>() {
    override fun onBuild(params: OtpRequest): Flow<OtpResponse> {
        return flowSingle { repository.getRequestOtp(params) }
    }
}