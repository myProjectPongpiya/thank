package com.thankyou.appdomain.usecase.dashboard

import com.thankyou.appdata.repository.api.dashboard.DashboardApi
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class AcceptConsentUseCase(
  private val repository: DashboardApi
) : BaseUseCase<Unit, Unit>() {
    override fun onBuild(params: Unit): Flow<Unit> {
        return flowSingle { repository.acceptConsent() }
    }
}
