package com.thankyou.appdomain.usecase.authentication.token

import com.thankyou.appdomain.usecase.authentication.RefreshTokenUseCase
import com.thankyou.appfoundation.authentication.RefreshTokenRequest
import com.thankyou.appfoundation.authentication.TokenService
import com.thankyou.appfoundation.http.interceptor.TokenRefreshService
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import okhttp3.Request
import org.koin.java.KoinJavaComponent.inject
import java.io.IOException

class TokenRefreshProviderImpl(
    private val tokenService: TokenService
) : TokenRefreshService {

    private val refreshTokenUseCase by inject(RefreshTokenUseCase::class.java)

    @Suppress("FoldInitializerAndIfToElvis")
    override fun refresh(request: Request): Request {
        return runBlocking {
            request.refreshPinTokenUseCase()
        }
    }

    private suspend fun Request.refreshPinTokenUseCase(): Request {
        val refreshToken = tokenService.getToken()?.refreshToken
            ?: throw IOException("No refresh token available.")

        val newToken = refreshTokenUseCase
            .build(RefreshTokenRequest(refreshToken))
            .single()

        return this.newBuilder()
            .header("Authorization", "Bearer ${newToken.content?.accessToken}")
            .build()
    }
}
