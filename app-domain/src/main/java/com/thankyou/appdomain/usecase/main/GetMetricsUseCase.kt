package com.thankyou.appdomain.usecase.main

import com.thankyou.appdata.repository.api.home.HomeApi
import com.thankyou.appdata.repository.response.home.NotificationAlertResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import javax.inject.Inject
import kotlinx.coroutines.flow.Flow

class GetMetricsUseCase @Inject constructor(
    private val metricsApi: HomeApi
) : BaseUseCase<Unit, NotificationAlertResponse>() {
    override fun onBuild(params: Unit): Flow<NotificationAlertResponse> =
        flowSingle { metricsApi.getIsNotification() }
}
