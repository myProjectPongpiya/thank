package com.thankyou.appdomain.usecase.profile

import com.thankyou.appdata.repository.api.profile.ProfileApi
import com.thankyou.appdata.repository.response.profile.PatchDoctorInfoRequest
import com.thankyou.appdata.repository.response.profile.PatchDoctorInfoResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetPatchDoctorInfoUseCase(
    private val repository: ProfileApi
) : BaseUseCase<PatchDoctorInfoRequest, PatchDoctorInfoResponse>() {
    override fun onBuild(params: PatchDoctorInfoRequest): Flow<PatchDoctorInfoResponse> {
        return flowSingle { repository.getPatchDoctorInfo(params) }
    }
}