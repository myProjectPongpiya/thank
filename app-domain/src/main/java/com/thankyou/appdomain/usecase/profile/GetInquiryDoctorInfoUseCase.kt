package com.thankyou.appdomain.usecase.profile

import com.thankyou.appdata.repository.api.profile.ProfileApi
import com.thankyou.appdata.repository.response.profile.InquiryDoctorInfoRequest
import com.thankyou.appdata.repository.response.profile.InquiryDoctorInfoResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetInquiryDoctorInfoUseCase(
    private val repository: ProfileApi
) : BaseUseCase<InquiryDoctorInfoRequest, InquiryDoctorInfoResponse>() {
    override fun onBuild(params: InquiryDoctorInfoRequest): Flow<InquiryDoctorInfoResponse> {
        return flowSingle { repository.getInquiryDoctorInfo(params) }
    }
}