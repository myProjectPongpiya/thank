package com.thankyou.appdomain.usecase.mdcard

import com.thankyou.appdata.repository.api.home.HomeApi
import com.thankyou.appdata.repository.response.home.CreateMdCardRequest
import com.thankyou.appdata.repository.response.home.CreateMdCardResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class CreateMdCardUseCase(
    private val repository: HomeApi
) : BaseUseCase<CreateMdCardRequest, CreateMdCardResponse>() {
    override fun onBuild(params: CreateMdCardRequest): Flow<CreateMdCardResponse> {
        return flowSingle { repository.createMdCard(params) }
    }
}