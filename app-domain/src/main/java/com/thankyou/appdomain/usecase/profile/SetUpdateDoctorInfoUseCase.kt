package com.thankyou.appdomain.usecase.profile

import com.thankyou.appdata.repository.api.profile.ProfileApi
import com.thankyou.appdata.repository.response.profile.SetUpdateDoctorInfoRequest
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class SetUpdateDoctorInfoUseCase(
    private val repository: ProfileApi
) : BaseUseCase<SetUpdateDoctorInfoRequest, Unit>() {
    override fun onBuild(params: SetUpdateDoctorInfoRequest): Flow<Unit> {
        return flowSingle { repository.setUpdateDoctorInfo(params) }
    }
}