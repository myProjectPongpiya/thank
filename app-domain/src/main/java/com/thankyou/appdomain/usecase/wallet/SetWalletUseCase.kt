package com.thankyou.appdomain.usecase.wallet

import com.thankyou.appdata.repository.api.wallet.WalletInvokeApi
import com.thankyou.appdata.repository.response.wallet.SetWalletRequest
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class SetWalletUseCase(
    private val repository: WalletInvokeApi
) : BaseUseCase<SetWalletRequest, Unit>() {
    override fun onBuild(params: SetWalletRequest): Flow<Unit> {
        return flowSingle { repository.setWallet(params) }
    }
}