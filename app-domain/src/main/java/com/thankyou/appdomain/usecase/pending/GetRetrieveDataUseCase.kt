package com.thankyou.appdomain.usecase.pending

import com.thankyou.appdata.repository.api.pending.PendingApi
import com.thankyou.appdata.repository.response.pending.RetrieveDataRequest
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetRetrieveDataUseCase(
    private val repository: PendingApi
) : BaseUseCase<RetrieveDataRequest, Unit>() {
    override fun onBuild(params: RetrieveDataRequest): Flow<Unit> {
        return flowSingle { repository.getRetrieveData(params) }
    }
}