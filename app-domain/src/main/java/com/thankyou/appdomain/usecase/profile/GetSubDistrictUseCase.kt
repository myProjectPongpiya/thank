package com.thankyou.appdomain.usecase.profile

import com.thankyou.appdata.repository.api.profile.ProfileApi
import com.thankyou.appdata.repository.response.profile.SubDistrictRequest
import com.thankyou.appdata.repository.response.profile.SubDistrictResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetSubDistrictUseCase(
  private val repository: ProfileApi
) : BaseUseCase<SubDistrictRequest, SubDistrictResponse>() {
    override fun onBuild(params: SubDistrictRequest): Flow<SubDistrictResponse> {
        return flowSingle { repository.getSubDistrict(params) }
    }
}
