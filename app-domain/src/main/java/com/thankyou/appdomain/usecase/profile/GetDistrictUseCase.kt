package com.thankyou.appdomain.usecase.profile

import com.thankyou.appdata.repository.api.profile.ProfileApi
import com.thankyou.appdata.repository.response.profile.DistrictRequest
import com.thankyou.appdata.repository.response.profile.DistrictResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetDistrictUseCase(
  private val repository: ProfileApi
) : BaseUseCase<DistrictRequest, DistrictResponse>() {
    override fun onBuild(params: DistrictRequest): Flow<DistrictResponse> {
        return flowSingle { repository.getDistrict(params) }
    }
}
