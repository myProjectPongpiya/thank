package com.thankyou.appdomain.usecase.dashboard

import com.thankyou.appdata.repository.api.dashboard.DashboardApi
import com.thankyou.appdata.repository.response.dashboard.MDEServiceRequest
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetDataMDEServiceUseCase(
  private val repository: DashboardApi
) : BaseUseCase<MDEServiceRequest, Unit>() {
    override fun onBuild(params: MDEServiceRequest): Flow<Unit> {
        return flowSingle { repository.getDataMDEService(params) }
    }
}
