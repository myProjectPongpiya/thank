package com.thankyou.appdomain.usecase.main

import com.thankyou.appdata.repository.api.home.HomeApi
import com.thankyou.appdata.repository.response.home.NotificationListRequest
import com.thankyou.appdata.repository.response.home.NotificationListResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetNotificationListUseCase(
    private val repository: HomeApi
) : BaseUseCase<NotificationListRequest, NotificationListResponse>() {
    override fun onBuild(params: NotificationListRequest): Flow<NotificationListResponse> {
        return flowSingle { repository.getNotificationList(params) }
    }
}