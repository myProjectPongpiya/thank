package com.thankyou.appdomain.usecase.authentication

import com.thankyou.appdata.repository.api.authentication.AuthenticationApi
import com.thankyou.appdata.repository.response.authentication.VerifyBiometricsRequest
import com.thankyou.appfoundation.authentication.AppStatus
import com.thankyou.appfoundation.authentication.TokenResponse
import com.thankyou.appfoundation.authentication.TokenResponse.Companion.APP_STATUS
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.data.TransientSession
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.onEach

class LoginBiometricUseCase(
    private val repository: AuthenticationApi,
    session: TransientSession
) : BaseUseCase<VerifyBiometricsRequest, TokenResponse>() {

    private val cache = session.forKey<AppStatus>(APP_STATUS)

    override fun onBuild(params: VerifyBiometricsRequest): Flow<TokenResponse> {
        return flowSingle { repository.loginBiometrics(params) }
            .onEach { cache.set(it.content?.appStatus ?: AppStatus.ACTIVE) }
    }
}
