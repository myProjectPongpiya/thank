package com.thankyou.appdomain.usecase.dashboard

import com.thankyou.appdata.repository.api.dashboard.DashboardApi
import com.thankyou.appdata.repository.response.dashboard.TmcResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetTmcUseCase(
  private val repository: DashboardApi
) : BaseUseCase<Unit, TmcResponse>() {
    override fun onBuild(params: Unit): Flow<TmcResponse> {
        return flowSingle { repository.getTmc() }
    }
}
