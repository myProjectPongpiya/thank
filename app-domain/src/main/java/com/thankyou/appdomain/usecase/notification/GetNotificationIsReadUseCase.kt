package com.thankyou.appdomain.usecase.notification

import com.thankyou.appdata.repository.api.notification.NotificationApi
import com.thankyou.appdata.repository.response.notifications.NotificationIsReadRequest
import com.thankyou.appdata.repository.response.notifications.NotificationIsReadResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetNotificationIsReadUseCase(
    private val repository: NotificationApi
) : BaseUseCase<NotificationIsReadRequest, NotificationIsReadResponse>() {
    override fun onBuild(params: NotificationIsReadRequest): Flow<NotificationIsReadResponse> {
        return flowSingle { repository.getNotificationIsRead(params) }
    }
}