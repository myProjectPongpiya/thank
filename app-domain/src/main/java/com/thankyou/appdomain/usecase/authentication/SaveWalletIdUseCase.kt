package com.thankyou.appdomain.usecase.authentication

import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.data.AppPersistentSession
import com.thankyou.appfoundation.data.DeviceIdStore
import com.thankyou.appfoundation.data.WalletSession
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf

class SaveWalletIdUseCase(
    private val deviceIdStore: DeviceIdStore,
    private val persistentSession: AppPersistentSession,
    private val walletSession: WalletSession
) : BaseUseCase<SaveWalletIdUseCase.Params, String?>() {
    override fun onBuild(params: Params): Flow<String?> {
        persistentSession.doctorId = params.doctorId
        walletSession.walletId = "${params.doctorId}-${deviceIdStore.getDeviceId()}"
        return flowOf(params.doctorId)
    }

    data class Params(val doctorId: String?)
}
