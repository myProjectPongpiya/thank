package com.thankyou.appdomain.usecase.dashboard

import com.thankyou.appdata.repository.api.dashboard.DashboardApi
import com.thankyou.appdata.repository.response.dashboard.StatusResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetStatusUseCase(
  private val repository: DashboardApi
) : BaseUseCase<Unit, StatusResponse>() {
    override fun onBuild(params: Unit): Flow<StatusResponse> {
        return flowSingle { repository.getConsentStatus() }
    }
}
