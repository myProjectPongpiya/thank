package com.thankyou.appdomain.usecase.notification

import com.thankyou.appdata.repository.api.notification.NotificationApi
import com.thankyou.appdata.repository.response.notifications.NotificationIsConfirmRequest
import com.thankyou.appdata.repository.response.notifications.NotificationIsConfirmResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetNotificationIsConfirmUseCase(
    private val repository: NotificationApi
) : BaseUseCase<NotificationIsConfirmRequest, NotificationIsConfirmResponse>() {
    override fun onBuild(params: NotificationIsConfirmRequest): Flow<NotificationIsConfirmResponse> {
        return flowSingle { repository.getNotificationIsConfirm(params) }
    }
}