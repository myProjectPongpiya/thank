package com.thankyou.appdomain.usecase.wallet

import com.thankyou.appdata.repository.api.wallet.WalletInvokeApi
import com.thankyou.appdata.repository.response.wallet.GetWalletInvitationRequest
import com.thankyou.appdata.repository.response.wallet.GetWalletInvitationResponse
import com.thankyou.appfoundation.base.BaseUseCase
import com.thankyou.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetWalletInvitationUseCase(
    private val repository: WalletInvokeApi
) : BaseUseCase<GetWalletInvitationRequest, GetWalletInvitationResponse>() {
    override fun onBuild(params: GetWalletInvitationRequest): Flow<GetWalletInvitationResponse> {
        return flowSingle { repository.getWalletInvitation(params) }
    }
}