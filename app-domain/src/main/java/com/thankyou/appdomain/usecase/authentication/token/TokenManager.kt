package com.thankyou.appdomain.usecase.authentication.token

import com.thankyou.appfoundation.authentication.AuthorizationToken
import com.thankyou.appfoundation.authentication.TokenService

class TokenManager : TokenService {

    private var token: AuthorizationToken? = null

    override fun getToken(): AuthorizationToken? {
        return token
    }

    override fun setToken(token: AuthorizationToken) {
        clear()
        this.token = token
    }

    override fun clear() {
        token = null
    }
}
