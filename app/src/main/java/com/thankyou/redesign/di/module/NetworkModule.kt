package com.thankyou.redesign.di.module

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.google.gson.Gson
import com.thankyou.appdomain.usecase.authentication.token.TokenManager
import com.thankyou.appdomain.usecase.authentication.token.TokenRefreshProviderImpl
import com.thankyou.appfoundation.authentication.TokenService
import com.thankyou.appfoundation.crypto.AESManager
import com.thankyou.appfoundation.crypto.AESService
import com.thankyou.appfoundation.crypto.encryption.EncryptionProviderImp
import com.thankyou.appfoundation.crypto.encryption.EncryptionService
import com.thankyou.appfoundation.crypto.manager.KeyProviderImp
import com.thankyou.appfoundation.crypto.manager.KeyService
import com.thankyou.appfoundation.http.interceptor.CommonExceptionInterceptor
import com.thankyou.appfoundation.http.interceptor.HttpOverrideInterceptor
import com.thankyou.appfoundation.http.interceptor.NoConnectivityInterceptor
import com.thankyou.appfoundation.http.interceptor.CommonHeaderInterceptor
import com.thankyou.appfoundation.http.interceptor.CryptoClientInterceptor
import com.thankyou.appfoundation.http.interceptor.HeaderRequestInterceptor
import com.thankyou.appfoundation.http.interceptor.ExceptionWrapperInterceptor
import com.thankyou.appfoundation.http.interceptor.TokenInjectorInterceptor
import com.thankyou.appfoundation.http.interceptor.TokenRefreshInterceptor
import com.thankyou.appfoundation.http.interceptor.TokenResponseInterceptor
import com.thankyou.appfoundation.http.interceptor.TokenRefreshService
import com.thankyou.appfoundation.trustmanager.UnsafeOkHttpClient
import com.thankyou.redesign.BuildConfig
import com.thankyou.redesign.extentions.addSslPinner
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.net.ssl.X509TrustManager

val networkModule = module {
    factory {
        provideHttpClient(
            androidContext(),
            get(),
            get(),
            get(),
            get(),
            get(),
            get(),
            get(),
            get()
        )
    }

    factory { NoConnectivityInterceptor(get()) }
    factory { HttpOverrideInterceptor() }
    factory { CryptoClientInterceptor(get()) }
    factory { CommonExceptionInterceptor(get()) }
    factory { TokenInjectorInterceptor(get()) }
    factory { TokenResponseInterceptor(get(), get()) }
    factory { TokenRefreshInterceptor(get(), get()) }

    single<TokenService> { TokenManager() }
    single<TokenRefreshService> { TokenRefreshProviderImpl(get()) }
    single { provideRetrofit(get(), get()) }

    single<AESService> { AESManager() }
    single<KeyService> { KeyProviderImp(get(named("publicKeyPEM"))) }
    single<EncryptionService> { EncryptionProviderImp(get(), get()) }
}

fun provideHttpClient(
    context: Context,
    checkForInternetConnectivity: NoConnectivityInterceptor,
    addCommonHeaders: CommonHeaderInterceptor,
    addHeaderRequest: HeaderRequestInterceptor,
    injectToken: TokenInjectorInterceptor,
    storeIncomingTokens: TokenResponseInterceptor,
    checkForExceptions: CommonExceptionInterceptor,
    refreshTokens: TokenRefreshInterceptor,
    cryptoClientInterceptor: CryptoClientInterceptor,
): OkHttpClient {
    val builder = OkHttpClient.Builder()
        .addInterceptor(checkForInternetConnectivity)
        .addInterceptor(addCommonHeaders)
        .addInterceptor(addHeaderRequest)
        .addInterceptor(ExceptionWrapperInterceptor())
        .addInterceptor(injectToken)
        .addInterceptor(storeIncomingTokens)
        .addInterceptor(checkForExceptions)
        .addInterceptor(refreshTokens)
        .addInterceptor(chuckerInterceptor(context))
        .addInterceptor(cryptoClientInterceptor)
        .callTimeout(CONNECTION_TIMEOUT_SECOND, TimeUnit.SECONDS)
        .connectTimeout(CONNECTION_TIMEOUT_SECOND, TimeUnit.SECONDS)
        .readTimeout(CONNECTION_TIMEOUT_SECOND, TimeUnit.SECONDS)
        .writeTimeout(CONNECTION_TIMEOUT_SECOND, TimeUnit.SECONDS)
        .addSslPinner(
            BuildConfig.BASE_API_URL,
            BuildConfig.PIN_ROOT,
            BuildConfig.PIN_INTERMEDIATE,
            BuildConfig.PIN_LEAF
        )

    if (BuildConfig.FLAVOR == "dev") {
        UnsafeOkHttpClient.getSSLSocketFactory()?.let {
            builder.sslSocketFactory(
                it,
                UnsafeOkHttpClient.getTrustManager()[0] as X509TrustManager
            )
        }
        builder.hostnameVerifier { _, _ -> true }
    }

    return builder.build()
}

fun provideRetrofit(httpClient: OkHttpClient, gson: Gson): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_API_URL)
        .client(httpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}

private fun chuckerInterceptor(
    context: Context
): ChuckerInterceptor {
    return ChuckerInterceptor.Builder(context)
        .collector(ChuckerCollector(context))
        .maxContentLength(CHUCKER_MAX_CONTENT_LENGTH)
        .redactHeaders(emptySet())
        .alwaysReadResponseBody(false)
        .build()
}

private const val CONNECTION_TIMEOUT_SECOND = 30.toLong()
private const val CHUCKER_MAX_CONTENT_LENGTH = 250000L
