package com.thankyou.redesign.di.module

import com.thankyou.appdomain.usecase.authentication.ChangePinCodeUseCase
import com.thankyou.appdomain.usecase.authentication.ForgotPinCodeUseCase
import com.thankyou.appdomain.usecase.authentication.GetBiometricsUuidUseCase
import com.thankyou.appdomain.usecase.dashboard.GetCampaignUseCase
import com.thankyou.appdomain.usecase.dashboard.GetTmcUseCase
import com.thankyou.appdomain.usecase.authentication.SaveBiometricsUuidUseCase
import com.thankyou.appdomain.usecase.authentication.RefreshTokenUseCase
import com.thankyou.appdomain.usecase.authentication.SaveWalletIdUseCase
import com.thankyou.appdomain.usecase.authentication.SetNotificationTokenUseCase
import com.thankyou.appdomain.usecase.authentication.SetPinCodeUseCase
import com.thankyou.appdomain.usecase.authentication.LoginBiometricUseCase
import com.thankyou.appdomain.usecase.authentication.LoginPinCodeUseCase
import com.thankyou.appdomain.usecase.authentication.VerifyPinCodeUseCase
import com.thankyou.appdomain.usecase.dashboard.AcceptConsentUseCase
import com.thankyou.appdomain.usecase.dashboard.AcceptTermAndConditionUseCase
import com.thankyou.appdomain.usecase.dashboard.GetConsentContentUseCase
import com.thankyou.appdomain.usecase.dashboard.GetStatusUseCase
import com.thankyou.appdomain.usecase.dashboard.GetDataMDEServiceUseCase
import com.thankyou.appdomain.usecase.dashboard.GetTermAndConditionContentUseCase
import com.thankyou.appdomain.usecase.loginHie.GetLoginHieUseCase
import com.thankyou.appdomain.usecase.main.GetMetricsUseCase
import com.thankyou.appdomain.usecase.main.GetNotificationListUseCase
import com.thankyou.appdomain.usecase.mdcard.CreateMdCardUseCase
import com.thankyou.appdomain.usecase.notification.GetNotificationIsConfirmUseCase
import com.thankyou.appdomain.usecase.notification.GetNotificationIsReadUseCase
import com.thankyou.appdomain.usecase.pending.GetRetrieveDataUseCase
import com.thankyou.appdomain.usecase.profile.GetDistrictUseCase
import com.thankyou.appdomain.usecase.profile.GetInquiryDoctorInfoUseCase
import com.thankyou.appdomain.usecase.profile.GetMaskInquiryDoctorInfoUseCase
import com.thankyou.appdomain.usecase.profile.GetOtpRequestUseCase
import com.thankyou.appdomain.usecase.profile.GetOtpVerifyUseCase
import com.thankyou.appdomain.usecase.profile.GetPatchDoctorInfoUseCase
import com.thankyou.appdomain.usecase.profile.GetProvinceUseCase
import com.thankyou.appdomain.usecase.profile.GetSubDistrictUseCase
import com.thankyou.appdomain.usecase.profile.SetUpdateDoctorInfoUseCase
import com.thankyou.appdomain.usecase.setting.GetContractUseCase
import com.thankyou.appdomain.usecase.setting.GetMaskingUseCase
import com.thankyou.appdomain.usecase.setting.GetSettingUseCase
import com.thankyou.appdomain.usecase.setting.SetNotificationUseCase
import com.thankyou.appdomain.usecase.state.GetStateUseCase
import com.thankyou.appdomain.usecase.state.SetStateUseCase
import com.thankyou.appdomain.usecase.wallet.GetWalletInvitationUseCase
import com.thankyou.appdomain.usecase.wallet.GenerateWalletKeyUseCase
import com.thankyou.appdomain.usecase.wallet.GetWalletInvitationForUpdateUseCase
import com.thankyou.appdomain.usecase.wallet.GetWalletUseCase
import com.thankyou.appdomain.usecase.wallet.SetWalletUseCase
import org.koin.dsl.module

val useCaseModule = module {
    factory { GetTmcUseCase(get()) }
    factory { GetCampaignUseCase(get()) }
    factory { GetMetricsUseCase(get()) }
    factory { GetBiometricsUuidUseCase(get()) }
    factory { SetPinCodeUseCase(get()) }
    factory { LoginBiometricUseCase(get(), get()) }
    factory { LoginPinCodeUseCase(get(), get()) }
    factory { VerifyPinCodeUseCase(get()) }
    factory { RefreshTokenUseCase(get()) }
    factory { GetDistrictUseCase(get()) }
    factory { GetProvinceUseCase(get()) }
    factory { GetSubDistrictUseCase(get()) }
    factory { GetInquiryDoctorInfoUseCase(get()) }
    factory { GetMaskInquiryDoctorInfoUseCase(get()) }
    factory { GetSettingUseCase(get()) }
    factory { SetNotificationUseCase(get()) }
    factory { GetContractUseCase(get()) }
    factory { SetNotificationTokenUseCase(get()) }
    factory { SetStateUseCase(get()) }
    factory { GetStateUseCase(get()) }
    factory { GetDataMDEServiceUseCase(get()) }
    factory { GetRetrieveDataUseCase(get()) }
    factory { GetNotificationListUseCase(get()) }
    factory { GetWalletUseCase(get()) }
    factory { SetWalletUseCase(get()) }
    factory { GenerateWalletKeyUseCase(get()) }
    factory { GetWalletInvitationUseCase(get()) }
    factory { GetWalletInvitationForUpdateUseCase(get()) }
    factory { GetPatchDoctorInfoUseCase(get()) }
    factory { SetUpdateDoctorInfoUseCase(get()) }
    factory { GetNotificationIsReadUseCase(get()) }
    factory { GetNotificationIsConfirmUseCase(get()) }
    factory { SaveBiometricsUuidUseCase(get()) }
    factory { SaveWalletIdUseCase(get(), get(), get()) }
    factory { CreateMdCardUseCase(get()) }
    factory { ForgotPinCodeUseCase(get()) }
    factory { GetOtpRequestUseCase(get()) }
    factory { GetOtpVerifyUseCase(get()) }
    factory { GetLoginHieUseCase(get()) }
    factory { GetMaskingUseCase(get()) }
    factory { GetStatusUseCase(get()) }
    factory { GetTermAndConditionContentUseCase(get()) }
    factory { AcceptTermAndConditionUseCase(get()) }
    factory { GetConsentContentUseCase(get()) }
    factory { AcceptConsentUseCase(get()) }
    factory { ChangePinCodeUseCase(get()) }
}