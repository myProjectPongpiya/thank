package com.thankyou.redesign.di.module

import com.thankyou.appdata.repository.api.authentication.AuthenticationApi
import com.thankyou.appdata.repository.api.dashboard.DashboardApi
import com.thankyou.appdata.repository.api.home.HomeApi
import com.thankyou.appdata.repository.api.notification.NotificationApi
import com.thankyou.appdata.repository.api.pending.PendingApi
import com.thankyou.appdata.repository.api.profile.ProfileApi
import com.thankyou.appdata.repository.api.setting.SettingApi
import com.thankyou.appdata.repository.api.state.StateApi
import com.thankyou.appdata.repository.api.wallet.WalletInvokeApi
import com.thankyou.appdata.repository.api.loginHie.LoginHieApi
import org.koin.dsl.module
import retrofit2.Retrofit

val repositoryModule = module {
    single { provideLoginApi(get()) }
    single { provideDashboardApi(get()) }
    single { provideHomeApi(get()) }
    single { provideLoginHieApi(get()) }
    single { provideProfileApi(get()) }
    single { provideSettingApi(get()) }
    single { provideStateApi(get()) }
    single { provideWalletApi(get()) }
    single { providePendingApi(get()) }
    single { provideNotificationsApi(get()) }
}

fun provideLoginApi(retrofit: Retrofit): AuthenticationApi {
    return retrofit.create(AuthenticationApi::class.java)
}

fun provideDashboardApi(retrofit: Retrofit): DashboardApi {
    return retrofit.create(DashboardApi::class.java)
}

fun provideHomeApi(retrofit: Retrofit): HomeApi {
    return retrofit.create(HomeApi::class.java)
}

fun provideLoginHieApi(retrofit: Retrofit): LoginHieApi {
    return retrofit.create(LoginHieApi::class.java)
}

fun provideProfileApi(retrofit: Retrofit): ProfileApi {
    return retrofit.create(ProfileApi::class.java)
}

fun provideSettingApi(retrofit: Retrofit): SettingApi {
    return retrofit.create(SettingApi::class.java)
}

fun provideStateApi(retrofit: Retrofit): StateApi {
    return retrofit.create(StateApi::class.java)
}

fun provideWalletApi(retrofit: Retrofit): WalletInvokeApi {
    return retrofit.create(WalletInvokeApi::class.java)
}

fun providePendingApi(retrofit: Retrofit): PendingApi {
    return retrofit.create(PendingApi::class.java)
}

fun provideNotificationsApi(retrofit: Retrofit): NotificationApi {
    return retrofit.create(NotificationApi::class.java)
}