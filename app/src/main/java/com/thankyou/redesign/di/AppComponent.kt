package com.thankyou.redesign.di

import com.thankyou.redesign.di.module.coreModule
import com.thankyou.redesign.di.module.networkModule
import com.thankyou.redesign.di.module.repositoryModule
import com.thankyou.redesign.di.module.textModule
import com.thankyou.redesign.di.module.useCaseModule
import com.thankyou.redesign.di.module.viewModelModule

val appComponent = listOf(
    coreModule, textModule, networkModule, repositoryModule, useCaseModule, viewModelModule
)