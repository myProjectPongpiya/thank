package com.thankyou.redesign.di.module

import com.thankyou.redesign.ui.common.confirmNewPin.ConfirmNewPinViewModel
import com.thankyou.redesign.ui.common.newPin.NewPinViewModel
import com.thankyou.redesign.ui.home.HomeViewModel
import com.thankyou.redesign.ui.home.main.MainViewModel
import com.thankyou.redesign.ui.home.profile.ProfileViewModel
import com.thankyou.redesign.ui.splash.SplashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { SplashViewModel(get()) }
    viewModel { HomeViewModel() }
    viewModel { MainViewModel(get()) }
    viewModel { ProfileViewModel() }
    viewModel { NewPinViewModel() }
    viewModel { ConfirmNewPinViewModel() }
}