package com.thankyou.redesign.di.module

import android.content.Context
import com.google.gson.Gson
import com.thankyou.appdata.repository.model.localization.I18NextLanguagePackProvider
import com.thankyou.appdata.repository.model.localization.LanguagePackResource
import com.thankyou.appfoundation.data.CommonContext
import com.thankyou.appfoundation.i18n.LanguagePackProvider
import org.koin.dsl.module

val textModule = module {
    single { provideLanguagePackResource(get(), get()) }
    single { provideI18NextLanguagePackProvider(get(), get(), get()) }
}

fun provideLanguagePackResource(
    context: Context,
    gson: Gson
): LanguagePackResource {
    return LanguagePackResource(context, gson)
}

fun provideI18NextLanguagePackProvider(
    gson: Gson,
    resource: LanguagePackResource,
    commonContext: CommonContext,
): LanguagePackProvider {
    return I18NextLanguagePackProvider(gson, resource, commonContext)
}