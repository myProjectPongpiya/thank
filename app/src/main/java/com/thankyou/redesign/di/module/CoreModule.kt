package com.thankyou.redesign.di.module

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Build
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.RetentionManager
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.thankyou.appfoundation.data.AppPersistentSession
import com.thankyou.appfoundation.data.CommonContext
import com.thankyou.appfoundation.data.DeviceIdStore
import com.thankyou.appfoundation.data.TransientSession
import com.thankyou.appfoundation.data.WalletSession
import com.thankyou.appfoundation.gson.LocalDateDeserializer
import com.thankyou.appfoundation.gson.LocalDateSerializer
import com.thankyou.appfoundation.gson.ZonedDateTimeDeserializer
import com.thankyou.appfoundation.gson.ZonedDateTimeSerializer
import com.thankyou.appfoundation.http.interceptor.CommonHeaderInterceptor
import com.thankyou.appfoundation.http.interceptor.HeaderRequestInterceptor
import com.thankyou.appfoundation.lib.eventbus.EventBus
import com.thankyou.redesign.BuildConfig
import com.thankyou.redesign.ui.AppTimeoutHelper
import com.thankyou.redesign.notificationservice.DrFirebaseMessagingService
import com.thankyou.redesign.notificationservice.DrHmsPushService
import com.thankyou.redesign.notificationservice.FirebaseAlertEventsQueue
import com.thankyou.redesign.notificationservice.FirebaseEventsQueue
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import org.threeten.bp.Clock
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime
import timber.log.Timber
import java.io.File
import java.security.KeyStore
import java.util.concurrent.TimeUnit

val coreModule = module {
    single(qualifier = named("applicationId")) { provideApplicationId() }
    single(qualifier = named("OAuthToken")) { provideOAuthToken() }
    single(qualifier = named("publicKeyPEM")) { providePublicKeyPEM() }
    single { provideGson() }
    single { provideClock() }
    single { providesEventBus() }
    single { providesCommonContext() }
    single { providesAppTimeoutHelper() }
    single { provideChuckerCollector(androidContext()) }
    single { providesAppPersistentSession(get(named("applicationId")), androidContext()) }
    single { providesDeviceIdStore(get(named("applicationId")), androidContext()) }
    single { providesWalletSession(get(named("applicationId")), androidContext()) }
    single { providesTransientSession(get()) }

    single { provideSetAlertChannel() }
    single { provideFirebaseEventQueue() }
    single { provideDrFirebaseMessagingService() }
    single { provideDrHmsPushService() }

    factory { provideCommonHeaderInterceptor() }
    factory { provideHeaderRequestInterceptor(get(), get(named("applicationId")), get()) }
    factory { provideConnectivityManager(androidContext()) }
}

private const val MAX_SESSION_TIMEOUT_MINUTES = 5L

private const val TAG_ENCRYPTED_SHARED_PREF = "ENCRYPTED_SHARED_PREF"
private const val FILE_PERSISTENT = ".persistent_session"
private const val FILE_DEVICE_ID = ".deviceid_store"
private const val FILE_WALLET_PERSISTENT = ".wallet_session"

fun provideApplicationId(): String = BuildConfig.APPLICATION_ID

fun provideOAuthToken(): String = BuildConfig.APPLICATION_ID

fun providePublicKeyPEM(): String = BuildConfig.PUBLIC_KEY_PEM

fun provideClock(): Clock = Clock.system(ZoneId.systemDefault())

fun providesEventBus(): EventBus = EventBus()

fun providesCommonContext(): CommonContext = CommonContext()

fun providesAppTimeoutHelper(): AppTimeoutHelper {
    val maxSessionMinutes =
        MAX_SESSION_TIMEOUT_MINUTES.also {
            Timber.tag("APP_TIMEOUT").w("Using default session timeout duration (5m)")
        }

    return AppTimeoutHelper(
        maxSessionSeconds = TimeUnit.MINUTES.toSeconds(maxSessionMinutes)
    )
}

fun provideGson(): Gson {
    return GsonBuilder()
        .registerTypeAdapter(ZonedDateTime::class.java, ZonedDateTimeSerializer())
        .registerTypeAdapter(ZonedDateTime::class.java, ZonedDateTimeDeserializer())
        .registerTypeAdapter(LocalDate::class.java, LocalDateSerializer())
        .registerTypeAdapter(LocalDate::class.java, LocalDateDeserializer())
        .create()
}

fun provideChuckerCollector(context: Context): ChuckerCollector {
    return ChuckerCollector(
        context = context,
        showNotification = true,
        retentionPeriod = RetentionManager.Period.ONE_HOUR
    )
}

fun providesAppPersistentSession(
    applicationId: String,
    applicationContext: Context
): AppPersistentSession {
    val prefs =
        getPref(applicationContext, applicationId, fileName = "$applicationId$FILE_PERSISTENT")
    return AppPersistentSession(prefs)
}

fun providesDeviceIdStore(
    applicationId: String,
    applicationContext: Context,
): DeviceIdStore {
    val prefs =
        getPref(applicationContext, applicationId, fileName = "$applicationId$FILE_DEVICE_ID")
    return DeviceIdStore(prefs)
}

fun providesWalletSession(
    applicationId: String,
    applicationContext: Context
): WalletSession {
    val prefs =
        getPref(
            applicationContext,
            applicationId,
            fileName = "$applicationId$FILE_WALLET_PERSISTENT"
        )
    return WalletSession(prefs)
}

fun providesTransientSession(clock: Clock): TransientSession = TransientSession(clock)

fun provideCommonHeaderInterceptor(): CommonHeaderInterceptor {
    return CommonHeaderInterceptor(
        clientVersion = BuildConfig.VERSION_NAME,
        platform = "android/${Build.VERSION.RELEASE}"
    )
}

fun provideHeaderRequestInterceptor(
    gson: Gson,
    appId: String,
    deviceIdStore: DeviceIdStore
): HeaderRequestInterceptor {
    return HeaderRequestInterceptor(
        gson = gson,
        appId = appId,
        clientVersion = BuildConfig.VERSION_NAME,
        platform = "android/${Build.VERSION.RELEASE}",
        deviceIdStore = deviceIdStore,
    )
}

fun provideConnectivityManager(context: Context): ConnectivityManager =
    context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

private fun getPref(
    applicationContext: Context,
    applicationId: String,
    fileName: String,
): SharedPreferences {
    try {
        return getEncryptedSharedPreference(
            applicationContext,
            fileName
        )
    } catch (exception: Exception) {

        /**
         * Known issue thrown by Tink:
         * java.security.KeyStoreException: the master key
         * android-keystore://_androidx_security_master_key_ exists but is unusable,
         * https://github.com/google/tink/issues/535
         *
         * java.security.GeneralSecurityException: decryption failed
         * https://github.com/google/tink/issues/321
         *
         * At this point assumed key store is corrupted and data is lost
         */

        // Log exception for monitoring purpose
        Timber.tag(TAG_ENCRYPTED_SHARED_PREF).e(exception, "Exception recorded for $fileName")

        resetEncryptedSharedPref(applicationContext, applicationId)

        return getEncryptedSharedPreference(applicationContext, fileName)
    }
}

private fun getEncryptedSharedPreference(
    applicationContext: Context,
    fileName: String,
): SharedPreferences {
    val masterKey = MasterKey.Builder(applicationContext, MasterKey.DEFAULT_MASTER_KEY_ALIAS)
        .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
        .build()
    return EncryptedSharedPreferences.create(
        applicationContext,
        fileName,
        masterKey,
        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
    )
}

private fun resetEncryptedSharedPref(
    applicationContext: Context,
    applicationId: String
) {
    try {
        /**
         * delete master key from key store
         */
        val keyStore = KeyStore.getInstance("AndroidKeyStore")
        keyStore.load(null)
        keyStore.deleteEntry(MasterKey.DEFAULT_MASTER_KEY_ALIAS)
    } catch (e: Exception) {
        Timber.tag(TAG_ENCRYPTED_SHARED_PREF).e(e, "Exception recorded when reset shared pref")
    }

    /**
     * delete encrypted shared pref file
     * deleting master key without deleting all shared pref file will cause InvalidProtocolBufferException
     */
    clearAndDeleteAllSharedPref(applicationContext, applicationId)
}

private fun clearAndDeleteAllSharedPref(applicationContext: Context, applicationId: String) {
    clearAndDeleteSharedPref(applicationContext, "$applicationId$FILE_DEVICE_ID")
    clearAndDeleteSharedPref(applicationContext, "$applicationId$FILE_PERSISTENT")
}

private fun clearAndDeleteSharedPref(applicationContext: Context, fileName: String) {
    applicationContext.getSharedPreferences(fileName, Context.MODE_PRIVATE).edit().clear().apply()
    val sharedPrefsFile = File("${applicationContext.filesDir.parent}/shared_prefs/$fileName.xml")
    if (sharedPrefsFile.exists()) {
        sharedPrefsFile.delete()
        Timber.tag(TAG_ENCRYPTED_SHARED_PREF).i("File $fileName successfully deleted")
    } else {
        Timber.tag(TAG_ENCRYPTED_SHARED_PREF)
            .i("File $fileName does not exist, skipping delete process")
    }
}

fun provideSetAlertChannel() = FirebaseAlertEventsQueue()

fun provideFirebaseEventQueue() = FirebaseEventsQueue()

fun provideDrFirebaseMessagingService() = DrFirebaseMessagingService()

fun provideDrHmsPushService() = DrHmsPushService()