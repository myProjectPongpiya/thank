package com.thankyou.redesign.notificationservice

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect

class FirebaseEventsQueue(
    private val channel: MutableStateFlow<FirebaseNotificationEvent?> = MutableStateFlow(null)
) {

    private var hasStarted: Boolean = false

    suspend fun consumeEach(action: suspend (value: FirebaseNotificationEvent?) -> Unit) = channel.collect(action)

    suspend fun send(event: FirebaseNotificationEvent?) {
        if (hasStarted) channel.emit(event)
    }

    fun start() {
        hasStarted = true
    }

    fun stop() {
        hasStarted = false
    }
}
