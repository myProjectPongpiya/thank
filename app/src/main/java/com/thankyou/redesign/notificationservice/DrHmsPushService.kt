package com.thankyou.redesign.notificationservice

import com.huawei.hms.push.HmsMessageService
import com.huawei.hms.push.RemoteMessage
import com.thankyou.appdata.repository.model.home.SetAlertModel
import com.thankyou.appdata.repository.model.notification.FirebaseMessageData
import com.thankyou.appdata.repository.model.notification.LoginHieState
import com.thankyou.appfoundation.authentication.AppStatus
import com.thankyou.appfoundation.data.AppPersistentSession
import com.thankyou.appfoundation.notifications.FirebaseNotificationType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

class DrHmsPushService : HmsMessageService(), CoroutineScope {

    private val firebaseEventsQueue: FirebaseEventsQueue by inject()

    private val setAlertChannel: FirebaseAlertEventsQueue by inject()

    private val session: AppPersistentSession by inject()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job
    private val job = Job()

    override fun onDestroy() {
        super.onDestroy()
        cancelAllJobs()
    }

    override fun onMessageReceived(message: RemoteMessage) {
        val notificationTitle = message.notification?.title ?: return
        val notificationMessage = message.notification?.body ?: return
        val type = parseType(message.dataOfMap[TYPE_SERVICE_NAME])
        val state = parseTypeState(message.dataOfMap[STATE_SERVICE_NAME])
        val appStatus = parseTypAppStatus(message.dataOfMap[APP_STATUS_SERVICE_NAME])
        val data = FirebaseMessageData(
            messageText = notificationMessage,
            messageTitle = notificationTitle,
            type = type,
            state = state,
            appStatus = appStatus
        )
        emitNewNotification(data)
        emitMessagesMetrics(type)
    }

    private fun parseType(type: String?): FirebaseNotificationType? {
        return try {
            type?.let { FirebaseNotificationType.valueOf(it.uppercase()) }
        } catch (e: IllegalArgumentException) {
            null
        }
    }

    private fun parseTypeState(state: String?): LoginHieState? {
        return try {
            state?.let { LoginHieState.valueOf(it.uppercase()) }
        } catch (e: IllegalArgumentException) {
            null
        }
    }

    private fun parseTypAppStatus(appStatus: String?): AppStatus? {
        return try {
            appStatus?.let { AppStatus.valueOf(it.uppercase()) }
        } catch (e: IllegalArgumentException) {
            null
        }
    }

    private fun emitNewNotification(data: FirebaseMessageData) {
        val newMessageEvent = FirebaseNotificationEvent.ReceivedNewMessage(data)
        emitNewEvent(newMessageEvent)
    }

    override fun onNewToken(token: String?) {
        token?.let {
            val hmsToken = "huawei|$it"
            val newTokenEvent = FirebaseNotificationEvent.ReceivedNewTokenEvent(hmsToken)
            emitNewEvent(newTokenEvent)
            session.hmsToken = hmsToken
            Timber.d("New HMS token received $token")
        }
    }

    private fun emitNewEvent(event: FirebaseNotificationEvent) {
        launch { firebaseEventsQueue.send(event) }
    }

    private fun emitMessagesMetrics(type: FirebaseNotificationType?) {
        if (type == FirebaseNotificationType.LOGIN_HIE) return
        val alertData =
            SetAlertModel(
                isNotification = true
            )
        launch { setAlertChannel.send(alertData) }
    }

    private fun cancelAllJobs() {
        job.cancel()
    }

    companion object {
        private const val STATE_SERVICE_NAME = "state"
        private const val TYPE_SERVICE_NAME = "type"
        private const val APP_STATUS_SERVICE_NAME = "status"
    }
}
