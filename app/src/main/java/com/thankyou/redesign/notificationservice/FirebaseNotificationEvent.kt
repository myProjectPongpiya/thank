package com.thankyou.redesign.notificationservice

import com.thankyou.appdata.repository.model.notification.FirebaseMessageData

sealed class FirebaseNotificationEvent {

  class ReceivedNewTokenEvent(val fcmToken: String) : FirebaseNotificationEvent()

  class ReceivedNewMessage(val data: FirebaseMessageData) :
    FirebaseNotificationEvent()
}