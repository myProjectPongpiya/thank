package com.thankyou.redesign.notificationservice

import com.thankyou.appdata.repository.model.home.SetAlertModel
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.channels.consumeEach

class FirebaseAlertEventsQueue(
    private val channel: BroadcastChannel<SetAlertModel> = ConflatedBroadcastChannel()
) {

    @ObsoleteCoroutinesApi
    suspend fun consumeEach(block: (SetAlertModel) -> Unit) = channel.consumeEach(block)

    suspend fun send(event: SetAlertModel) {
        channel.send(event)
    }
}
