package com.thankyou.redesign

import android.app.Application
import com.thankyou.appfoundation.i18n.I18nLayoutInflaterInterceptor
import com.thankyou.appfoundation.logging.CrashlyticsTree
import com.thankyou.redesign.di.appComponent
import com.jakewharton.threetenabp.AndroidThreeTen
import org.hyperledger.indy.sdk.LibIndy
import io.github.inflationx.viewpump.ViewPump
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)

        startKoin {
            androidContext(this@MainApplication)
            modules(appComponent)
        }

        bootstrapLogging()
        bootstrapViewPump()
        setupCredWallet()
    }

    private fun bootstrapLogging() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(CrashlyticsTree())
        }
    }

    /**
     * Bootstraps layout inflation interceptors.
     */
    private fun bootstrapViewPump() {
        val viewPump = ViewPump.builder()
            .addInterceptor(I18nLayoutInflaterInterceptor())
            .build()
        ViewPump.init(viewPump)
    }

    private fun setupCredWallet() {
        System.setProperty("jna.nosys", "false")
        System.loadLibrary("jnidispatch")
        System.loadLibrary("indy")
        LibIndy.init()
    }
}