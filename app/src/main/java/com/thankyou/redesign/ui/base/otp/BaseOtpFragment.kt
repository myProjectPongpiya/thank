package com.thankyou.redesign.ui.base.otp

import android.os.Bundle
import android.view.View
import androidx.core.view.isGone
import com.thankyou.appfoundation.extensions.gone
import com.thankyou.appfoundation.lib.lifecycle.bindImage
import com.thankyou.appfoundation.lib.lifecycle.bindText
import com.thankyou.appfoundation.lib.lifecycle.bindEnabled
import com.thankyou.appfoundation.lib.lifecycle.bindVisibility
import com.thankyou.appfoundation.lib.lifecycle.withViewLifecycleOwner
import com.thankyou.redesign.R
import com.thankyou.redesign.databinding.ScreenOtpBinding
import com.thankyou.redesign.ui.base.SharedScreenFragment
import com.thankyou.redesign.ui.base.otp.BaseOtpViewModel.Companion.OTP_MAX_LENGTH

abstract class BaseOtpFragment : SharedScreenFragment<ScreenOtpBinding>(R.layout.screen_otp) {

    abstract override val vm: BaseOtpViewModel

    override fun initializeLayoutBinding(view: View): ScreenOtpBinding {
        return ScreenOtpBinding.bind(view)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        withViewLifecycleOwner {
            layout.ivOTP.bindImage(this, vm.otpIcon)
            layout.tvTitle.bindText(this, vm.titleLabel)
            layout.tvSubtitle.bindText(this, vm.subtitleLabel)
            layout.tvResendTime.bindText(this, vm.resendLabel)
            layout.wgtOtp.bindEnabled(this, vm.isInputEnabled)
            layout.tvRefId.bindText(this, vm.refIdLabel)
            layout.tvInstructionLabel.bindText(this, vm.instructionLabel)
            layout.tvInstructionLabel.bindVisibility(this, vm.instructionLabel)
            vm.onClearInputEvent.observe(this) { layout.wgtOtp.clear() }
            vm.onShowKeyboardEvent.observe(this) { layout.wgtOtp.requestFocusWithKeyboard() }
            vm.isResendButtonEnabled.observe {
                layout.gResendOTP.isGone = it
                layout.btnResend.isGone = !it
            }
        }

        layout.wgtOtp.setOnInputOtpListener {
            if (it.isNotEmpty()) {
                layout.tvInstructionLabel.gone()
            }
            if (it.length >= OTP_MAX_LENGTH) {
                vm.onEnterOtp(it)
            }
        }

        layout.btnResend.setOnClickListener {
            vm.onClickResend()
        }

        layout.wgtOtp.requestFocusWithKeyboard()
    }
}
