package com.thankyou.redesign.ui.common.newPin

import com.google.gson.annotations.SerializedName

enum class NewPinType {
  @SerializedName("CREATE")
  CREATE,
  @SerializedName("FORGOT_PIN")
  FORGOT_PIN,
  @SerializedName("CHANGE_PIN")
  CHANGE_PIN
}
