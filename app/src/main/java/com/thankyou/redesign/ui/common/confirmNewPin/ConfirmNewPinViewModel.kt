package com.thankyou.redesign.ui.common.confirmNewPin

import android.os.Parcelable
import com.thankyou.redesign.ui.base.pin.BasePinViewModel

class ConfirmNewPinViewModel(

) : BasePinViewModel() {

    override fun initialize(args: Parcelable?) {

    }

    override fun onEnterPin(pin: String) {

    }
}