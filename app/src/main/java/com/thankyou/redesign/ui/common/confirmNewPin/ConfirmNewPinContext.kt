package com.thankyou.redesign.ui.common.confirmNewPin

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ConfirmNewPinContext(
    private val value: String
) : Parcelable