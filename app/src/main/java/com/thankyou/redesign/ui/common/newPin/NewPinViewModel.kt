package com.thankyou.redesign.ui.common.newPin

import android.os.Parcelable
import androidx.lifecycle.MutableLiveData
import com.thankyou.appfoundation.i18n.T
import com.thankyou.redesign.ui.base.pin.BasePinViewModel

class NewPinViewModel : BasePinViewModel() {

    override val screenTitle = MutableLiveData(T.get("label_pin_change"))

    private lateinit var newPinContext: NewPinContext

    override fun initialize(args: Parcelable?) {
        clearPin()
        val value = args as? NewPinContext
        value?.let { newPinContext = it }
        instructionLabel.value = newPinContext.instructionLabelForNewPin
        isShowToolBar.value = newPinContext.isShowToolbar
        isShowLogo.value = newPinContext.isShowLogo
    }

    override fun onEnterPin(pin: String) {

    }
}