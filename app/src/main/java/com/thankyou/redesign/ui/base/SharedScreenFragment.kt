package com.thankyou.redesign.ui.base

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.viewbinding.ViewBinding
import com.thankyou.appfoundation.base.ViewBindingFragment
import com.thankyou.appfoundation.lib.lifecycle.withViewLifecycleOwner

abstract class SharedScreenFragment<T : ViewBinding>(
  @LayoutRes contentLayoutId: Int
) : ViewBindingFragment<T>(contentLayoutId) {

    abstract val vm: SharedScreenViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        withViewLifecycleOwner {
            vm.screenTitle.observe(this) { toolbar?.setTitle(it) }
            vm.toolbarNavigationType.observe(this) { setToolbarNavigationType(it) }
        }
    }

    override suspend fun onBackPressed(): Boolean {
        return vm.onBackPressed()
    }
}
