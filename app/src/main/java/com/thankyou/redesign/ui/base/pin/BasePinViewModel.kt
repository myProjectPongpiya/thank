package com.thankyou.redesign.ui.base.pin

import android.os.Parcelable
import androidx.lifecycle.MutableLiveData
import com.thankyou.appdata.repository.response.authentication.NotificationTokenRequest
import com.thankyou.appdomain.usecase.authentication.SetNotificationTokenUseCase
import com.thankyou.appfoundation.data.AppPersistentSession
import com.thankyou.appfoundation.extensions.isGreaterThanOrEqual
import com.thankyou.appfoundation.extensions.launchWith
import com.thankyou.appfoundation.extensions.tryOrNull
import com.thankyou.appfoundation.forms.NumberPadInput
import com.thankyou.appfoundation.i18n.T
import com.thankyou.appfoundation.model.MutableLiveEvent
import com.thankyou.appfoundation.text.RegexUtils
import com.thankyou.redesign.ui.base.SharedScreenViewModel

abstract class BasePinViewModel(
    private val persistentSession: AppPersistentSession? = null,
    private val setNotificationTokenUseCase: SetNotificationTokenUseCase? = null,
) : SharedScreenViewModel() {

    // UI
    open val pinMaxLength = PIN_MAX_LENGTH
    open val additionalActionLabel = MutableLiveData<String>()
    open val instructionLabel = MutableLiveData<String>()
    open val instructionError = MutableLiveEvent<Boolean>()
    open val isShowToolBar = MutableLiveData(true)
    open val isShowLogo = MutableLiveData(false)

    val registerNotification = MutableLiveEvent<Unit>()

    // Input

    val input = MutableLiveData<String>()

    /**
     * Initialize this View Model with [args].
     * @see BasePinScreen.createArguments
     */
    abstract fun initialize(args: Parcelable? = null)

    /**
     * Invoked when the user enters a full [pin].
     */
    abstract fun onEnterPin(pin: String)

    /**
     * Invoked when the user clicks on the forgot PIN button.
     */
    open fun onAdditionalActionClick() {
        // do nothing
    }

    /**
     * Invoked when the user enters a new [key].
     */
    open fun onEnterKey(key: NumberPadInput) {
        if (key == NumberPadInput.ADDITIONAL_ACTION) {
            return onAdditionalActionClick()
        }

        if (input.value?.length.isGreaterThanOrEqual(pinMaxLength) && key != NumberPadInput.DELETE) {
            return
        }

        val currentInput = when (key) {
            NumberPadInput.DELETE -> tryOrNull { input.value?.dropLast(1) } ?: ""
            else -> (input.value ?: "") + key.value
        }

        input.value = currentInput

        if (currentInput.isNotEmpty()) {
            instructionError.emit(false)
        }
        if (currentInput.length >= pinMaxLength) {
            onEnterPin(currentInput)
        }
    }

    open fun validatePin(pin: String): Boolean {
        return !RegexUtils.hasRepeatingDigits(pin)
                && !RegexUtils.hasMirroredDigits(pin)
                && !RegexUtils.hasSequentialDigits(pin)
                && !RegexUtils.hasRepeatingDigits(pin)
                && !RegexUtils.hasRepeatedSequentialDigits(pin)
    }

    open fun displayWeakPrompt(onClickContinue: (() -> Unit)? = null) {
        showPopupWithMultiButton(
            title = T.get("label_pin_title_weak"),
            message = T.get("label_pin_message_weak"),
            negativeButtonLabel = T.get("label_pin_negative_weak"),
            positiveButtonLabel = T.get("label_pin_positive_weak"),
            negativeButtonListener = {
                clearPin()
            },
            positiveButtonListener = {
                onClickContinue?.invoke()
            }
        )
    }

    /**
     * Clears the pin input.
     */
    open fun pinNotMach() {
        instructionError.emit(true)
        input.value = ""
    }

    /**
     * Clears the pin input.
     */
    open fun clearPin() {
        input.value = ""
    }

    fun registerFcmNotifications() {
        persistentSession?.fcmToken?.let {
            val params = NotificationTokenRequest(it)
            setNotificationTokenUseCase?.build(params)
                ?.launchWith(this)
        }
    }

    fun registerHmsNotifications() {
        persistentSession?.hmsToken?.let {
            val params = NotificationTokenRequest(it)
            setNotificationTokenUseCase?.build(params)
                ?.launchWith(this)
        }
    }

    fun showPopupInstructionWeak() {
        showPopup(
            title = T.get("label_pin_instruction_weak"),
            message = T.get("label_pin_instruction_message_weak"),
            buttonLabel = T.get("label_button_ok"),
        )
    }

    companion object {
        private const val PIN_MAX_LENGTH = 6
    }
}