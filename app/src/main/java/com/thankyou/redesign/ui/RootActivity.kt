package com.thankyou.redesign.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.annotation.IdRes
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.NavHostFragment
import com.thankyou.appfoundation.base.BaseActivity
import com.thankyou.appfoundation.i18n.LanguagePackProvider
import com.thankyou.appfoundation.i18n.T
import com.thankyou.appfoundation.i18n.models.SupportedLanguage
import com.thankyou.appfoundation.lib.eventbus.AppEvent
import com.thankyou.appfoundation.lib.eventbus.EventBus
import com.thankyou.appfoundation.lib.lifecycle.withViewLifecycleOwner
import com.thankyou.redesign.R
import com.thankyou.redesign.databinding.ActivityRootBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import timber.log.Timber

class RootActivity : BaseActivity(), AppTimeoutHelper.Listener {

    private val binding: ActivityRootBinding by lazy { ActivityRootBinding.inflate(layoutInflater) }

    private val languagePackProvider: LanguagePackProvider by inject()

    private val eventBus: EventBus by inject()

    private val appTimeoutHelper: AppTimeoutHelper by inject()

    override fun onUserInteraction() {
        super.onUserInteraction()
        appTimeoutHelper.registerInteraction()
    }

    override fun onResume() {
        super.onResume()
        T.setLanguage(SupportedLanguage.THAI)
        if (appTimeoutHelper.getLogin()) {
            appTimeoutHelper.start()
        }
//        window?.setFlags(
//            WindowManager.LayoutParams.FLAG_SECURE,
//            WindowManager.LayoutParams.FLAG_SECURE
//        )
        binding.navHostFragment.isVisible = true
        binding.ivAppLogo.isGone = true
    }

    override fun onPause() {
        super.onPause()
        window?.clearFlags(WindowManager.LayoutParams.FLAG_SECURE)
//        window?.setFlags(
//            WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
//            WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
//        )
        binding.ivAppLogo.isVisible = true
        binding.navHostFragment.isGone = true
    }

    override fun onDestroy() {
        super.onDestroy()
        appTimeoutHelper.dispose()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupStatusBar()
        listenForEventBus()
        setupLanguagePack()
        withViewLifecycleOwner {

        }
    }

    private fun setupStatusBar() {
        window.apply {
            statusBarColor = getColor(android.R.color.transparent)
            decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }
    }

    private fun listenForEventBus() {
        appTimeoutHelper.setListener(this@RootActivity)
        launch {
            eventBus.listen(AppEvent.LoginSuccess::class.java)
                .collect {
                    appTimeoutHelper.setLogin(true)
                    appTimeoutHelper.start()
                    appTimeoutHelper.restartSessionTimeout()
                }
        }
    }

    private fun setupLanguagePack() {
        languagePackProvider.initialize()
        T.setProvider(languagePackProvider)
    }

    override fun onSessionTimeout() {
        restartSession()
    }

    /**
    //     * When a deep link is invoked when the app is in the foreground, the deep
    //     * link intent will be routed through here.
    //     */
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
    }

    override fun gotoPage(navDirections: NavDirections) {
        createNavController().navigate(navDirections)
    }

    override fun goBack(@IdRes destinationId: Int?) {
        val navController = createNavController()
        if (destinationId == null) {
            navController.popBackStack()
        } else {
            try {
                navController
                    .popBackStack(destinationId, false)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    private fun createNavController(): NavController {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        return navHostFragment.navController
    }

    override fun restartSession() {}

    companion object {
        private const val LOGIN_CONTEXT = "loginContext"
    }
}