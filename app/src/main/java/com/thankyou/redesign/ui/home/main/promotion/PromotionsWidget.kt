package com.thankyou.redesign.ui.home.main.promotion

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.thankyou.appdata.repository.model.home.CampaignModel
import com.thankyou.redesign.R
import kotlinx.android.synthetic.main.layout_promotion_carousel.view.*

class PromotionsWidget @JvmOverloads constructor(
  context: Context,
  attrs: AttributeSet? = null,
  defStyle: Int = 0,
  defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyle, defStyleRes) {

    private var onPromotionCarouselClick: ((String?) -> Unit)? = null

    private lateinit var onScrollListener: OnScrollListener
    private var isOneItem = false

    private val mAdapter: PromotionCarouselAdapter by lazy {
        PromotionCarouselAdapter(this::onItemClick)
    }

    init {
        View.inflate(context, R.layout.layout_promotion_carousel, this)
    }

    init {
        val itemDecoration = PromotionCarouselSpacer(
            horizontalSpacing = resources.getDimension(R.dimen.spacing_tight).toInt()
        )

        val mLayoutManager = object : LinearLayoutManager(context, HORIZONTAL, false) {
            override fun checkLayoutParams(lp: RecyclerView.LayoutParams): Boolean {
                return if (mAdapter.itemCount == 1) {
                    // Force match parent of width
                    lp.width = ViewGroup.LayoutParams.MATCH_PARENT
                    true
                } else {
                    // Force to 80% of width
                    lp.width = (width * WIDTH_PERCENTAGE).toInt()
                    true
                }
            }
        }

        rvPromotionCarousel.apply {
            adapter = mAdapter
            addItemDecoration(itemDecoration)
            layoutManager = mLayoutManager
            mAdapter.onAttachedToRecyclerView(this)
        }
    }

    private fun setupScroller() {
        if (mAdapter.itemCount < 2) {
            return
        }

        onScrollListener = OnScrollListener(
            itemCount = mAdapter.itemCount,
            layoutManager = rvPromotionCarousel.layoutManager as LinearLayoutManager,
            stateChanged = { /* do nothing */ }
        )
        rvPromotionCarousel.addOnScrollListener(onScrollListener)
    }

    fun setOnItemClickListener(listener: (String?) -> Unit) {
        onPromotionCarouselClick = listener
    }

    private fun onItemClick(redirectUrl: String?) {
        onPromotionCarouselClick?.invoke(redirectUrl)
    }

    fun setPromotionItem(promotions: List<CampaignModel>) {
        mAdapter.setData(promotions)

        isOneItem = promotions.size <= 1

        if (promotions.isNotEmpty() && promotions.size > 1) {
//            rvPromotionCarousel.smoothScrollToPosition(0)
            setupScroller()
        }
    }

    private class OnScrollListener(
      val itemCount: Int,
      val layoutManager: LinearLayoutManager,
      val stateChanged: (Int) -> Unit
    ) : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val firstItemVisible = layoutManager.findFirstVisibleItemPosition()

            if (firstItemVisible > 0 && firstItemVisible % (itemCount - 1) == 0) {
                // When position reaches end of the list, it should go back to the beginning
                recyclerView.scrollToPosition(1)
            }
        }

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            stateChanged(newState)
        }
    }

    companion object {
        private const val WIDTH_PERCENTAGE = 0.8
    }
}
