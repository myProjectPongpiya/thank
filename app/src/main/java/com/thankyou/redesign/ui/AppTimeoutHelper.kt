package com.thankyou.redesign.ui

import android.os.SystemClock
import androidx.annotation.WorkerThread
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.thankyou.appfoundation.extensions.disposedBy
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * [AppTimeoutHelper] is an utility class to limit the session of the user. When
 * the timer starts, two different kinds of timeout will be tracked:
 *
 * **Session Timeout** - Invoked when the user uses the app longer than
 *   [maxSessionSeconds] since the user last logged in.
 */
class AppTimeoutHelper(
    private val maxSessionSeconds: Long,
    private val getCurrentMillis: () -> Long = { SystemClock.elapsedRealtime() }
) {

    private val timerDisposable = CompositeDisposable()
    private var listener: Listener? = null

    private var lastInteractionTimestamp: Long? = null

    private var isLogin = false

    fun setLogin(isLogin: Boolean) {
        this.isLogin = isLogin
    }

    fun getLogin(): Boolean {
        return this.isLogin
    }

    /**
     * Sets the [listener] to receive events.
     */
    fun setListener(listener: Listener?) {
        this.listener = listener
    }

    /**
     * Starts tracking the user's session timeout.
     */
    fun start() {
        timerDisposable.clear()
        Observable.interval(1, TimeUnit.SECONDS, Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread()).subscribe({ onTick() }, {
                FirebaseCrashlytics.getInstance().log("Exception: " + it.message)
            })
            .disposedBy(timerDisposable)
    }

    /**
     * Stops tracking the user's session timeout.
     */
    fun stop() {
        timerDisposable.clear()
        setLogin(false)
    }

    /**
     * Restarts the session timeout timer.
     */
    fun restartSessionTimeout() {
        start()
    }

    /**
     * Clears all disposables and prevents any further tracking.
     */
    fun dispose() {
        timerDisposable.dispose()
    }

    /**
     * Resets the last interaction timestamp.
     */
    fun registerInteraction() {
        lastInteractionTimestamp = getCurrentMillis()
    }

    private fun onTick() {
        // Check for session timeout
        lastInteractionTimestamp?.let {
            if (TimeUnit.MILLISECONDS.toSeconds(getCurrentMillis() - it) > maxSessionSeconds) {
                listener?.onSessionTimeout()
                timerDisposable.clear()
                Timber.tag("APP_TIMEOUT").d("Session timed out")
                return
            }
        }
    }

    interface Listener {
        /**
         * Invoked when the user's session is longer than [maxSessionSeconds].
         */
        @WorkerThread
        fun onSessionTimeout()
    }
}