package com.thankyou.redesign.ui.base.biometric

import android.content.Intent
import android.os.Build
import android.provider.Settings
import androidx.annotation.LayoutRes
import androidx.viewbinding.ViewBinding
import co.infinum.goldfinger.Goldfinger
import co.infinum.goldfinger.NoEnrolledFingerprintException
import com.thankyou.appfoundation.base.ViewBindingFragment
import com.thankyou.appfoundation.i18n.T

abstract class BaseBiometricFragment<Binding : ViewBinding>(
    @LayoutRes contentLayoutId: Int
) : ViewBindingFragment<Binding>(contentLayoutId) {

    open val goldfinger by lazy {
        Goldfinger.Builder(requireContext()).build()
    }

    open fun onRequestBiometrics(event: BiometricEvent) {
        if (this.isAdded) {
            val params = Goldfinger.PromptParams.Builder(this)
                .title(T.get("label_button_finger_print"))
                .negativeButtonText(T.get("label_button_cancel"))
                .build()

            val callback = object : Goldfinger.Callback {
                override fun onResult(result: Goldfinger.Result) {
                    when (result.type()) {
                        Goldfinger.Type.SUCCESS -> {
                            result.value()?.let { event.onSuccess(it) }
                        }
                        Goldfinger.Type.ERROR -> {
                            when (result.reason()) {
                                Goldfinger.Reason.USER_CANCELED,
                                Goldfinger.Reason.CANCELED,
                                Goldfinger.Reason.NEGATIVE_BUTTON -> {
                                    event.onCanceled?.invoke()
                                }
                                else -> event.onError.invoke(result.message() ?: "")
                            }
                        }
                        Goldfinger.Type.INFO -> {
                            /* do nothing */
                        }
                    }
                }

                override fun onError(e: Exception) {
                    if (e is NoEnrolledFingerprintException) {
                        showPopupWithMultiButton(
                            T.get("label_enable_setting_biometrics_title"),
                            T.get("label_enable_setting_biometrics_message"),
                            negativeButtonLabel = T.get("label_button_cancel"),
                            positiveButtonLabel = T.get("label_setting_dialog"),
                            positiveButtonListener = {
                                setUpBiometricsSettingDevice()
                            }
                        )
                    } else {
                        event.onError.invoke(e.message ?: "")
                    }
                }
            }

            goldfinger.encrypt(
                params,
                ENCRYPT_BIOMETRICS_UUID,
                event.biometricsUuid,
                callback
            )
        }
    }

    private fun setUpBiometricsSettingDevice() {
        val intent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            Intent(Settings.ACTION_FINGERPRINT_ENROLL)
        } else {
            Intent(Settings.ACTION_SECURITY_SETTINGS)
        }
        startActivity(intent)
    }

    companion object {
        const val ENCRYPT_BIOMETRICS_UUID = "ENCRYPT_BIOMETRICS_UUID"
    }
}