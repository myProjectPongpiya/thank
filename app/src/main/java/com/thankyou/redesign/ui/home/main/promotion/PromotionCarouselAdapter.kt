package com.thankyou.redesign.ui.home.main.promotion

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.thankyou.appdata.repository.model.home.CampaignModel
import com.thankyou.redesign.R

class PromotionCarouselAdapter(
  private val onPromotionCarouselClick: ((promotionId: String?) -> Unit)? = null
) : RecyclerView.Adapter<PromotionCarouselViewHolder>() {

  private var promotionList: List<CampaignModel> = listOf()

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PromotionCarouselViewHolder {
    val view = LayoutInflater
      .from(parent.context)
      .inflate(R.layout.item_promotion_carousel_item, parent, false)
    return PromotionCarouselViewHolder(view, this::onClickViewHolder)
  }

  override fun getItemCount(): Int {
    return promotionList.size
  }

  override fun onBindViewHolder(holder: PromotionCarouselViewHolder, position: Int) {
    holder.bind(promotionList[position])
  }

  private fun onClickViewHolder(position: Int) {
    promotionList[position].redirectUrl.let {
      onPromotionCarouselClick?.invoke(it.toString())
    }
  }

  fun setData(
    promotionList: List<CampaignModel>
  ) {
    this.promotionList = promotionList
    this.notifyDataSetChanged()
  }
}