package com.thankyou.redesign.ui.common.confirmNewPin

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.thankyou.appfoundation.extensions.bindViewModel
import com.thankyou.appfoundation.lib.lifecycle.withViewLifecycleOwner
import com.thankyou.redesign.ui.base.pin.BasePinFragment
import org.koin.androidx.viewmodel.ext.android.getViewModel

class ConfirmNewPinFragment : BasePinFragment() {

    override val vm by bindViewModel { getViewModel(clazz = ConfirmNewPinViewModel::class) }

    private val args by navArgs<ConfirmNewPinFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(vm) {
            withViewLifecycleOwner {

            }
        }
    }
}