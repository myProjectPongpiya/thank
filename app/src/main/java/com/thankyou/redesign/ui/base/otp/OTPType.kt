package com.thankyou.redesign.ui.base.otp

import com.google.gson.annotations.SerializedName

enum class OTPType {
    @SerializedName("MOBILE")
    MOBILE,
    @SerializedName("EMAIL")
    EMAIL
}