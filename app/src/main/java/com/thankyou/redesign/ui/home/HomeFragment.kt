package com.thankyou.redesign.ui.home

import android.os.Bundle
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.thankyou.appfoundation.base.ViewBindingFragment
import com.thankyou.appfoundation.extensions.bindViewModel
import com.thankyou.appfoundation.lib.lifecycle.withViewLifecycleOwner
import com.thankyou.appfoundation.menu.setNavBottomTabItemListener
import com.thankyou.redesign.R
import com.thankyou.redesign.databinding.ScreenHomeBinding
import org.koin.androidx.viewmodel.ext.android.getViewModel

class HomeFragment :
    ViewBindingFragment<ScreenHomeBinding>(R.layout.screen_home) {

    private val vm by bindViewModel { getViewModel(clazz = HomeViewModel::class) }

    override fun initializeLayoutBinding(view: View) =
        ScreenHomeBinding.bind(view)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm.initialize()
        initViewPager()
        initBottomNavBar()
        observeViewPagerData()
        observeCloseApplicationEvent()
    }

    private fun initBottomNavBar() {
        layout.navBar.initMenu(requireActivity())
        layout.navBar.setNavBottomTabItemListener { tab ->
            vm.onTabChanged(tab)
        }
    }

    private fun initViewPager() {
        layout.vpHome.apply {
            isUserInputEnabled = false
            offscreenPageLimit = ViewPager2.OFFSCREEN_PAGE_LIMIT_DEFAULT
        }
    }

    private fun observeViewPagerData() {
        withViewLifecycleOwner {
            vm.viewPagerScreens.observe {
                layout.vpHome.adapter = HomeAdapter(this@HomeFragment, it)
            }
            vm.currentlySelectedTab.observe { tab ->
                layout.vpHome.setCurrentItem(tab, false)
            }
        }
    }

    override suspend fun onBackPressed() = vm.onBackButtonPressed()

    private fun observeCloseApplicationEvent() {
        withViewLifecycleOwner {
            vm.onCloseApplicationEvent.observe {
                activity?.finishAndRemoveTask()
            }
        }
    }
}