package com.thankyou.redesign.ui.splash

import android.os.Build
import android.os.Bundle
import android.view.View
import com.scottyab.rootbeer.RootBeer
import com.thankyou.appfoundation.base.ViewBindingFragment
import com.thankyou.appfoundation.extensions.bindViewModel
import com.thankyou.appfoundation.i18n.T
import com.thankyou.redesign.R
import com.thankyou.redesign.databinding.ScreenSplashBinding
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import org.koin.androidx.viewmodel.ext.android.getViewModel
import kotlin.coroutines.resume

class SplashFragment :
    ViewBindingFragment<ScreenSplashBinding>(R.layout.screen_splash) {

    private val vm by bindViewModel { getViewModel(clazz = SplashViewModel::class) }

    override fun initializeLayoutBinding(view: View) =
        ScreenSplashBinding.bind(view)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rootBeer = RootBeer(context)
        when {
            isEmulator() -> launch { showEmulatorDialog() }
            rootBeer.isRooted || rootBeer.checkForMagiskBinary() -> launch { showRootDialog() }
            else -> vm.initialize()
        }
        layout.ivInfinitas.text = T.get("label_simulator_root_title")
    }

    private suspend fun showRootDialog() =
        suspendCancellableCoroutine<Unit> { continuation ->
            showPopup(
                title = T.get("label_simulator_root_title"),
                message = T.get("label_simulator_root_message"),
                onDismiss = {
                    requireActivity().finish()
                    continuation.resume(Unit)
                }
            )
        }

    private suspend fun showEmulatorDialog() =
        suspendCancellableCoroutine<Unit> { continuation ->
            showPopup(
                title = T.get("label_simulator_root_title"),
                message = T.get("label_simulator_found_message"),
                onDismiss = {
                    requireActivity().finish()
                    continuation.resume(Unit)
                }
            )
        }

    @SuppressWarnings("ComplexMethod")
    private fun isEmulator(): Boolean {
        return Build.BRAND.startsWith("generic")
                && Build.DEVICE.startsWith("generic")
                || Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.HARDWARE.contains("goldfish")
                || Build.HARDWARE.contains("ranchu")
                || Build.HARDWARE.equals("vbox86")
                || Build.HARDWARE.lowercase().contains("nox")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MODEL.lowercase().contains("droid4x")
                || Build.MANUFACTURER.contains("Genymotion")
                || Build.PRODUCT.contains("sdk_google")
                || Build.PRODUCT.contains("google_sdk")
                || Build.PRODUCT.contains("sdk")
                || Build.PRODUCT.contains("sdk_x86")
                || Build.PRODUCT.contains("vbox86p")
                || Build.PRODUCT.contains("emulator")
                || Build.PRODUCT.contains("simulator")
                || Build.PRODUCT.lowercase().contains("nox")
                || Build.BOARD.lowercase().contains("nox")
                || Build.BOOTLOADER.lowercase().contains("nox")
    }
}