package com.thankyou.redesign.ui.home

import androidx.lifecycle.MutableLiveData
import com.thankyou.appfoundation.base.BaseViewModel
import com.thankyou.appfoundation.base.ScreenFragment
import com.thankyou.appfoundation.i18n.T
import com.thankyou.appfoundation.menu.BottomTabWidget
import com.thankyou.appfoundation.model.MutableLiveEvent
import com.thankyou.appfoundation.ui.Alert
import com.thankyou.redesign.ui.home.main.MainFragment
import com.thankyou.redesign.ui.home.profile.ProfileFragment

class HomeViewModel : BaseViewModel() {

    val onCloseApplicationEvent = MutableLiveEvent<Unit>()

    val viewPagerScreens = MutableLiveData<List<ScreenFragment>>()
    val currentlySelectedTab = MutableLiveData<Int>()

    private val logoutPopup = Alert.NativePopup(
        title = T.get("label_logout_title"),
        message = T.get("label_logout_message"),
        positiveButtonLabel = T.get("label_button_confirm"),
        negativeButtonLabel = T.get("label_button_cancel"),
        positiveButtonListener = ::onLogoutPopButtonPressed
    )

    fun initialize() {
        initScreens()
    }

    private fun initScreens() {
        val screens: List<ScreenFragment> =
            listOf(
                MainFragment.newInstance(),
                ProfileFragment.newInstance()
            )
        viewPagerScreens.value = screens
    }

    fun onTabChanged(index: Int) {
        val tabIndex = when (index) {
            BottomTabWidget.MAIN_TAB -> HomeScreen.MAIN_SCREEN.page
            BottomTabWidget.PROFILE_TAB -> HomeScreen.PROFILE_SCREEN.page
            else -> NO_SCREEN
        }

        val itemCount = viewPagerScreens.value?.size ?: EMPTY_LIST_SIZE
        if (tabIndex in EMPTY_LIST_SIZE until itemCount) {
            currentlySelectedTab.value = tabIndex
        }
    }

    fun onBackButtonPressed(): Boolean {
        onAlertEvent.emit(logoutPopup)
        return true
    }

    private fun onLogoutPopButtonPressed() {
        closeApplication()
    }

    private fun closeApplication() {
        onCloseApplicationEvent.call()
    }

    companion object {
        const val NO_SCREEN = -1
        const val EMPTY_LIST_SIZE = 0
    }
}