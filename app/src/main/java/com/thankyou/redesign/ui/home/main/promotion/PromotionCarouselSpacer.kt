package com.thankyou.redesign.ui.home.main.promotion

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class PromotionCarouselSpacer(
  private val horizontalSpacing: Int
) : RecyclerView.ItemDecoration() {

  override fun getItemOffsets(
    outRect: Rect,
    view: View,
    parent: RecyclerView,
    state: RecyclerView.State
  ) {
    if (view.isLastItem(parent)) {
      return
    }

    outRect.right = horizontalSpacing
  }

  private fun View.isLastItem(parent: RecyclerView): Boolean {
    val itemCount = parent.adapter?.itemCount ?: return false
    return parent.getChildAdapterPosition(this) == itemCount - 1
  }
}
