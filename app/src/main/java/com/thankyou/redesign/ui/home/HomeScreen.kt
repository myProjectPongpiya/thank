package com.thankyou.redesign.ui.home

enum class HomeScreen(val page: Int) {
    MAIN_SCREEN(0),
    PROFILE_SCREEN(1)
}