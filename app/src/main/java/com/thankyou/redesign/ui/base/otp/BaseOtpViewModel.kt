package com.thankyou.redesign.ui.base.otp

import android.os.Parcelable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import com.thankyou.appfoundation.extensions.formatType
import com.thankyou.appfoundation.extensions.toCountdownFormat
import com.thankyou.appfoundation.graphics.DrImage
import com.thankyou.appfoundation.i18n.T
import com.thankyou.appfoundation.model.MutableLiveEvent
import com.thankyou.appfoundation.text.FormatType
import com.thankyou.redesign.R
import com.thankyou.redesign.ui.base.SharedScreenViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

abstract class BaseOtpViewModel : SharedScreenViewModel() {

    protected val cooldownSeconds = MutableLiveData<Long>()
    private var countdownJob: Job? = null

    // UI

    abstract val otpIcon: LiveData<DrImage>
    abstract val titleLabel: LiveData<String>
    abstract val subtitleLabel: LiveData<String>
    abstract val refIdLabel: LiveData<String>
    abstract val resendLabel: LiveData<String>
    internal val isInputEnabled = MutableLiveData(true)
    open val isResendButtonEnabled = Transformations.map(cooldownSeconds) {
        it == ZERO.toLong()
    }
    open val onOtpSuccess = MutableLiveEvent<Pair<OTPType, Parcelable>>()

    internal val instructionLabel = MutableLiveData<String>()

    // Events

    internal val onShowKeyboardEvent = MutableLiveEvent<Unit>()
    internal val onClearInputEvent = MutableLiveEvent<Unit>()

    /**
     * Initialize this View Model.
     */
    abstract fun initialize(args: Parcelable? = null)

    /**
     * Invoked when the user enters the the OTP.
     */
    abstract fun onEnterOtp(otp: String)

    /**
     * Invoked when the user clicks on the resend button.
     */
    abstract fun onClickResend()

    fun showKeyboard() {
        onShowKeyboardEvent.call()
    }

    /**
     * Clears the current OTP input.
     */
    fun clearInput() {
        onClearInputEvent.call()
    }

    /**
     * Set data to the instruction label.
     */
    fun setInstructionLabel(instructionLabel: String) {
        this.instructionLabel.value = instructionLabel
    }

    /**
     * Enable/disable the OTP input.
     */
    fun enableInput(isEnabled: Boolean) {
        this.isInputEnabled.value = isEnabled
    }

    /**
     * Starts the resend OTP cooldown. If an existing cooldown is currently active,
     * calling this function will reset it.
     */
    fun startCooldown(seconds: Long = DEFAULT_OTP_COOLDOWN_SECONDS) {
        countdownJob?.cancel()
        cooldownSeconds.value = seconds
        countdownJob = viewModelScope.launch {
            var remainingSeconds = seconds
            while (remainingSeconds > 0) {
                delay(COOL_DOWN_DELAY)
                remainingSeconds -= 1
                cooldownSeconds.value = remainingSeconds
            }
        }
    }

    /**
     * Stops the resend OTP cooldown.
     */
    fun stopCooldown() {
        countdownJob?.cancel()
        cooldownSeconds.value = 0
    }

    open fun getIcon(otpContext: OtpContext): DrImage {
        val icon = when (otpContext.type) {
            OTPType.MOBILE -> R.drawable.ic_otp_mobile
            OTPType.EMAIL -> R.drawable.ic_otp_email
        }
        return DrImage.ResId(resId = icon)
    }

    open fun getScreenTitle(otpContext: OtpContext): String {
        val str = when (otpContext.type) {
            OTPType.MOBILE -> "label_otp_nav_mobile"
            OTPType.EMAIL -> "label_otp_nav_email"
        }
        return T.get(str)
    }

    open fun getTitleLabel(otpContext: OtpContext): String {
        val str = when (otpContext.type) {
            OTPType.MOBILE -> "label_otp_mobile"
            OTPType.EMAIL -> "label_otp_email"
        }
        return T.get(str)
    }

    open fun getSubTitleLabel(otpContext: OtpContext): String {
        val format = when (otpContext.type) {
            OTPType.MOBILE -> FormatType.MobileNo
            OTPType.EMAIL -> FormatType.Email
        }
        return otpContext.value.formatType(format, true)
    }

    open fun getResendTime(cooldownSeconds: Long?): String {
        return if (cooldownSeconds == null || cooldownSeconds <= ZERO) {
            EMPTY_DATA
        } else {
            cooldownSeconds.takeIf { it > ZERO }
                ?.toCountdownFormat()
                ?: EMPTY_DATA
        }
    }

    companion object {
        private const val COOL_DOWN_DELAY = 1000L
        private const val DEFAULT_OTP_COOLDOWN_SECONDS = 15L
        const val EMPTY_DATA = ""
        const val ZERO = 0
        const val OTP_MAX_LENGTH = 6
    }
}
