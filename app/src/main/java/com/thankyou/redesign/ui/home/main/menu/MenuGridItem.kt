package com.thankyou.redesign.ui.home.main.menu

import com.thankyou.appfoundation.graphics.DrImage

data class MenuGridItem(
  val id: String,
  val title: String,
  val icon: DrImage,
  var isActive: Boolean = true,
)
