package com.thankyou.redesign.ui.home.main.menu

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MenuGridList @JvmOverloads constructor(
  context: Context,
  attrs: AttributeSet? = null,
  defStyle: Int = 0
) : RecyclerView(context, attrs, defStyle) {

  private val adapter = MenuGridAdapter()

  init {
    super.setAdapter(adapter)
    super.setLayoutManager(GridLayoutManager(context, SPAN_COUNT))
  }

  fun setItems(items: List<MenuGridItem>?) {
    adapter.setItems(items ?: emptyList())
  }

  fun setOnItemClickListener(listener: ((id: String) -> Unit)?) {
    adapter.setOnClickListener(listener)
  }

  companion object {
    private const val SPAN_COUNT = 2
  }
}
