package com.thankyou.redesign.ui.base.biometric

data class BiometricEvent(
  val biometricsUuid: String,
  val onSuccess: (encryptedValue: String) -> Unit,
  val onError: (errorMessage: String) -> Unit,
  val onCanceled: (() -> Unit)? = null
)
