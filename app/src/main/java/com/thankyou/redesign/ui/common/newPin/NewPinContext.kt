package com.thankyou.redesign.ui.common.newPin

import android.os.Parcelable
import androidx.annotation.IdRes
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NewPinContext(
    @IdRes
    val goBack: Int? = null,
    val tokenUuid: String? = null,
    val isShowLogo: Boolean = false,
    val isShowToolbar: Boolean = true,
    val instructionLabelForNewPin: String,
    val instructionLabelForConfirmNewPin: String,
    val newPinType: NewPinType,
    val oldPin: String? = null
) : Parcelable
