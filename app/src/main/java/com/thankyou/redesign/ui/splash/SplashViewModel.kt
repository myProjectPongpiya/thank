package com.thankyou.redesign.ui.splash

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.thankyou.appdata.repository.response.dashboard.TmcResponse
import com.thankyou.appdomain.usecase.dashboard.GetTmcUseCase
import com.thankyou.appfoundation.base.BaseViewModel
import com.thankyou.appfoundation.extensions.launchWith
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class SplashViewModel(
    private val getTmcUseCase: GetTmcUseCase,
) : BaseViewModel() {

    private var startTime = System.currentTimeMillis()
    private val tmcResponse = MutableLiveData<TmcResponse>()

    fun initialize() {
        getTmcUseCase.build(Unit)
            .onEach {
                getTmcResponseSuccess(it)
            }
            .launchWith(this, onError = {

            })
    }

    private fun getTmcResponseSuccess(response: TmcResponse) {
        tmcResponse.value = response
        navToPage(SplashFragmentDirections.gotoHomeFragment())
    }

    private fun getDelay(): Long {
        return startTime.let {
            2500 - (System.currentTimeMillis() - it)
        } ?: 2500
    }

    private fun navToPage(page: NavDirections, gotoNow: Boolean = false) {
        if (gotoNow) {
            gotoPage(page)
        } else {
            GlobalScope.launch(Dispatchers.Main) {
                delay(getDelay())
                navToPage(page, true)
            }
        }
    }
}