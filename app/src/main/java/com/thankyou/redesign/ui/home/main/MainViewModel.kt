package com.thankyou.redesign.ui.home.main

import android.graphics.BitmapFactory
import android.util.Base64
import androidx.lifecycle.MutableLiveData
import com.thankyou.appdata.repository.model.profile.InquiryDoctorInfoModel
import com.thankyou.appfoundation.base.BaseViewModel
import com.thankyou.appfoundation.data.AppPersistentSession
import com.thankyou.appfoundation.graphics.DrImage
import com.thankyou.appfoundation.graphics.ImageTransformations
import com.thankyou.appfoundation.i18n.T
import com.thankyou.redesign.R
import com.thankyou.redesign.ui.home.main.menu.MenuGridItem

class MainViewModel(
    private val persistentSession: AppPersistentSession,
) : BaseViewModel() {

    val itemGrid = MutableLiveData<List<MenuGridItem>>()

    var permissionCameraCheck = persistentSession.permissionCameraCheck

    fun initialize() {
        initIconMenu()
    }

    private fun onSetDoctorImg(items: InquiryDoctorInfoModel): DrImage {
        return if (items.doctorImg.isNullOrEmpty()) {
            DrImage.ResId(
                resId = R.drawable.ic_avatar_profile,
                transformations = listOf(ImageTransformations.CircleCrop)
            )
        } else {
            val imageBytes = Base64.decode(items.doctorImg, 0)
            val image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
            DrImage.Bitmap(
                bitmap = image,
                transformations = listOf(ImageTransformations.CircleCrop)
            )
        }
    }

    private fun initIconMenu() {
        itemGrid.value = listOf(
            MenuGridItem(
                id = MD_CARD,
                title = T.get("label_icon_md_card"),
                icon = DrImage.ResId(resId = R.drawable.ic_md_card),
            ),
            MenuGridItem(
                id = SCAN_HIE,
                title = T.get("label_icon_scan_hie"),
                icon = DrImage.ResId(resId = R.drawable.ic_scan_hie),
            )
        )
    }

    private fun updateCameraPermission() {
        persistentSession.permissionCameraCheck = true
        permissionCameraCheck = true
    }

    companion object {
        private const val EMPTY_DATA = ""
        internal const val MD_CARD = "MD_CARD"
        internal const val SCAN_HIE = "SCAN_HIE"
    }
}