package com.thankyou.redesign.ui.base.pin

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.view.isVisible
import com.thankyou.appfoundation.extensions.toPx
import com.thankyou.appfoundation.lib.lifecycle.bindText
import com.thankyou.appfoundation.lib.lifecycle.bindVisibility
import com.thankyou.appfoundation.lib.lifecycle.withViewLifecycleOwner
import com.thankyou.appfoundation.service.startGmsHmsCheck
import com.thankyou.redesign.R
import com.thankyou.redesign.databinding.ScreenPinBinding
import com.thankyou.redesign.ui.base.SharedScreenFragment

abstract class BasePinFragment : SharedScreenFragment<ScreenPinBinding>(R.layout.screen_pin) {

    abstract override val vm: BasePinViewModel

    private var indicators = emptyList<ImageView>()

    override fun initializeLayoutBinding(view: View): ScreenPinBinding {
        return ScreenPinBinding.bind(view)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createIndicators(vm.pinMaxLength)

        withViewLifecycleOwner {
            layout.tvInstruction
                .bindText(this, vm.instructionLabel)
                .bindVisibility(this, vm.instructionLabel)
            layout.tvInstructionError
                .bindVisibility(this, vm.instructionError)

            vm.input.observe(this) { updateIndicators(it) }
            vm.additionalActionLabel.observe(this) { layout.npView.setAdditionalActionLabel(it) }

            vm.isShowLogo.observe(this) {
                layout.ivLogo.isVisible = it
            }

            vm.isShowToolBar.observe(this) {
                layout.toolbar.isVisible = it
            }

            vm.registerNotification.observe {
                context?.startGmsHmsCheck(
                    isGmsAvailableCallback = { vm.registerFcmNotifications() },
                    isHmsAvailableCallback = { vm.registerHmsNotifications() }
                )
            }
            layout.tvInstructionWeak.setOnClickListener {
                vm.showPopupInstructionWeak()
            }
        }

        layout.npView.setOnInputListener {
            vm.onEnterKey(it)
        }
    }

    private fun createIndicators(count: Int) {
        layout.divIndicators.removeAllViews()

        this.indicators = List(count) { index ->
            ImageView(requireContext()).apply {
                layoutParams =
                    LinearLayout.LayoutParams(
                        INDICATOR_WIDTH_SIZE.toPx().toInt(),
                        INDICATOR_HEIGHT_SIZE.toPx().toInt()
                    ).also {
                        when (index) {
                            INDICATOR_FIRST_INDEX -> {
                                it.marginStart =
                                    context.resources.getDimension(R.dimen.spacing_tight).toInt()
                                it.marginEnd =
                                    context.resources.getDimension(R.dimen.spacing_tiny).toInt()
                            }
                            count - 1 -> {
                                it.marginStart =
                                    context.resources.getDimension(R.dimen.spacing_tiny).toInt()
                                it.marginEnd =
                                    context.resources.getDimension(R.dimen.spacing_tight).toInt()
                            }
                            else -> {
                                it.marginStart =
                                    context.resources.getDimension(R.dimen.spacing_tiny).toInt()
                                it.marginEnd =
                                    context.resources.getDimension(R.dimen.spacing_tiny).toInt()
                            }
                        }
                    }
                setImageResource(R.drawable.bg_pin_indicator_empty)
            }
        }

        indicators.forEach { layout.divIndicators.addView(it) }
    }

    private fun updateIndicators(input: String?) {
        indicators.forEachIndexed { i, view ->
            val char = input?.getOrNull(i)

            val resId = if (char == null || char.isWhitespace()) {
                R.drawable.bg_pin_indicator_empty
            } else {
                R.drawable.bg_pin_indicator_filled
            }

            view.setImageResource(resId)
        }
    }

    companion object {
        const val INDICATOR_FIRST_INDEX = 0
        const val INDICATOR_WIDTH_SIZE = 16f
        const val INDICATOR_HEIGHT_SIZE = 16f
    }
}
