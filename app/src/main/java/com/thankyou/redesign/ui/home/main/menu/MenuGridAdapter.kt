package com.thankyou.redesign.ui.home.main.menu

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.thankyou.redesign.R

internal class MenuGridAdapter : RecyclerView.Adapter<MenuGridViewHolder>() {

  private var onClickListener: ((id: String) -> Unit)? = null

  private var items = mutableListOf<MenuGridItem>()

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuGridViewHolder {
    val inflater = LayoutInflater.from(parent.context)
    val layout = inflater.inflate(R.layout.item_grid, parent, false)
    return MenuGridViewHolder(layout, this::onClickViewHolder)
  }

  override fun onBindViewHolder(holder: MenuGridViewHolder, position: Int) {
    holder.bindIcon(items[position])
  }

  override fun getItemCount(): Int = items.size

  private fun onClickViewHolder(position: Int) {
    onClickListener?.invoke(items[position].id)
  }

  fun setItems(items: List<MenuGridItem>) {
    if (items.isEmpty()) return
    this.items.addAll(items)
    notifyDataSetChanged()
  }

  fun setOnClickListener(listener: ((id: String) -> Unit)?) {
    this.onClickListener = listener
  }
}
