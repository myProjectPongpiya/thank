package com.thankyou.redesign.ui.common.newPin

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.thankyou.appfoundation.extensions.bindViewModel
import com.thankyou.appfoundation.extensions.visible
import com.thankyou.redesign.ui.base.pin.BasePinFragment
import org.koin.androidx.viewmodel.ext.android.getViewModel

class NewPinFragment : BasePinFragment() {

    override val vm by bindViewModel { getViewModel(clazz = NewPinViewModel::class) }

    private val args by navArgs<NewPinFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm.initialize(args.newPinContext)
        layout.tvInstructionWeak.visible()
    }
}