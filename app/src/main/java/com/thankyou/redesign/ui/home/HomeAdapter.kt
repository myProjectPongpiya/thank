package com.thankyou.redesign.ui.home

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.thankyou.appfoundation.base.ScreenFragment

class HomeAdapter(fragment: Fragment, private val screens: List<ScreenFragment>) :
  FragmentStateAdapter(fragment) {

  override fun getItemCount(): Int {
    return screens.size
  }

  override fun createFragment(position: Int): ScreenFragment {
    return screens[position]
  }
}
