package com.thankyou.redesign.ui.home.main

import android.os.Bundle
import android.view.View
import com.thankyou.appfoundation.base.ViewBindingFragment
import com.thankyou.appfoundation.extensions.bindViewModel
import com.thankyou.appfoundation.lib.lifecycle.withViewLifecycleOwner
import com.thankyou.redesign.R
import com.thankyou.redesign.databinding.ScreenMainBinding
import com.thankyou.redesign.ui.home.main.MainViewModel.Companion.MD_CARD
import com.thankyou.redesign.ui.home.main.MainViewModel.Companion.SCAN_HIE
import org.koin.androidx.viewmodel.ext.android.getViewModel

class MainFragment :
    ViewBindingFragment<ScreenMainBinding>(R.layout.screen_main) {

    private val vm by bindViewModel { getViewModel(clazz = MainViewModel::class) }

    override fun initializeLayoutBinding(view: View) =
        ScreenMainBinding.bind(view)

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHeader()
        setBody()
        vm.initialize()
    }

    private fun setHeader() {
        with(vm) {
            withViewLifecycleOwner {
                layout.divMainHeader.mainToolbar.setOnClickSetting {

                }
                layout.divMainHeader.mainToolbar.setOnClickNotification {

                }
            }
        }
    }

    private fun setBody() {
        with(vm) {
            withViewLifecycleOwner {
                itemGrid.observe {
                    layout.rlvIconGrid.setItems(it)
                }
                layout.rlvIconGrid.setOnItemClickListener {
                    when (it) {
                        MD_CARD -> { }
                        SCAN_HIE -> { }
                    }
                }
            }
        }
    }

    companion object {
        fun newInstance() = MainFragment()
        const val OBSERVER_REGISTER_SUCCESS = "observerRegisterSuccess"
        const val OBSERVER_SCAN_HIE = "observerScanHie"
    }
}