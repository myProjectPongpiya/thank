package com.thankyou.redesign.ui.home.profile

import android.os.Bundle
import android.view.View
import com.thankyou.appfoundation.base.ViewBindingFragment
import com.thankyou.appfoundation.extensions.bindViewModel
import com.thankyou.appfoundation.i18n.T
import com.thankyou.appfoundation.lib.lifecycle.withViewLifecycleOwner
import com.thankyou.redesign.R
import com.thankyou.redesign.databinding.ScreenProfileBinding
import org.koin.androidx.viewmodel.ext.android.getViewModel

class ProfileFragment :
    ViewBindingFragment<ScreenProfileBinding>(R.layout.screen_profile) {

    private val vm by bindViewModel { getViewModel(clazz = ProfileViewModel::class) }

    override fun initializeLayoutBinding(view: View) =
        ScreenProfileBinding.bind(view)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpToolBar()
        observerVerifyUpdateInfo()
        with(vm) {
            initialize()
            withViewLifecycleOwner {

            }
        }
    }

    private fun observerVerifyUpdateInfo() {
        withViewLifecycleOwner {
            parentFragmentManager.setFragmentResultListener(
                OBSERVER_VERIFY_UPDATE_INFO,
                this,
            ) { _, _ ->
                showToast(T.get("label_record_info_success"))
            }
        }
    }

    private fun setUpToolBar() {
        layout.toolbar.hideLeft()
        layout.toolbar.setTitle(T.get("label_profile"))
    }

    companion object {
        fun newInstance() = ProfileFragment()

        const val OBSERVER_VERIFY_UPDATE_INFO = "observerVerifyUpdateInfo"
    }
}