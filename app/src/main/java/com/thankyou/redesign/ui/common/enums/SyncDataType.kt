package com.thankyou.redesign.ui.common.enums

import com.google.gson.annotations.SerializedName

enum class SyncDataType {
    @SerializedName("ON_BOARDING")
    ON_BOARDING,
    @SerializedName("UPDATE")
    UPDATE
}