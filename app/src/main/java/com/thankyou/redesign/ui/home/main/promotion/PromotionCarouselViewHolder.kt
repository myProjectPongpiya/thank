package com.thankyou.redesign.ui.home.main.promotion

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.thankyou.appdata.repository.model.home.CampaignModel
import com.thankyou.appfoundation.extensions.setImage
import com.thankyou.appfoundation.graphics.DrImage
import com.thankyou.redesign.R
import kotlinx.android.synthetic.main.item_promotion_carousel_item.view.*

class PromotionCarouselViewHolder(
    view: View,
    private val onClickListener: (position: Int) -> Unit
) : RecyclerView.ViewHolder(view), View.OnClickListener {

    override fun onClick(v: View?) {
        onClickListener.invoke(absoluteAdapterPosition)
    }

    fun bind(promotions: CampaignModel) {
        val bnImage = DrImage.Url(
            promotions.imageUrl ?: "",
            placeholder = R.drawable.bg_banner_placeholder
        )
        bnImage.let {
            itemView.ivBackground.setImage(it)
        }
        itemView.ivBackground.setOnClickListener(this)
    }
}
