package com.thankyou.redesign.ui.base.otp

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OtpContext(
  val type: OTPType,
  val value: String
) : Parcelable
