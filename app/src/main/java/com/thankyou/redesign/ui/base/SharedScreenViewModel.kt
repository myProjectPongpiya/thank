package com.thankyou.redesign.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.thankyou.appfoundation.base.BaseViewModel
import com.thankyou.appfoundation.toolbar.DrToolbarNavigationType

abstract class SharedScreenViewModel : BaseViewModel() {

  open val screenTitle: LiveData<String> = MutableLiveData("")

  open val toolbarNavigationType: LiveData<DrToolbarNavigationType> =
    MutableLiveData(DrToolbarNavigationType.BACK)

  /**
   * Called when the activity has detected the user's press of the back key.
   *
   * Should return `true` if the back button has been handled. Otherwise
   * returning `false` would continue to defer the back button handling to the
   * default behavior.
   */
  open suspend fun onBackPressed(): Boolean {
    return false
  }
}
