package com.thankyou.redesign.ui.home.main.menu

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.thankyou.appfoundation.extensions.setImage
import kotlinx.android.synthetic.main.item_grid.view.*

internal class MenuGridViewHolder(
  val view: View,
  private val onClickListener: (position: Int) -> Unit,
) : RecyclerView.ViewHolder(view), View.OnClickListener {

  override fun onClick(v: View?) {
    onClickListener.invoke(absoluteAdapterPosition)
  }

  fun bindIcon(item: MenuGridItem) {
    itemView.apply {
      tvRefId.text = item.title
      ivIcon.setImage(item.icon)
    }
    itemView.setOnClickListener(this)
  }
}
