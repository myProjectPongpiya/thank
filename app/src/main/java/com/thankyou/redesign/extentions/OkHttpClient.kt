package com.thankyou.redesign.extentions

import com.thankyou.redesign.BuildConfig
import java.net.URI
import okhttp3.CertificatePinner
import okhttp3.OkHttpClient

fun OkHttpClient.Builder.addSslPinner(
    url: String,
    pinRoot: String,
    pinIntermediate: String,
    pinLeaf: String
): OkHttpClient.Builder {
    val uri = URI.create(url)
    val pattern = uri.host
    if (uri.scheme == "https") {
        when (BuildConfig.FLAVOR) {
            "sit", "uat", "production" -> {
                val certificatePinner = CertificatePinner.Builder()
                    .add(pattern, pinRoot)
                    .add(pattern, pinIntermediate)
                    .add(pattern, pinLeaf)
                    .build()
                this.certificatePinner(certificatePinner)
            }
        }
    }
    return this
}
