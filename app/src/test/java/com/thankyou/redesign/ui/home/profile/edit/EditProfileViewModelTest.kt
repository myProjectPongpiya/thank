package com.thankyou.redesign.ui.home.profile.edit

import com.google.common.truth.Truth.assertThat
import com.thankyou.appdata.repository.model.profile.DistrictModel
import com.thankyou.appdata.repository.model.profile.ProvinceModel
import com.thankyou.appdata.repository.model.profile.SubDistrictModel
import com.thankyou.appdata.repository.response.profile.DistrictResponse
import com.thankyou.appdata.repository.response.profile.ProvinceResponse
import com.thankyou.appdata.repository.response.profile.SubDistrictResponse
import com.thankyou.appdomain.usecase.profile.GetDistrictUseCase
import com.thankyou.appdomain.usecase.profile.GetInquiryDoctorInfoUseCase
import com.thankyou.appdomain.usecase.profile.GetProvinceUseCase
import com.thankyou.appdomain.usecase.profile.GetSubDistrictUseCase
import com.thankyou.appdomain.usecase.wallet.GetWalletUseCase
import com.thankyou.appfoundation.flatlist.DrFlatListItem
import com.thankyou.appfoundation.wellet.WalletService
import com.thankyou.apptestutils.BaseViewModelTests
import com.thankyou.apptestutils.extensions.thenReturnFlow
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

class EditProfileViewModelTest : BaseViewModelTests<EditProfileViewModel>() {

    @Mock
    private lateinit var getProvinceUseCase: GetProvinceUseCase

    @Mock
    private lateinit var getDistrictUseCase: GetDistrictUseCase

    @Mock
    private lateinit var getSubDistrictUseCase: GetSubDistrictUseCase

    @Mock
    private lateinit var getInquiryDoctorInfoUseCase: GetInquiryDoctorInfoUseCase

    @Mock
    private lateinit var getWalletUseCase: GetWalletUseCase

    @Mock
    private lateinit var walletService: WalletService

    override fun createViewModel() =
        EditProfileViewModel(
            getProvinceUseCase = getProvinceUseCase,
            getDistrictUseCase = getDistrictUseCase,
            getSubDistrictUseCase = getSubDistrictUseCase,
            getInquiryDoctorInfoUseCase = getInquiryDoctorInfoUseCase,
            getWalletUseCase = getWalletUseCase,
            walletService = walletService
        )

    @Before
    fun setUp() {
        vm.listProvince.observeForever { }
        vm.listDistrict.observeForever { }
        vm.listSubDistrict.observeForever { }
    }

    @Test
    fun `should get province list when call api success`() {
        whenever(getProvinceUseCase.build(any())).thenReturnFlow(mockProvinceResponse)
        vm.initialize()
        assertThat(vm.listProvince.value).isEqualTo(listProvinceResponse())
    }

    @Test
    fun `should get district list when call api province success`() {
        whenever(getDistrictUseCase.build(any())).thenReturnFlow(mockDistrictResponse)
        vm.onSelectedProvince(DrFlatListItem.Item(id = "0", title = "String"))
        assertThat(vm.listDistrict.value).isEqualTo(listDistrictResponse())
    }

    @Test
    fun `should get sub district list when call api district success`() {
        whenever(getSubDistrictUseCase.build(any())).thenReturnFlow(mockSubDistrictResponse)
        vm.onSelectedDistrict(DrFlatListItem.Item(id = "0", title = "String"))
        assertThat(vm.listSubDistrict.value).isEqualTo(listSubDistrictResponse())
    }

    private val mockProvinceResponse = ProvinceResponse(
        content = ProvinceModel(
            provinces = listOf(
                ProvinceModel.Provinces(
                    code = "0",
                    name = "String"
                ),
                ProvinceModel.Provinces(
                    code = "1",
                    name = "String"
                ),
                ProvinceModel.Provinces(
                    code = "2",
                    name = "String"
                )
            )
        )
    )

    private val mockDistrictResponse = DistrictResponse(
        content = DistrictModel(
            districts = listOf(
                DistrictModel.Districts(
                    code = "0",
                    name = "String"
                ),
                DistrictModel.Districts(
                    code = "1",
                    name = "String"
                ),
                DistrictModel.Districts(
                    code = "2",
                    name = "String"
                )
            )
        )
    )

    private val subDistrict = SubDistrictModel.SubDistricts(
        code = "0",
        name = "String"
    )

    private val mockSubDistrictResponse = SubDistrictResponse(
        content = SubDistrictModel(
            subDistricts = listOf(
                subDistrict.copy(
                    code = "1"
                ),
                subDistrict.copy(
                    code = "2"
                ),
                subDistrict.copy(
                    code = "3"
                )
            )
        )
    )

    private fun listProvinceResponse(): List<DrFlatListItem> {
        return listOf(
            DrFlatListItem.Item(id = "0", title = "String"),
            DrFlatListItem.Item(id = "1", title = "String"),
            DrFlatListItem.Item(id = "2", title = "String")
        )
    }

    private fun listDistrictResponse(): List<DrFlatListItem> {
        return listOf(
            DrFlatListItem.Item(id = "0", title = "String"),
            DrFlatListItem.Item(id = "1", title = "String"),
            DrFlatListItem.Item(id = "2", title = "String")
        )
    }

    private fun listSubDistrictResponse(): List<DrFlatListItem> {
        return listOf(
            DrFlatListItem.Item(id = "0", title = "String"),
            DrFlatListItem.Item(id = "1", title = "String"),
            DrFlatListItem.Item(id = "2", title = "String")
        )
    }
}