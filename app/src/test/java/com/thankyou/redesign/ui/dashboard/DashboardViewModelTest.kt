package com.thankyou.redesign.ui.dashboard

import android.content.Context
import com.google.common.truth.Truth.assertThat
import com.thankyou.appdata.repository.model.dashboard.TmcResponseModel
import com.thankyou.appdata.repository.response.dashboard.TmcResponse
import com.thankyou.appdomain.usecase.dashboard.GetStatusUseCase
import com.thankyou.appdomain.usecase.dashboard.GetTmcUseCase
import com.thankyou.appdomain.usecase.state.GetStateUseCase
import com.thankyou.apptestutils.BaseViewModelTests
import com.thankyou.apptestutils.extensions.thenReturnFlow
import com.thankyou.redesign.R
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

class DashboardViewModelTest: BaseViewModelTests<DashboardViewModel>() {

    @Mock
    private lateinit var getTmcUseCase: GetTmcUseCase
    private lateinit var getStateUseCase: GetStateUseCase
    private lateinit var getStatusUseCase: GetStatusUseCase
    private lateinit var context: Context

    override fun createViewModel() =
        DashboardViewModel(
            getTmcUseCase = getTmcUseCase,
            getStateUseCase = getStateUseCase,
            getStatusUseCase = getStatusUseCase,
            context = context
        )

    @Before
    fun setUp() {
        vm.dashboardUrl.observeForever {  }
    }


    @Test
    fun `should get tmc link when call api success`() {
        whenever(getTmcUseCase.build(any())).thenReturnFlow(mockTmcResponse)
        vm.initialize()
        assertThat(vm.dashboardUrl.value).isEqualTo("String")
    }

    @Test
    fun `should navigate to sign in screen`() {
        vm.gotoLoginPage()
        assertThat(vm.gotoPage.value?.actionId).isEqualTo(R.id.gotoLoginByPaotangFragment)
    }

    val mockTmcResponse = TmcResponse(
        content = TmcResponseModel(
            url = "String"
        )
    )
}