package com.thankyou.appdata.repository.response.loginHie

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.loginHie.LoginHieModel

data class LoginHieResponse(
    @SerializedName("content")
    val content: LoginHieModel
)
