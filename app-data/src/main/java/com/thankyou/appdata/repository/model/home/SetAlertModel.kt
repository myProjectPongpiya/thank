package com.thankyou.appdata.repository.model.home

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SetAlertModel(
    @SerializedName("isNoti")
    val isNotification: Boolean
) : Parcelable
