package com.thankyou.appdata.repository.response.dashboard

import com.google.gson.annotations.SerializedName

data class MDEServiceRequest(
    @SerializedName("codecpe")
    val codecpe: String,
    @SerializedName("pin")
    val pin: String,
    @SerializedName("firebaseToken")
    val firebaseToken: String
)
