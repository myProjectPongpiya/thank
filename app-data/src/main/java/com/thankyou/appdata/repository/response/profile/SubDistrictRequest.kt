package com.thankyou.appdata.repository.response.profile

import com.google.gson.annotations.SerializedName

data class SubDistrictRequest(
  @SerializedName("districtCode")
  val districtCode: String? = null
)
