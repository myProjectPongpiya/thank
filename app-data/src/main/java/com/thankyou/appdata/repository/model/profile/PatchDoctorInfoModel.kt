package com.thankyou.appdata.repository.model.profile

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PatchDoctorInfoModel(
    @SerializedName("titleTh")
    val titleTh: String? = null,
    @SerializedName("firstNameTh")
    val firstNameTh: String? = null,
    @SerializedName("lastNameTh")
    val lastNameTh: String? = null,
    @SerializedName("doctorID")
    val doctorID: String? = null,
    @SerializedName("cID")
    val cID: String? = null,
    @SerializedName("doctorImg")
    val doctorImg: String? = null,
    @SerializedName("licenseYear")
    val licenseYear: String? = null,
    @SerializedName("licenseDate")
    val licenseDate: String? = null,
    @SerializedName("suffixEn")
    val suffixEn: String? = null,
    @SerializedName("firstNameEn")
    val firstNameEn: String? = null,
    @SerializedName("lastNameEn")
    val lastNameEn: String? = null,
    @SerializedName("email")
    val email: String? = null,
    @SerializedName("mobileOtp")
    val mobileOtp: String? = null,
    @SerializedName("urlQrCode")
    val urlQrCode: String? = null,
    @SerializedName("statusLoginMdConnect")
    val statusLoginMdConnect: Boolean,
    @SerializedName("graduateInstitute")
    val graduateInstitute: String? = null,
    @SerializedName("statusActiveMd")
    val statusActiveMd: Boolean,
    @SerializedName("statusPinCodeMd")
    val statusPinCodeMd: Boolean,
    @SerializedName("isPinCodeMd")
    val isPinCodeMd: Boolean,
    @SerializedName("activeMessage")
    val activeMessage: ActiveMessage,
    @SerializedName("branchSpecialization")
    val branchSpecialization: List<BranchSpecialization>? = null,
    @SerializedName("address")
    val address: Address,
    @SerializedName("otherSkill")
    val otherSkill: List<OtherSkill>? = null
) : Parcelable {

    @Parcelize
    data class ActiveMessage(
        @SerializedName("live")
        val live: Active,
        @SerializedName("ethic")
        val ethic: Active,
        @SerializedName("active")
        val active: Active
    ) : Parcelable

    @Parcelize
    data class BranchSpecialization(
        @SerializedName("sname_th")
        val snameTh: String? = null,
        @SerializedName("sname_en")
        val snameEn: String? = null,
        @SerializedName("prectition_code")
        val prectitionCode: String? = null
    ) : Parcelable

    @Parcelize
    data class Address(
        @SerializedName("addressId")
        val addressId: String? = null,
        @SerializedName("addressType")
        val addressType: String? = null,
        @SerializedName("addressPlace")
        val addressPlace: String? = null,
        @SerializedName("addressNo")
        val addressNo: String? = null,
        @SerializedName("addressSoi")
        val addressSoi: String? = null,
        @SerializedName("addressSubDistrictID")
        val addressSubDistrictID: String? = null,
        @SerializedName("addressSubDistrictName")
        val addressSubDistrictName: String? = null,
        @SerializedName("addressDistrictID")
        val addressDistrictID: String? = null,
        @SerializedName("addressDistrictName")
        val addressDistrictName: String? = null,
        @SerializedName("addressProvinceID")
        val addressProvinceID: String? = null,
        @SerializedName("addressProvinceName")
        val addressProvinceName: String? = null,
        @SerializedName("addressPostCode")
        val addressPostCode: String? = null,
        @SerializedName("addressContractPhone")
        val addressContractPhone: String? = null
    ) : Parcelable

    @Parcelize
    data class OtherSkill(
        @SerializedName("otherSkillTh")
        val otherSkillTh: String? = null,
        @SerializedName("otherSkillEn")
        val otherSkillEn: String? = null,
        @SerializedName("otherSkillStartDate")
        val otherSkillStartDate: String? = null,
        @SerializedName("otherSkillEndDate")
        val otherSkillEndDate: String? = null
    ) : Parcelable

    @Parcelize
    data class Active(
        @SerializedName("code")
        val code: String? = null,
        @SerializedName("label")
        val label: String? = null
    ) : Parcelable
}