package com.thankyou.appdata.repository.response.wallet

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.wallet.GetWalletModel

data class GetWalletResponse(
    @SerializedName("content")
    val content: GetWalletModel
)
