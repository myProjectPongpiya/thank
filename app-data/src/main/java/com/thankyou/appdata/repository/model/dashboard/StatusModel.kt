package com.thankyou.appdata.repository.model.dashboard

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class StatusModel(
    @SerializedName("term")
    val termAndCondition: Boolean? = false,
    @SerializedName("consent")
    val consent: Boolean? = false,
) : Parcelable
