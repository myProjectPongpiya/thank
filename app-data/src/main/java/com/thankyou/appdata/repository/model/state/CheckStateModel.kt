package com.thankyou.appdata.repository.model.state

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import org.threeten.bp.ZonedDateTime

@Parcelize
data class CheckStateModel(
    @SerializedName("currentState")
    val currentState: UserState,
    @SerializedName("paotangExpireDate")
    val paotangExpireDate: ZonedDateTime? = null
) : Parcelable
