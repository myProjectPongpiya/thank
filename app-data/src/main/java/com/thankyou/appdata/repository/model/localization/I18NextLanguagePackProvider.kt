package com.thankyou.appdata.repository.model.localization

import com.google.gson.Gson
import com.i18next.android.I18Next
import com.i18next.android.Operation
import com.thankyou.appfoundation.data.CommonContext
import com.thankyou.appfoundation.extensions.tryOrNull
import com.thankyou.appfoundation.i18n.LanguagePackProvider
import com.thankyou.appfoundation.i18n.models.LanguagePack
import com.thankyou.appfoundation.i18n.models.SupportedLanguage

class I18NextLanguagePackProvider(
  private val gson: Gson,
  private val resource: LanguagePackResource,
  private var commonContext: CommonContext,
) : LanguagePackProvider {

  private val i18Next = I18Next()

  override fun initialize() {
    loadCurrentLanguagePack()?.let { i18Next.load(it) }
  }

  override fun get(key: String, operation: Operation?): String? {
    return i18Next.t(key, operation)
  }

  override fun get(key: String, language: SupportedLanguage, operation: Operation?): String? {
    val prevLang = i18Next.options.language
    i18Next.options.language = language.toString()
    return get(key, operation).also {
      i18Next.options.language = prevLang
    }
  }

  override fun setLanguage(language: SupportedLanguage) {
    commonContext.appLanguage = language
    i18Next.options.language = language.toString()
  }

  override fun getLanguage(): SupportedLanguage {
    return commonContext.appLanguage
  }

  private fun loadCurrentLanguagePack(): LanguagePack? = tryOrNull {
    loadLanguagePackFromBundledResource()
  }

  private fun loadLanguagePackFromBundledResource() = try {
    resource.get()
  } catch (t: Throwable) {
    // This technically should never happen, the bundled LanguagePack should always be working
    null
  }

  /**
   * Loads the [languagePack] into [I18Next].
   */
  private fun I18Next.load(languagePack: LanguagePack) {
    languagePack.translations.forEach { (key, _) ->
      tryOrNull {
        val lang = SupportedLanguage.fromIso639_1(key)?.toString() ?: return@tryOrNull
        this.loader()
          .lang(lang)
          .from(languagePack.forLanguageAsJson(key, gson))
          .load()
      }
    }
  }
}
