package com.thankyou.appdata.repository.response.pending

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.pending.RetrieveDataModel

data class RetrieveDataResponse(
  @SerializedName("content")
  val content: RetrieveDataModel
)