package com.thankyou.appdata.repository.model.home

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CreateMdCardModel(
    @SerializedName("card_status")
    val cardStatus: Int? = null,
    @SerializedName("card_license_code")
    val cardLicenseCode: String? = null,
    @SerializedName("card_img")
    val cardImg: String? = null,
    @SerializedName("card_qrcode")
    val cardQrCode: String? = null,
    @SerializedName("card_license_date")
    val cardLicenseDate: String? = null,
    @SerializedName("card_issue_date")
    val cardIssueDate: String? = null,
    @SerializedName("card_expire_date")
    val cardExpireDate: String? = null,
    @SerializedName("card_number")
    val cardNumber: String? = null,
    @SerializedName("card_title")
    val cardTitle: String? = null,
    @SerializedName("card_name")
    val cardName: String? = null,
    @SerializedName("card_surename")
    val cardSurename: String? = null,
    @SerializedName("card_name_en")
    val cardNameEn: String? = null,
    @SerializedName("card_surename_en")
    val cardSurenameEn: String? = null,
    @SerializedName("card_pid")
    val cardPid: String? = null,
    @SerializedName("card_bdate")
    val cardBDate: String? = null,
    @SerializedName("card_address")
    val cardAddress: String? = null,
    @SerializedName("active")
    val active: String? = null,
    @SerializedName("active_msg")
    val activeMsg: ActiveMsg,
    @SerializedName("card_suffix_en")
    val cardSuffixEn: String? = null
) : Parcelable {

    @Parcelize
    data class ActiveMsg(
        @SerializedName("ACTIVE")
        val active: Active,
        @SerializedName("ETHICS")
        val ethics: Ethics,
        @SerializedName("LIVE")
        val live: Live
    ) : Parcelable {

        @Parcelize
        data class Active(
            @SerializedName("code")
            val code: String? = null,
            @SerializedName("label")
            val label: String? = null
        ) : Parcelable

        @Parcelize
        data class Ethics(
            @SerializedName("code")
            val code: String? = null,
            @SerializedName("label")
            val label: String? = null
        ) : Parcelable

        @Parcelize
        data class Live(
            @SerializedName("code")
            val code: String? = null,
            @SerializedName("label")
            val label: String? = null
        ) : Parcelable
    }
}