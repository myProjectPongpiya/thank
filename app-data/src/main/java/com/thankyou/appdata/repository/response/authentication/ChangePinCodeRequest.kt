package com.thankyou.appdata.repository.response.authentication

import com.google.gson.annotations.SerializedName

data class ChangePinCodeRequest(
    @SerializedName("newPinCode")
    val newPinCode: String,
    @SerializedName("oldPinCode")
    val oldPinCode: String,
    @SerializedName("tokenUuid")
    val tokenUuid: String? = null
)