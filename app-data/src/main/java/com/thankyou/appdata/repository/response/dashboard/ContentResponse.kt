package com.thankyou.appdata.repository.response.dashboard

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.dashboard.ContentModel

data class ContentResponse(
    @SerializedName("content")
    val content: ContentModel,
)
