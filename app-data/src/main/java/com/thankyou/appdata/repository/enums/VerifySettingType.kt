package com.thankyou.appdata.repository.enums

enum class VerifySettingType {
    OLD_PIN,
    MASKING,
    EDIT_PROFILE
}