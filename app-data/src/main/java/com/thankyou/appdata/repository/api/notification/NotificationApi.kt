package com.thankyou.appdata.repository.api.notification

import com.thankyou.appdata.repository.response.notifications.NotificationIsConfirmRequest
import com.thankyou.appdata.repository.response.notifications.NotificationIsConfirmResponse
import com.thankyou.appdata.repository.response.notifications.NotificationIsReadRequest
import com.thankyou.appdata.repository.response.notifications.NotificationIsReadResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface NotificationApi {
    @POST("notification/api/v1/notification-is-read")
    suspend fun getNotificationIsRead(@Body request: NotificationIsReadRequest): NotificationIsReadResponse

    @POST("notification/api/v1/notification-is-confirm")
    suspend fun getNotificationIsConfirm(@Body request: NotificationIsConfirmRequest): NotificationIsConfirmResponse
}