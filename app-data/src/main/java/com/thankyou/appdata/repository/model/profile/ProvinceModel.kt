package com.thankyou.appdata.repository.model.profile

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProvinceModel(
  @SerializedName("provinces")
  val provinces: List<Provinces>? = null
) : Parcelable {

    @Parcelize
    data class Provinces(
      @SerializedName("code")
      val code: String,
      @SerializedName("name")
      val name: String? = null,
    ) : Parcelable
}
