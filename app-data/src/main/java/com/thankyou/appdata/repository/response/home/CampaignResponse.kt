package com.thankyou.appdata.repository.response.home

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.home.CampaignModel

data class CampaignResponse(
  @SerializedName("content")
  val content: List<CampaignModel>
)
