package com.thankyou.appdata.repository.response.wallet

import com.google.gson.annotations.SerializedName

data class GetWalletInvitationRequest(
    @SerializedName("walletID")
    val walletID: String? = null
)
