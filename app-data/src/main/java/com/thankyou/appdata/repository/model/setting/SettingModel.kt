package com.thankyou.appdata.repository.model.setting

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SettingModel(
    @SerializedName("isEnableNotification")
    val isEnableNotification: Boolean? = false,
    @SerializedName("isEnableBiometric")
    val isEnableBiometric: Boolean? = false,
    @SerializedName("isMaskingCid")
    val isMaskingCid: Boolean? = true,
    @SerializedName("isMaskingMobile")
    val isMaskingMobile: Boolean? = true,
    @SerializedName("isMaskingEmail")
    val isMaskingEmail: Boolean? = true
) : Parcelable
