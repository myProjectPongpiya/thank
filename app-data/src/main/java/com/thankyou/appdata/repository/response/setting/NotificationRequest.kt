package com.thankyou.appdata.repository.response.setting

import com.google.gson.annotations.SerializedName

data class NotificationRequest(
    @SerializedName("isEnableNotification")
    val isEnableNotification: Boolean? = true,
)
