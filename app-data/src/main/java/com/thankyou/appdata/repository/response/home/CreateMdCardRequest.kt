package com.thankyou.appdata.repository.response.home

import com.google.gson.annotations.SerializedName

data class CreateMdCardRequest(
  @SerializedName("doctorId")
  val doctorId: String
)
