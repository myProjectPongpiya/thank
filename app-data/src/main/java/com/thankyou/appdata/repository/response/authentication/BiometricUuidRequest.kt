package com.thankyou.appdata.repository.response.authentication

import com.google.gson.annotations.SerializedName

data class BiometricUuidRequest(
  @SerializedName("isEnableBiometric")
  val isEnableBiometric: Boolean? = true,
  @SerializedName("isCheckDoctor")
  val isCheckDoctor: Boolean? = false,
)
