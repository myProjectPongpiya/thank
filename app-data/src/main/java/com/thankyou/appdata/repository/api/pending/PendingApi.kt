package com.thankyou.appdata.repository.api.pending

import com.thankyou.appdata.repository.response.pending.RetrieveDataRequest
import retrofit2.http.Body
import retrofit2.http.POST

interface PendingApi {
    @POST("onboard/api/v1/retreive-data")
    suspend fun getRetrieveData(@Body request: RetrieveDataRequest)
}
