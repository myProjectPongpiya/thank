package com.thankyou.appdata.repository.response.wallet

import com.google.gson.annotations.SerializedName

data class GetWalletInvitationForUpdateRequest(
    @SerializedName("walletID")
    val walletID: String? = null,
    @SerializedName("requestId")
    val requestId: String? = null,
    @SerializedName("gender")
    val gender: String? = null,
    @SerializedName("birthDate")
    val birthDate: String? = null,
    @SerializedName("credentialReferentId")
    val credentialReferentId: String? = null
)
