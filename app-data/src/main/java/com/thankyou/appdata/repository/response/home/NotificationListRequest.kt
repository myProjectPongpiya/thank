package com.thankyou.appdata.repository.response.home

import com.google.gson.annotations.SerializedName

data class NotificationListRequest(
  @SerializedName("page")
  val page: String? = "1"
)
