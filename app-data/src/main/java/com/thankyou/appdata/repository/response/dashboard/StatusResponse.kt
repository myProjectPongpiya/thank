package com.thankyou.appdata.repository.response.dashboard

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.dashboard.StatusModel

data class StatusResponse(
    @SerializedName("content")
    val content: StatusModel,
)
