package com.thankyou.appdata.repository.api.setting

import com.thankyou.appdata.repository.model.setting.MaskingModel
import com.thankyou.appdata.repository.response.setting.ContractResponse
import com.thankyou.appdata.repository.response.setting.NotificationRequest
import com.thankyou.appdata.repository.response.setting.SettingResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface SettingApi {
    @POST("auth/api/v1/setting")
    suspend fun getSetting(): SettingResponse

    @POST("auth/api/v1/available-notification")
    suspend fun setNotification(@Body request: NotificationRequest)

    @POST("inquiry/api/v1/contact")
    suspend fun getContract(): ContractResponse

    @POST("auth/api/v1/masking")
    suspend fun setMasking(@Body request: MaskingModel)
}