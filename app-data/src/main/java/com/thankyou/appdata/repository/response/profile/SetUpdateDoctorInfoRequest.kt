package com.thankyou.appdata.repository.response.profile

import com.google.gson.annotations.SerializedName

data class SetUpdateDoctorInfoRequest(
    @SerializedName("credentialReferentId")
    val credentialReferentId: String? = null
)
