package com.thankyou.appdata.repository.model.loginHie

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LoginHieModel(
    @SerializedName("invitationUrl")
    val invitationUrl: String? = null,
    @SerializedName("issuerName")
    val issuerName: String? = null,
    @SerializedName("credManID")
    val credManID: String? = null,
    @SerializedName("credentialName")
    val credentialName: String? = null,
) : Parcelable
