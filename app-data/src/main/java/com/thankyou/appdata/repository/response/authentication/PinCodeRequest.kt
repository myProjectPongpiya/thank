package com.thankyou.appdata.repository.response.authentication

import com.google.gson.annotations.SerializedName

data class PinCodeRequest(
    @SerializedName("tokenUuid")
    val tokenUuid: String,
    @SerializedName("pincode")
    val pincode: String,
)
