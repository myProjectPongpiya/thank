package com.thankyou.appdata.repository.response.setting

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.setting.SettingModel

data class SettingResponse(
    @SerializedName("content")
    val content: SettingModel
)
