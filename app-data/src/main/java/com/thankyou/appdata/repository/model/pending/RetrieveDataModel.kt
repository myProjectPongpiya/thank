package com.thankyou.appdata.repository.model.pending

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RetrieveDataModel(
    @SerializedName("cide")
    var cide: String? = null,
    @SerializedName("thaiTitle")
    var thaiTitle: String? = null,
    @SerializedName("thaiFirstName")
    var thaiFirstName: String? = null,
    @SerializedName("thaiMiddleName")
    var thaiMiddleName: String? = null,
    @SerializedName("thaiLastName")
    var thaiLastName: String? = null,
    @SerializedName("engFirstName")
    var engFirstName: String? = null,
    @SerializedName("engMiddleName")
    var engMiddleName: String? = null,
    @SerializedName("engLastName")
    var engLastName: String? = null,
    @SerializedName("gender")
    var gender: String? = null,
    @SerializedName("lastNameEn")
    var lastNameEn: String? = null,
    @SerializedName("birthDate")
    var birthDate: String? = null
) : Parcelable