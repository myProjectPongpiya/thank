package com.thankyou.appdata.repository.enums

import com.google.gson.annotations.SerializedName

enum class IsReadType {
    @SerializedName("Y")
    Y,
    @SerializedName("N")
    N
}