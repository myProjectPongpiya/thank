package com.thankyou.appdata.repository.model.wallet

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetWalletInvitationModel(
    @SerializedName("invitationUrl")
    val invitationUrl: String? = null,
    @SerializedName("credmanID")
    val credManID: String? = null,
    @SerializedName("credDefID")
    val credDefID: String? = null,
    @SerializedName("credentialName")
    val credentialName: String? = null,
    @SerializedName("issuerName")
    val issuerName: String? = null,
) : Parcelable
