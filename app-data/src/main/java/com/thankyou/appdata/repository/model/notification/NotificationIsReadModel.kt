package com.thankyou.appdata.repository.model.notification

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NotificationIsReadModel(
    @SerializedName("data")
    val data: String? = null
) : Parcelable