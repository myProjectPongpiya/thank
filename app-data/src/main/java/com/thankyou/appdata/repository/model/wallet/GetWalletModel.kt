package com.thankyou.appdata.repository.model.wallet

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetWalletModel(
    @SerializedName("walletKey")
    val walletKey: String? = null
) : Parcelable
