package com.thankyou.appdata.repository.response.wallet

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.wallet.GetWalletInvitationModel

data class GetWalletInvitationResponse(
    @SerializedName("content")
    val content: GetWalletInvitationModel
)
