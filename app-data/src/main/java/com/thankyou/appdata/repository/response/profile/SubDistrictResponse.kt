package com.thankyou.appdata.repository.response.profile

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.profile.SubDistrictModel

data class SubDistrictResponse(
  @SerializedName("content")
  val content: SubDistrictModel
)
