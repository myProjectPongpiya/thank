package com.thankyou.appdata.repository.response.authentication

import com.google.gson.annotations.SerializedName

data class VerifyPinCodeRequest(
  @SerializedName("pincode") val pincode: String
)
