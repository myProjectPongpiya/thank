package com.thankyou.appdata.repository.response.dashboard

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.dashboard.TmcResponseModel

data class TmcResponse(
  @SerializedName("content")
  val content: TmcResponseModel
)
