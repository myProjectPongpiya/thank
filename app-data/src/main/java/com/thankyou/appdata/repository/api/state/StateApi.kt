package com.thankyou.appdata.repository.api.state

import com.thankyou.appdata.repository.response.state.SetStateRequest
import com.thankyou.appdata.repository.response.state.CheckStateResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface StateApi {

    @POST("onboard/api/v1/set-state")
    suspend fun setState(@Body request: SetStateRequest)

    @POST("onboard/api/v1/check-state")
    suspend fun getState(): CheckStateResponse
}
