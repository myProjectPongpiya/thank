package com.thankyou.appdata.repository.response.home

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.home.CreateMdCardModel

data class CreateMdCardResponse(
  @SerializedName("content")
  val content: CreateMdCardModel
)
