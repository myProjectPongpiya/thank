package com.thankyou.appdata.repository.response.profile

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.profile.OtpSendRequestModel

data class OtpResponse(
    @SerializedName("content")
    val content: OtpSendRequestModel
)
