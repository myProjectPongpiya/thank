package com.thankyou.appdata.repository.response.profile

import com.google.gson.annotations.SerializedName

data class InquiryDoctorInfoRequest(
    @SerializedName("doctorId")
    val doctorId: String? = null
)
