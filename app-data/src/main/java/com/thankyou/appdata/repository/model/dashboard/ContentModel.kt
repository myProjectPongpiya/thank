package com.thankyou.appdata.repository.model.dashboard

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ContentModel(
    @SerializedName("content")
    val content: String,
) : Parcelable
