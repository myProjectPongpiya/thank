package com.thankyou.appdata.repository.model.notification

import com.google.gson.annotations.SerializedName

enum class LoginHieState {
    @SerializedName("SUCCESS")
    SUCCESS,

    @SerializedName("FALSE_HIE")
    FALSE_HIE,
}