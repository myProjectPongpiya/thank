package com.thankyou.appdata.repository.model.localization

import android.content.Context
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.thankyou.appdata.R
import com.thankyou.appfoundation.i18n.models.LanguagePack
import java.io.BufferedReader
import java.io.InputStreamReader

/**
 * [LanguagePackResource] retrieves the [LanguagePack] bundled with the app
 * during compilation.
 *
 * The default [LanguagePack] should be an Android [resourceId] belonging to the
 * language pack JSON file.
 */
class LanguagePackResource(
  private val context: Context,
  private val gson: Gson
) {

  /**
   * Returns the [LanguagePack] bundled with the app during compilation.
   *
   * @throws JsonSyntaxException when parsing of the language pack data fails.
   */
  @Throws(JsonSyntaxException::class)
  fun get(): LanguagePack {
    val inputStream = context.resources.openRawResource(R.raw.language_pack)
    val reader = BufferedReader(InputStreamReader(inputStream))
    return gson.fromJson(reader, LanguagePack::class.java)
  }
}
