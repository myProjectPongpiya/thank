package com.thankyou.appdata.repository.response.profile

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.profile.ProvinceModel

data class ProvinceResponse(
  @SerializedName("content")
  val content: ProvinceModel
)