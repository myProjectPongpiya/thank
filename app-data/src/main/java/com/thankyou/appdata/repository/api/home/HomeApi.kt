package com.thankyou.appdata.repository.api.home

import com.thankyou.appdata.repository.response.home.CampaignResponse
import com.thankyou.appdata.repository.response.home.CreateMdCardRequest
import com.thankyou.appdata.repository.response.home.CreateMdCardResponse
import com.thankyou.appdata.repository.response.home.NotificationAlertResponse
import com.thankyou.appdata.repository.response.home.NotificationListRequest
import com.thankyou.appdata.repository.response.home.NotificationListResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface HomeApi {
    @POST("inquiry/api/v1/campaign-doctor-list")
    suspend fun getCampaign(): CampaignResponse

    @POST("notification/api/v1/notification-list")
    suspend fun getNotificationList(@Body request: NotificationListRequest): NotificationListResponse

    @POST("notification/api/v1/notification-alert")
    suspend fun getIsNotification(): NotificationAlertResponse

    @POST("doctor/api/v1/create-mdcard")
    suspend fun createMdCard(@Body request: CreateMdCardRequest): CreateMdCardResponse
}
