package com.thankyou.appdata.repository.model.setting

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ContractModel(
    @SerializedName("address")
    val address: String? = null,
    @SerializedName("urlMap")
    val urlMap: String? = null,
    @SerializedName("serviceDate")
    val serviceDate: String? = null,
    @SerializedName("contact")
    val contact: List<ContractDetailsModel>? = null
) : Parcelable {

    @Parcelize
    data class ContractDetailsModel(
        @SerializedName("branchName")
        val branchName: String? = null,
        @SerializedName("mobileNo")
        val mobileNo: List<String>? = null,
        @SerializedName("email")
        val email: String? = null,
    ) : Parcelable
}
