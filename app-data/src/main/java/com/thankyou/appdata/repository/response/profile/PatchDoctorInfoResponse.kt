package com.thankyou.appdata.repository.response.profile

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.profile.PatchDoctorInfoModel

data class PatchDoctorInfoResponse(
    @SerializedName("content")
    val content: PatchDoctorInfoModel
)
