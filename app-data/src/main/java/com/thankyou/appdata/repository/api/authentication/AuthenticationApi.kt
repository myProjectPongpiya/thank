package com.thankyou.appdata.repository.api.authentication

import com.thankyou.appdata.repository.response.authentication.BiometricUuidRequest
import com.thankyou.appdata.repository.response.authentication.BiometricUuidResponse
import com.thankyou.appdata.repository.response.authentication.ChangePinCodeRequest
import com.thankyou.appdata.repository.response.authentication.ForgotPinCodeRequest
import com.thankyou.appdata.repository.response.authentication.NotificationTokenRequest
import com.thankyou.appdata.repository.response.authentication.PinCodeRequest
import com.thankyou.appdata.repository.response.authentication.VerifyBiometricsRequest
import com.thankyou.appdata.repository.response.authentication.VerifyPinCodeRequest
import com.thankyou.appfoundation.authentication.RefreshTokenRequest
import com.thankyou.appfoundation.authentication.TokenResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthenticationApi {
    @POST("auth/api/v1/available-biometric")
    suspend fun getBiometricUuid(@Body request: BiometricUuidRequest): BiometricUuidResponse

    @POST("auth/api/v1/login-pincode")
    suspend fun loginPinCode(@Body request: VerifyPinCodeRequest): TokenResponse

    @POST("auth/api/v1/verify-biometric")
    suspend fun loginBiometrics(@Body request: VerifyBiometricsRequest): TokenResponse

    @POST("auth/api/v1/set-pincode")
    suspend fun setPinCode(@Body request: PinCodeRequest)

    @POST("auth/api/v1/verify-pincode")
    suspend fun verifyPinCode(@Body request: VerifyPinCodeRequest): TokenResponse

    @POST("auth/api/v1/refresh-token")
    suspend fun refreshPinToken(@Body request: RefreshTokenRequest): TokenResponse

    @POST("notification/api/v1/notification-token")
    suspend fun notificationToken(@Body request: NotificationTokenRequest)

    @POST("auth/api/v1/forgot-pincode")
    suspend fun forgotPinCode(@Body request: ForgotPinCodeRequest)

    @POST("auth/api/v1/change-pincode")
    suspend fun changePinCode(@Body request: ChangePinCodeRequest)
}
