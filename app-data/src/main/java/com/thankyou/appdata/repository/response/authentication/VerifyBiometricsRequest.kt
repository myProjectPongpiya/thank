package com.thankyou.appdata.repository.response.authentication

import com.google.gson.annotations.SerializedName

data class VerifyBiometricsRequest(
  @SerializedName("key") val key: String
)
