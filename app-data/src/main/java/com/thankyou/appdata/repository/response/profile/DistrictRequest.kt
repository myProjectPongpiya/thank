package com.thankyou.appdata.repository.response.profile

import com.google.gson.annotations.SerializedName

data class DistrictRequest(
  @SerializedName("provinceCode")
  val provinceCode: String? = null
)
