package com.thankyou.appdata.repository.model.setting

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MaskingModel(
    @SerializedName("isMaskingCid")
    val isMaskingCid: Boolean,
    @SerializedName("isMaskingMobile")
    val isMaskingMobile: Boolean,
    @SerializedName("isMaskingEmail")
    val isMaskingEmail: Boolean,
    @SerializedName("tokenUuid")
    val tokenUuid: String? = ""
) : Parcelable