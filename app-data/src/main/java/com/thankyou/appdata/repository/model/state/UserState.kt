package com.thankyou.appdata.repository.model.state

import com.google.gson.annotations.SerializedName

enum class UserState {
    @SerializedName("NEW")
    NEW,

    @SerializedName("PAOTANG_RETRIEVED")
    PAOTANG_RETRIEVED,

    @SerializedName("MD_ESERVICE_RETRIEVED")
    MD_ESERVICE_RETRIEVED,

    @SerializedName("CRED_NET_PROCESSING")
    CRED_NET_PROCESSING,

    @SerializedName("DONE")
    DONE,

    @SerializedName("INACTIVE")
    INACTIVE
}