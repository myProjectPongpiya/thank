package com.thankyou.appdata.repository.response.loginHie

import com.google.gson.annotations.SerializedName

data class LoginHieRequest(
    @SerializedName("qrCode")
    val qrCode: String,
    @SerializedName("walletID")
    val walletID: String
)
