package com.thankyou.appdata.repository.response.home

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.home.NotificationListModel

data class NotificationListResponse(
  @SerializedName("content")
  val content: NotificationListModel
)
