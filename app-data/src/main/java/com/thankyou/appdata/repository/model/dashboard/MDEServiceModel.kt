package com.thankyou.appdata.repository.model.dashboard

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MDEServiceModel(
    @SerializedName("citizenId")
    var citizenId: String? = null,
    @SerializedName("doctorId")
    var doctorId: String? = null,
    @SerializedName("birthDate")
    var birthDate: String? = null,
    @SerializedName("userImage")
    var userImage: String? = null,
    @SerializedName("titleTh")
    var titleTh: String? = null,
    @SerializedName("suffixEn")
    var suffixEn: String? = null,
    @SerializedName("firstNameTh")
    var firstNameTh: String? = null,
    @SerializedName("firstNameEn")
    var firstNameEn: String? = null,
    @SerializedName("lastNameTh")
    var lastNameTh: String? = null,
    @SerializedName("lastNameEn")
    var lastNameEn: String? = null,
    @SerializedName("addr")
    var addr: Addr? = null,
    @SerializedName("qrCodeUrl")
    var qrCodeUrl: String? = null,
    @SerializedName("licenseDate")
    var licensedate: String? = null,
    @SerializedName("issueDate")
    var issueDate: String? = null,
    @SerializedName("expireDate")
    var expireDate: String? = null,
    @SerializedName("mdCardNumber")
    var mdCardNumber: String? = null
) : Parcelable {

    @Parcelize
    data class Addr(
        @SerializedName("addrID")
        var addrID: String? = null,
        @SerializedName("addrType")
        var addrType: String? = null,
        @SerializedName("addrPlace")
        var addrPlace: String? = null,
        @SerializedName("addrNo")
        var addrNo: String? = null,
        @SerializedName("addrSoi")
        var addrSoi: String? = null,
        @SerializedName("addrSubdistrictID")
        var addrSubdistrictID: String? = null,
        @SerializedName("addrSubdistrictName")
        var addrSubdistrictName: String? = null,
        @SerializedName("addrDistrictID")
        var addrDistrictID: String? = null,
        @SerializedName("addrDistrictName")
        var addrDistrictName: String? = null,
        @SerializedName("addrProvinceID")
        var addrProvinceID: String? = null,
        @SerializedName("addrProvinceName")
        var addrProvinceName: String? = null,
        @SerializedName("addrPostcode")
        var addrPostcode: String? = null,
        @SerializedName("addrContactPhone")
        var addrContactPhone: String? = null
    ) : Parcelable
}
