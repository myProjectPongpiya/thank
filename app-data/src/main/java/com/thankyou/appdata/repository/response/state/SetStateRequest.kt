package com.thankyou.appdata.repository.response.state

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.state.UserState

data class SetStateRequest(
    @SerializedName("currentState")
    val currentState: UserState
)
