package com.thankyou.appdata.repository.response.state

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.state.CheckStateModel

data class CheckStateResponse(
    @SerializedName("content")
    val content: CheckStateModel
)
