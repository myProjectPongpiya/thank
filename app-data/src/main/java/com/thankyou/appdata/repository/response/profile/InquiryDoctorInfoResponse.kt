package com.thankyou.appdata.repository.response.profile

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.profile.InquiryDoctorInfoModel

data class InquiryDoctorInfoResponse(
    @SerializedName("content")
    val content: InquiryDoctorInfoModel
)
