package com.thankyou.appdata.repository.response.profile

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.profile.DistrictModel

data class DistrictResponse(
  @SerializedName("content")
  val content: DistrictModel
)
