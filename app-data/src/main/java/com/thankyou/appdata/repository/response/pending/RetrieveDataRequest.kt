package com.thankyou.appdata.repository.response.pending

import com.google.gson.annotations.SerializedName

data class RetrieveDataRequest(
    @SerializedName("code")
    val code: String
)