package com.thankyou.appdata.repository.response.notifications

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.notification.NotificationIsConfirmModel

data class NotificationIsConfirmResponse(
    @SerializedName("content")
    val content: NotificationIsConfirmModel
)
