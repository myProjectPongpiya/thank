package com.thankyou.appdata.repository.response.authentication

import com.google.gson.annotations.SerializedName

data class NotificationTokenRequest(
    @SerializedName("firebaseToken") val firebaseToken: String
)
