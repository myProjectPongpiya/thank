package com.thankyou.appdata.repository.api.profile

import com.thankyou.appdata.repository.response.profile.ProvinceResponse
import com.thankyou.appdata.repository.response.profile.DistrictRequest
import com.thankyou.appdata.repository.response.profile.DistrictResponse
import com.thankyou.appdata.repository.response.profile.InquiryDoctorInfoRequest
import com.thankyou.appdata.repository.response.profile.InquiryDoctorInfoResponse
import com.thankyou.appdata.repository.response.profile.OtpRequest
import com.thankyou.appdata.repository.response.profile.OtpResponse
import com.thankyou.appdata.repository.response.profile.OtpVerifyRequest
import com.thankyou.appdata.repository.response.profile.PatchDoctorInfoRequest
import com.thankyou.appdata.repository.response.profile.PatchDoctorInfoResponse
import com.thankyou.appdata.repository.response.profile.SetUpdateDoctorInfoRequest
import com.thankyou.appdata.repository.response.profile.SubDistrictRequest
import com.thankyou.appdata.repository.response.profile.SubDistrictResponse
import retrofit2.http.Body
import retrofit2.http.PATCH
import retrofit2.http.POST

interface ProfileApi {
    @POST("inquiry/api/v1/province")
    suspend fun getProvince(): ProvinceResponse

    @POST("inquiry/api/v1/districts")
    suspend fun getDistrict(@Body request: DistrictRequest): DistrictResponse

    @POST("inquiry/api/v1/sub-district")
    suspend fun getSubDistrict(@Body request: SubDistrictRequest): SubDistrictResponse

    @POST("inquiry/api/v1/mask/doctor-info")
    suspend fun getMaskInquiryDoctorInfo(@Body request: InquiryDoctorInfoRequest): InquiryDoctorInfoResponse

    @POST("inquiry/api/v1/doctor-info")
    suspend fun getInquiryDoctorInfo(@Body request: InquiryDoctorInfoRequest): InquiryDoctorInfoResponse

    @PATCH("doctor/api/v1/patch-doctor-info")
    suspend fun getPatchDoctorInfo(@Body request: PatchDoctorInfoRequest): PatchDoctorInfoResponse

    @POST("doctor/api/v1/update-doctor-info")
    suspend fun setUpdateDoctorInfo(@Body request: SetUpdateDoctorInfoRequest)

    @POST("otp/api/v1/request-otp")
    suspend fun getRequestOtp(@Body request: OtpRequest): OtpResponse

    @POST("otp/api/v1/verify-otp")
    suspend fun getVerifyOtp(@Body request: OtpVerifyRequest)
}