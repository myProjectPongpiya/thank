package com.thankyou.appdata.repository.model.profile

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SubDistrictModel(
    @SerializedName("subDistricts")
    val subDistricts: List<SubDistricts>? = null
) : Parcelable {

    @Parcelize
    data class SubDistricts(
        @SerializedName("code")
        val code: String,
        @SerializedName("name")
        val name: String? = null,
        @SerializedName("postCode")
        val postCode: List<String>? = null,
    ) : Parcelable
}
