package com.thankyou.appdata.repository.response.profile

import com.google.gson.annotations.SerializedName

data class PatchDoctorInfoRequest(
    @SerializedName("tokenUuid")
    val tokenUuid: String,
    @SerializedName("codecpe")
    val codecpe: String? = null,
    @SerializedName("mobile")
    val mobile: String? = null,
    @SerializedName("email")
    val email: String? = null,
    @SerializedName("addressId")
    val addressId: String? = null,
    @SerializedName("addressType")
    val addressType: String? = null,
    @SerializedName("addressPlace")
    val addressPlace: String? = null,
    @SerializedName("addressNo")
    val addressNo: String? = null,
    @SerializedName("addressSoi")
    val addressSoi: String? = null,
    @SerializedName("addressSubDistrictID")
    val addressSubDistrictID: String? = null,
    @SerializedName("addressSubDistrictName")
    val addressSubDistrictName: String? = null,
    @SerializedName("addressDistrictID")
    val addressDistrictID: String? = null,
    @SerializedName("addressDistrictName")
    val addressDistrictName: String? = null,
    @SerializedName("addressProvinceID")
    val addressProvinceID: String? = null,
    @SerializedName("addressProvinceName")
    val addressProvinceName: String? = null,
    @SerializedName("addressPostCode")
    val addressPostCode: String? = null,
    @SerializedName("addressContractPhone")
    val addressContractPhone: String? = null
)