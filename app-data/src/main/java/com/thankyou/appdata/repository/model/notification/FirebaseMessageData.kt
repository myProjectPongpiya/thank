package com.thankyou.appdata.repository.model.notification

import com.thankyou.appfoundation.authentication.AppStatus
import com.thankyou.appfoundation.notifications.FirebaseNotificationType

data class FirebaseMessageData(
  val messageTitle: String,
  val messageText: String,
  val appStatus: AppStatus?,
  val state: LoginHieState?,
  val type: FirebaseNotificationType?
)
