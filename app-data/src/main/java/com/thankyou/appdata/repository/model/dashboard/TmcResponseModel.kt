package com.thankyou.appdata.repository.model.dashboard

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TmcResponseModel(
  @SerializedName("url")
  val url: String? = null
) : Parcelable
