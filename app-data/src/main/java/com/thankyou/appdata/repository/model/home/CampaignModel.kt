package com.thankyou.appdata.repository.model.home

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CampaignModel(
    @SerializedName("imageUrl")
    val imageUrl: String? = null,
    @SerializedName("redirectUrl")
    val redirectUrl: String? = null,
) : Parcelable
