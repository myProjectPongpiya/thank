package com.thankyou.appdata.repository.response.authentication

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.common.BiometricsUuidResponseModel

data class BiometricUuidResponse(
  @SerializedName("content")
  val content: BiometricsUuidResponseModel
)
