package com.thankyou.appdata.repository.response.profile

import com.google.gson.annotations.SerializedName

data class OtpRequest(
    @SerializedName("type")
    val type: String,
    @SerializedName("mobileNumber")
    val mobileNumber: String? = null,
    @SerializedName("email")
    val email: String? = null
)
