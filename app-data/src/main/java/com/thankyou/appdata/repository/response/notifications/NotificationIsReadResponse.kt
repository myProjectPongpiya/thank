package com.thankyou.appdata.repository.response.notifications

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.notification.NotificationIsReadModel

data class NotificationIsReadResponse(
    @SerializedName("content")
    val content: NotificationIsReadModel
)
