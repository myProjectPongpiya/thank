package com.thankyou.appdata.repository.model.profile

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DistrictModel(
  @SerializedName("districts")
  val districts: List<Districts>? = null
) : Parcelable {

    @Parcelize
    data class Districts(
      @SerializedName("code")
      val code: String,
      @SerializedName("name")
      val name: String? = null,
    ) : Parcelable
}
