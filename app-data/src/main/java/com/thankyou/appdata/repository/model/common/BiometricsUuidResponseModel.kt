package com.thankyou.appdata.repository.model.common

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BiometricsUuidResponseModel(
    @SerializedName("key")
    val key: String? = null,
    @SerializedName("checkDoctor")
    val checkDoctor: Boolean? = false
) : Parcelable
