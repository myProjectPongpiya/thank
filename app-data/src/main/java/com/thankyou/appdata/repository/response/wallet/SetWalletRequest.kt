package com.thankyou.appdata.repository.response.wallet

import com.google.gson.annotations.SerializedName

data class SetWalletRequest(
    @SerializedName("walletKey")
    val walletKey: String
)
