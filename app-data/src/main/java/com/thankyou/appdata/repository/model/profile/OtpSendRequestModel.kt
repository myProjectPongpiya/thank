package com.thankyou.appdata.repository.model.profile

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OtpSendRequestModel(
    @SerializedName("ref")
    val ref: String
) : Parcelable