package com.thankyou.appdata.repository.api.dashboard

import com.thankyou.appdata.repository.response.dashboard.ContentResponse
import com.thankyou.appdata.repository.response.dashboard.StatusResponse
import com.thankyou.appdata.repository.response.dashboard.MDEServiceRequest
import com.thankyou.appdata.repository.response.dashboard.TmcResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface DashboardApi {

    @POST("inquiry/api/v1/get-tmc-url")
    suspend fun getTmc(): TmcResponse

    @POST("inquiry/api/v1/get-consent-status")
    suspend fun getConsentStatus(): StatusResponse

    @POST("inquiry/api/v1/get-consent-content")
    suspend fun getConsentContent(): ContentResponse

    @POST("inquiry/api/v1/accept-consent")
    suspend fun acceptConsent()

    @POST("inquiry/api/v1/get-term-content")
    suspend fun getTermAndConditionContent(): ContentResponse

    @POST("inquiry/api/v1/accept-term")
    suspend fun acceptTermAndCondition()

    @POST("onboard/api/v1/get-data-md-eservice")
    suspend fun getDataMDEService(@Body params: MDEServiceRequest)
}
