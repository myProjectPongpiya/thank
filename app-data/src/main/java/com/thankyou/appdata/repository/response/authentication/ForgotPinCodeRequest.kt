package com.thankyou.appdata.repository.response.authentication

import com.google.gson.annotations.SerializedName

data class ForgotPinCodeRequest(
    @SerializedName("cid")
    val cid: String,
    @SerializedName("bdate")
    val bdate: String,
    @SerializedName("doctorId")
    val doctorId: String,
    @SerializedName("pin")
    val pin: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("surename")
    val surename: String
)
