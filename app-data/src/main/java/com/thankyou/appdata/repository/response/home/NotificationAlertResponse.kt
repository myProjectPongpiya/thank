package com.thankyou.appdata.repository.response.home

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.home.SetAlertModel

data class NotificationAlertResponse(
    @SerializedName("content")
    val content: SetAlertModel
)