package com.thankyou.appdata.repository.response.setting

import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.model.setting.ContractModel

data class ContractResponse(
    @SerializedName("content")
    val content: ContractModel
)
