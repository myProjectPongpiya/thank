package com.thankyou.appdata.repository.response.notifications

import com.google.gson.annotations.SerializedName

data class NotificationIsConfirmRequest(
    @SerializedName("notification_id")
    val notificationId: String
)
