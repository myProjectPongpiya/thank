package com.thankyou.appdata.repository.api.loginHie

import com.thankyou.appdata.repository.response.loginHie.LoginHieRequest
import com.thankyou.appdata.repository.response.loginHie.LoginHieResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginHieApi {

    @POST("doctor/api/v1/login-hie")
    suspend fun getLoginHie(@Body request: LoginHieRequest): LoginHieResponse
}
