package com.thankyou.appdata.repository.model.home

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.thankyou.appdata.repository.enums.IsReadType
import com.thankyou.appfoundation.notifications.FirebaseNotificationType
import kotlinx.android.parcel.Parcelize
import org.threeten.bp.ZonedDateTime

@Parcelize
data class NotificationListModel(
    @SerializedName("pageCount")
    val pageCount: Int,
    @SerializedName("announceContent")
    val announceContent: List<AnnounceContent>? = null
) : Parcelable {

    @Parcelize
    data class AnnounceContent(
        @SerializedName("id")
        val id: Int,
        @SerializedName("title")
        val title: String? = null,
        @SerializedName("description")
        val description: String? = null,
        @SerializedName("createdDate")
        val createdDate: ZonedDateTime? = null,
        @SerializedName("notificationType")
        val notificationType: FirebaseNotificationType? = null,
        @SerializedName("isConfirm")
        val isConfirm: String? = null,
        @SerializedName("isRead")
        var isRead: IsReadType? = null,
    ) : Parcelable
}