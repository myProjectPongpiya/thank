package com.thankyou.appdata.repository.api.wallet

import com.thankyou.appdata.repository.response.wallet.GenerateWalletResponse
import com.thankyou.appdata.repository.response.wallet.GetWalletInvitationForUpdateRequest
import com.thankyou.appdata.repository.response.wallet.GetWalletInvitationRequest
import com.thankyou.appdata.repository.response.wallet.GetWalletInvitationResponse
import com.thankyou.appdata.repository.response.wallet.GetWalletResponse
import com.thankyou.appdata.repository.response.wallet.SetWalletRequest
import retrofit2.http.Body
import retrofit2.http.POST

interface WalletInvokeApi {

    @POST("wallet/api/v1/set-wallet")
    suspend fun setWallet(@Body request: SetWalletRequest)

    @POST("wallet/api/v1/get-wallet")
    suspend fun getWallet(): GetWalletResponse

    @POST("wallet/api/v1/gen-wallet-key")
    suspend fun generateWalletKey(): GenerateWalletResponse

    @POST("onboard/api/v1/credential/request")
    suspend fun getWalletInvitation(@Body request: GetWalletInvitationRequest): GetWalletInvitationResponse

    @POST("onboard/api/v1/credential/update")
    suspend fun getWalletInvitation(@Body request: GetWalletInvitationForUpdateRequest): GetWalletInvitationResponse
}