package com.thankyou.apptestutils

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.navigation.NavDirections
import com.thankyou.appfoundation.base.BaseViewModel
import com.thankyou.appfoundation.deeplink.DeepLinkEvent
import com.thankyou.appfoundation.permissions.PermissionRequest
import com.thankyou.appfoundation.ui.Alert
import com.thankyou.apptestutils.rules.CoroutineDispatcherRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

/**
 * Base class for testing ViewModels.
 *
 * The ViewModel under test will be accessible via the property [vm].
 */
@RunWith(MockitoJUnitRunner::class)
abstract class BaseViewModelTests<VM : BaseViewModel> {

    protected lateinit var vm: VM

    protected val testDispatcher = TestCoroutineDispatcher()

    /**
     * This bypasses the main thread check, and immediately runs any tasks on your test thread,
     * allowing for immediate and predictable calls and therefore assertions
     */
    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    /**
     * This causes coroutines to immediately execute. You can manually override
     * the execution (delaying, advancing, etc) by controlling [testDispatcher].
     */
    @ExperimentalCoroutinesApi
    @Rule
    @JvmField
    var coroutineDispatcherRule = CoroutineDispatcherRule(testDispatcher)

    @Mock
    lateinit var onAlertEventObserver: Observer<Alert>

    @Mock
    lateinit var onRequestPermissionEventObserver: Observer<PermissionRequest>

    @Mock
    lateinit var onLaunchDeepLinkEventObserver: Observer<DeepLinkEvent>

    @Mock
    lateinit var onGotoPage: Observer<NavDirections>

    /**
     * Initialize and return the view model under test here.
     */
    abstract fun createViewModel(): VM

    @Before
    fun setupViewModel() {
        vm = createViewModel()
        vm.onAlertEvent.observeForever(onAlertEventObserver)
        vm.onRequestPermissionEvent.observeForever(onRequestPermissionEventObserver)
        vm.onLaunchDeepLinkEvent.observeForever(onLaunchDeepLinkEventObserver)
        vm.gotoPage.observeForever(onGotoPage)
    }
}
