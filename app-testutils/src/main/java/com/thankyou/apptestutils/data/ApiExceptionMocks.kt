package com.thankyou.apptestutils.data

import com.thankyou.appfoundation.http.ApiException
import com.thankyou.appfoundation.http.ApiHeader

object ApiExceptionMocks {

    /**
     * Returns an [ApiException] with the provided error [code].
     */
    fun withCode(code: String): ApiException {
        val error = ApiHeader()
        return ApiException(error)
    }
}
