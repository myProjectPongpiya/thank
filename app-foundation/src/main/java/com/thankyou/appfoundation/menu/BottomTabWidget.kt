package com.thankyou.appfoundation.menu

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.view.menu.MenuBuilder
import androidx.core.view.MenuItemCompat
import androidx.core.view.forEach
import com.google.android.material.shape.CutCornerTreatment
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel
import com.google.android.material.shape.TriangleEdgeTreatment
import com.thankyou.appfoundation.R
import kotlinx.android.synthetic.main.layout_bottom_navigation.view.tabWidget

class BottomTabWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0,
) : FrameLayout(context, attrs, defStyle, defStyleRes) {

    enum class BottomTabNav(val navId: Int) {
        MAIN_TAB(BottomTabWidget.MAIN_TAB),
        PROFILE_TAB(BottomTabWidget.PROFILE_TAB),
    }

    private var navBottomTabItemListener: NavBottomTabItemListener? = null
    private var onMenuPrepareListener: ((Unit) -> Unit?)? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_bottom_navigation, this, true)
    }

    fun setNavBottomTabItemListener(listener: NavBottomTabItemListener) {
        navBottomTabItemListener = listener
    }

    @SuppressLint("RestrictedApi")
    fun initMenu(activity: Activity) {
        val menu = MenuBuilder(activity)
        activity.menuInflater.inflate(R.menu.menu_bottom, menu)
        setMenuItems(menu)
        initMenuItemListener()
    }

    private fun initMenuItemListener() {
        tabWidget.setOnNavigationItemSelectedListener { item ->

            return@setOnNavigationItemSelectedListener when (item.itemId) {
                R.id.navigation_main -> checkNavBarAction(
                    BottomTabNav.MAIN_TAB.navId
                )
                R.id.navigation_profile -> checkNavBarAction(
                    BottomTabNav.PROFILE_TAB.navId
                )
                else -> true
            }
        }
    }

    private fun checkNavBarAction(tabId: Int): Boolean {
        navBottomTabItemListener?.navServiceItemChange(tabId)
        return true
    }

    private fun setMenuItems(menuBuilder: Menu) {
        tabWidget.menu.clear()
        tabWidget.itemIconTintList = null
        menuBuilder.forEach { menu ->
            val label = menu.title.toString()
            val contentDescriptionText = menu.titleCondensed.toString()

            tabWidget.menu.add(Menu.NONE, menu.itemId, Menu.NONE, label).apply {
                icon = menu.icon
                MenuItemCompat.setContentDescription(
                    this,
                    contentDescriptionText
                )
            }
        }
        onMenuPrepareListener?.invoke(Unit)
    }

    interface NavBottomTabItemListener {
        fun navServiceItemChange(activeTab: Int)
    }

    companion object {
        const val MAIN_TAB = 0
        const val PROFILE_TAB = 1
    }
}

inline fun BottomTabWidget.setNavBottomTabItemListener(crossinline block: (Int) -> Unit) {
    this.setNavBottomTabItemListener(object : BottomTabWidget.NavBottomTabItemListener {
        override fun navServiceItemChange(activeTab: Int) = block(activeTab)
    })
}
