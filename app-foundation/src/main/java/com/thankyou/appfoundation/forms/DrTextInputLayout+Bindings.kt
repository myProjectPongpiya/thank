package com.thankyou.appfoundation.forms

import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.thankyou.appfoundation.R
import com.thankyou.appfoundation.lib.lifecycle.BindDirection
import com.thankyou.appfoundation.lib.lifecycle.bindText
import com.thankyou.appfoundation.text.SpecialCharactersInputFilter
import com.thankyou.appfoundation.text.TextFormInput

/**
 * Binds the error value of [liveData] to this text input layout's error field.
 */
fun DrTextInputLayout.bindError(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<String>
) {
    liveData.observe(lifecycleOwner) {
        this.error = it
        val editText = this.editText as? DrTextInput
        if (it != null) {
            isErrorEnabled = true
            editText?.setBackgroundResource(R.drawable.bg_edit_input_error)
        } else {
            isErrorEnabled = false
            editText?.setBackgroundResource(R.drawable.bg_edit_input)
        }
    }
}

/**
 * Binds the error value of [liveData] to this text input layout's error field.
 */
fun DrTextInputLayout.bindErrorDropDown(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<String?>
) {
    liveData.observe(lifecycleOwner) {
        this.error = it
        val editText = this.editText as? DrTextInput
        if (it != null) {
            isErrorEnabled = true
            editText?.setBackgroundResource(R.drawable.bg_edit_input_error)
        } else {
            isErrorEnabled = false
            editText?.setBackgroundResource(R.drawable.bg_edit_input)
        }
    }
}

/**
 * Binds the text value of [liveData] to this text input layout's inner [EditText].
 */
fun DrTextInputLayout.bindText(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<String>,
    direction: BindDirection = BindDirection.TWO_WAY
) {
    val editText =
        this.editText ?: throw IllegalStateException("Child must be an instance of EditText.")
    editText.bindText(lifecycleOwner, liveData, direction)
}

fun DrTextInputLayout.bindHelperText(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<String?>,
) = apply {
    liveData.observe(lifecycleOwner) { this.helperText = it }
}

fun DrTextInputLayout.bindFormInput(
    lifecycleOwner: LifecycleOwner,
    formInput: TextFormInput,
    maxLength: Int = 0,
    allowSpacialCharacters: Boolean = true
) = apply {
    val editText = this.editText as? DrTextInput
        ?: throw IllegalStateException("Child must be an instance of EditText.")

    formInput.input.observe(lifecycleOwner) {
        if (it == editText.text?.toString()) {
            return@observe
        } else {
            editText.setText(it)
        }
    }

    this.bindError(lifecycleOwner, formInput.error)

    editText.addOnEditDoneListener {
        formInput.setTouched(true)
        formInput.updateError()
    }

    editText.addOnFocusChangeListener {
        if (!it) {
            formInput.setTouched(true)
            formInput.updateError()
        }
    }

    val filter = mutableListOf<InputFilter>()

    if (!allowSpacialCharacters) {
        filter.add(SpecialCharactersInputFilter())
        editText.filters = filter.toTypedArray()
    }

    if (maxLength > 0) {
        filter.add(InputFilter.LengthFilter(maxLength))
        editText.filters = filter.toTypedArray()
    }

    editText.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            // do nothing
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (formInput.input.value != s?.toString()) {
                formInput.input.value = s?.toString()
            }
        }

        override fun afterTextChanged(s: Editable?) {
            if (formInput.clearErrorOnChange) {
                formInput.clearError()
            }
        }
    })
}
