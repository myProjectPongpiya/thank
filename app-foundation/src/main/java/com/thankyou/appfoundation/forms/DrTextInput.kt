package com.thankyou.appfoundation.forms

import android.content.Context
import android.text.InputFilter
import android.text.method.DigitsKeyListener
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import androidx.core.content.withStyledAttributes
import com.google.android.material.textfield.TextInputEditText
import com.thankyou.appfoundation.R
import com.thankyou.appfoundation.text.AutoFormatTextWatcher
import com.thankyou.appfoundation.text.AutoFormatType
import com.thankyou.appfoundation.text.AutoPhoneFormatTextWatcher

class DrTextInput : TextInputEditText {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        parseAttributes(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        parseAttributes(attrs)
    }

    private val focusChangeListeners = mutableListOf<(Boolean) -> Unit>()
    private val editDoneListeners = mutableListOf<() -> Unit>()
    private var formattingType = AutoFormatType.NO_FORMAT

    init {
        setOnEditorActionListener { _, actionId, event ->
            if (
                actionId == EditorInfo.IME_ACTION_SEARCH ||
                actionId == EditorInfo.IME_ACTION_DONE ||
                event?.action == KeyEvent.ACTION_DOWN &&
                event.keyCode == KeyEvent.KEYCODE_ENTER
            ) {
                editDoneListeners.forEach { it.invoke() }
            }
            false
        }

        setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                editDoneListeners.forEach { it.invoke() }
            }
        }
    }

    override fun onKeyPreIme(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP) {
            editDoneListeners.forEach { it.invoke() }
        }
        return super.onKeyPreIme(keyCode, event)
    }

    /**
     * Add a [listener] to be invoked when this text field changed focus.
     */
    fun addOnFocusChangeListener(listener: (Boolean) -> Unit) {
        focusChangeListeners.add(listener)
    }

    /**
     * Add a [listener] to be invoked when the user completes editing this text field.
     */
    fun addOnEditDoneListener(listener: () -> Unit) {
        editDoneListeners.add(listener)
    }

    private fun parseAttributes(attrs: AttributeSet?) {
        context.withStyledAttributes(attrs, R.styleable.DrTextInput) {
            formattingType =
                AutoFormatType.values()[getInt(
                    R.styleable.DrTextInput_formattingType,
                    AutoFormatType.NO_FORMAT.ordinal
                )]
        }

        setFormattingType()
    }

    fun setFormatTypeForFragment(autoFormatType: AutoFormatType) {
        formattingType = autoFormatType
        setFormattingType()
    }

    private fun setFormattingType() {
        when (formattingType) {
            AutoFormatType.NO_FORMAT -> Unit
            AutoFormatType.PHONE_FORMAT -> {
                initKeyboardOption(formattingType.formatChar)
                addTextChangedListener(AutoPhoneFormatTextWatcher(listener = { format ->
                    initKeyboardOption(format.formatChar)
                    format.maxLength?.let { filters = arrayOf(InputFilter.LengthFilter(it)) }
                }))
            }
            else -> {
                initKeyboardOption(formattingType.formatChar)
                addTextChangedListener(AutoFormatTextWatcher(formattingType))
            }
        }
    }

    private fun initKeyboardOption(formatChar: String) {
        keyListener = DigitsKeyListener.getInstance("$FORMAT_DIGITS$formatChar")
        formattingType.maxLength?.let { filters = arrayOf(InputFilter.LengthFilter(it)) }
    }

    companion object {
        const val FORMAT_DIGITS = "0123456789"
    }
}
