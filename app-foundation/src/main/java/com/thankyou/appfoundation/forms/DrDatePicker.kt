package com.thankyou.appfoundation.forms

import android.content.Context
import android.icu.util.BuddhistCalendar
import android.icu.util.Calendar
import android.text.InputType
import android.util.AttributeSet
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.withStyledAttributes
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.thankyou.appfoundation.R
import com.thankyou.appfoundation.i18n.T
import com.thankyou.appfoundation.text.DateFormat
import com.layernet.thaidatetimepicker.date.DatePickerDialog
import kotlinx.android.synthetic.main.layout_date_picker.view.*
import org.threeten.bp.LocalDate
import org.threeten.bp.chrono.ThaiBuddhistDate
import org.threeten.bp.format.DateTimeFormatter

class DrDatePicker @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0,
) : DrBaseDropDown(context, attrs, defStyle, defStyleRes) {

    /**
     * used to display a populated data in drop down that cannot be updated
     */
    private var isFilledDisabledState: Boolean = false

    private var onSelectListener: ((data: String) -> Unit)? = null

    init {
        parseAttributes(attrs)
        initializeScreen(R.layout.layout_date_picker)
        initialize()
    }

    private fun initialize() {
        disableTextInputForClickable()
        val cal = BuddhistCalendar.getInstance()
        val datePickerDialog = DatePickerDialog.newInstance(
            { _, year, monthOfYear, dayOfMonth ->
                val selectedDataOfBirth = LocalDate.of(year, monthOfYear + NUM_1, dayOfMonth)
                onSelectListener?.invoke(setDateOfBirthFormat(selectedDataOfBirth))
                etDatePickerValue.setText(setDateOfBirthDisplayFormat(selectedDataOfBirth))
            },
            cal.get(Calendar.YEAR),
            cal.get(Calendar.MONTH),
            cal.get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.maxDate = java.util.Calendar.getInstance()

        etDatePickerValue.setOnClickListener {
            datePickerDialog.show(
                (context as AppCompatActivity).fragmentManager,
                DrBaseDropDown::class.java.simpleName
            )
        }
    }

    private fun setDateOfBirthDisplayFormat(dateInput: LocalDate): String {
        return DateTimeFormatter.ofPattern(DateFormat.DR_DATE.format, T.getLocale())
            .format(ThaiBuddhistDate.from(dateInput))
    }

    private fun setDateOfBirthFormat(dateInput: LocalDate): String {
        return DateTimeFormatter.ofPattern(DateFormat.DR_DATE_BIRTH.format, T.getLocale())
            .format(ThaiBuddhistDate.from(dateInput))
    }

    fun setOnClickListenerDatePicker(listener: ((date: String) -> Unit)) {
        onSelectListener = listener
    }

    private fun disableTextInputForClickable() {
        etDatePickerValue.apply {
            isFocusable = false
            isClickable = true
            inputType = InputType.TYPE_NULL
        }
    }

    fun setText(text: String) {
        etDatePickerValue.setText(text)
    }

    fun setError(
        lifecycleOwner: LifecycleOwner,
        liveData: LiveData<String?>
    ) {
        divDatePickerHint.bindErrorDropDown(lifecycleOwner, liveData)
    }

    fun setButtonAndDialogTitle(title: String, header: String? = null) {
        divDatePickerHint.hint = title
        headerDialog = header ?: title
    }

    private fun parseAttributes(attrs: AttributeSet?) {
        context.withStyledAttributes(attrs, R.styleable.DrDropDown) {
            isFilledDisabledState =
                getBoolean(R.styleable.DrDropDown_isFilledDisabledState, false)
        }
    }

    companion object {
        const val NUM_1 = 1
    }
}
