package com.thankyou.appfoundation.forms

import android.content.Context
import android.text.InputType
import android.util.AttributeSet
import androidx.core.content.withStyledAttributes
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.thankyou.appfoundation.R
import kotlinx.android.synthetic.main.layout_dropdown_list.view.*
import timber.log.Timber

class DrDropDown @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0,
) : DrBaseDropDown(context, attrs, defStyle, defStyleRes) {

    /**
     * used to display a populated data in drop down that cannot be updated
     */
    private var isFilledDisabledState: Boolean = false

    init {
        parseAttributes(attrs)
        Timber.d("isFilledDisabledState: $isFilledDisabledState")
        when (isFilledDisabledState) {
            true -> initializeScreen(R.layout.layout_dropdown_list_disabled)
            else -> initializeScreen(R.layout.layout_dropdown_list)
        }
        initialize()
    }

    private var isDropdownClickEnabled: Boolean = true

    private var onDropdownSelectedListener: (() -> Unit)? = null

    private fun initialize() {
        disableTextInputForClickable()
        etDropdownValue.setOnClickListener {
            if (isDropdownClickEnabled) {
                onClickDisplayBottomMenu()
            }
            onDropdownSelectedListener?.invoke()
        }
    }

    private fun disableTextInputForClickable() {
        etDropdownValue.apply {
            isFocusable = false
            isClickable = true
            inputType = InputType.TYPE_NULL
        }
    }

    fun setText(text: String) {
        etDropdownValue.setText(text)
    }

    fun setHint(hint: String) {
        divDropdownHint.hint = hint
    }

    fun setDisableBackground(isDisable: Boolean) {
        if (!isDisable) {
            etDropdownValue.apply {
                setBackgroundResource(R.drawable.bg_edit_input_disable)
                val iconDrawable =
                    this.context.resources.getDrawable(R.drawable.ic_dropdown_disable)
                etDropdownValue.setCompoundDrawablesWithIntrinsicBounds(
                    null,
                    null,
                    iconDrawable,
                    null
                )
                setDropdownClickEnabled(false)
            }
        } else {
            etDropdownValue.apply {
                setBackgroundResource(R.drawable.bg_edit_input_normal)
                val iconDrawable = this.context.resources.getDrawable(R.drawable.ic_dropdown_active)
                etDropdownValue.setCompoundDrawablesWithIntrinsicBounds(
                    null,
                    null,
                    iconDrawable,
                    null
                )
                setDropdownClickEnabled(true)
            }
        }
    }

    fun setButtonAndDialogTitle(title: String, header: String? = null) {
        divDropdownHint.hint = title
        headerDialog = header ?: title
    }

    fun setHelperText(helperText: CharSequence?) {
        divDropdownHint.helperText = helperText
    }

    fun setDropdownClickEnabled(isClickEnable: Boolean) {
        isDropdownClickEnabled = isClickEnable
    }

    fun setError(
        lifecycleOwner: LifecycleOwner,
        liveData: LiveData<String?>
    ) {
        divDropdownHint.bindErrorDropDown(lifecycleOwner, liveData)
    }

    /**
     * update drop down appearance
     */
    fun setEnabledAppearance(isEnabled: Boolean) {
        divDropdownHint.isEnabled = isEnabled
    }

    private fun parseAttributes(attrs: AttributeSet?) {
        context.withStyledAttributes(attrs, R.styleable.DrDropDown) {
            isFilledDisabledState =
                getBoolean(R.styleable.DrDropDown_isFilledDisabledState, false)
        }
    }
}
