package com.thankyou.appfoundation.forms

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ContextThemeWrapper
import androidx.fragment.app.FragmentManager
import com.thankyou.appfoundation.flatlist.DrFlatListItem
import com.thankyou.appfoundation.menu.DrBottomMenu

abstract class DrBaseDropDown @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyle, defStyleRes) {

    private lateinit var dropdownItems: List<DrFlatListItem>
    private var onSelectListener: ((item: DrFlatListItem.Item) -> Unit)? = null
    var selectedId: String? = ""

    lateinit var headerDialog: String

    fun initializeScreen(@LayoutRes layout: Int) {
        View.inflate(context, layout, this)
    }

    fun onClickDisplayBottomMenu() {
        if (!this::dropdownItems.isInitialized) return
        val dialog = DrBottomMenu(
            items = dropdownItems,
            heading = headerDialog,
            selectedId = selectedId,
            onClickListener = this@DrBaseDropDown::onSelectItem
        )
        getFragmentManager()?.let { fragmentManager ->
            dialog.show(
                fragmentManager,
                DrBaseDropDown::class.java.simpleName
            )
        }
    }

    /**
     * Its possible that button drop is being used
     * in another view potentially from [ContextThemeWrapper]
     */
    private fun getFragmentManager(): FragmentManager? {
        return when (context) {
            is ContextThemeWrapper ->
                ((context as? ContextThemeWrapper)?.baseContext as? AppCompatActivity)?.supportFragmentManager
            else -> (context as AppCompatActivity).supportFragmentManager
        }
    }

    private fun onSelectItem(item: DrFlatListItem.Item) {
        selectedId = item.id
        onSelectListener?.invoke(item)
    }

    fun setOnClickListener(listener: ((item: DrFlatListItem.Item) -> Unit)) {
        onSelectListener = listener
    }

    fun setItems(items: List<DrFlatListItem>) {
        dropdownItems = items
    }

    /**
     * Set a default selection id if drop down already have a default value
     * Ignore if there are no default
     */
    fun setDefaultSelectedId(id: String?) {
        selectedId = id
    }

    fun clearSelectedId() {
        selectedId = ""
    }
}
