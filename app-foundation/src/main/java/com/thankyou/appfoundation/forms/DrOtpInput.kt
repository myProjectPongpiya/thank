package com.thankyou.appfoundation.forms

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.MutableLiveData
import com.thankyou.appfoundation.R
import kotlinx.android.synthetic.main.layout_otp_input.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

/**
 * [DrOtpInput] is a reusable class to display an OTP input.
 *
 * It works by rendering an invisible [EditText] over the entire view, which the
 * user will interact with. There will be six (6) 'dummy' [TextView] that will
 * only be used to render the content of the user-interactable, invisible [EditText].
 */
class DrOtpInput @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyle, defStyleRes), CoroutineScope {

    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    private val otp = MutableLiveData<String>()
    private val hasFocus = MutableLiveData<Boolean>()
    private var listener: ((String) -> Unit)? = null

    init {
        View.inflate(context, R.layout.layout_otp_input, this)
        setupEditText()
        setupObservers()
    }

    private val displayInputs = listOf(
        OtpInputDisplay(textView = tvOtp1, cursorView = vCursor1),
        OtpInputDisplay(textView = tvOtp2, cursorView = vCursor2),
        OtpInputDisplay(textView = tvOtp3, cursorView = vCursor3),
        OtpInputDisplay(textView = tvOtp4, cursorView = vCursor4),
        OtpInputDisplay(textView = tvOtp5, cursorView = vCursor5),
        OtpInputDisplay(textView = tvOtp6, cursorView = vCursor6)
    )

    private fun setupEditText() {
        etMain.addTextChangedListener(
            afterTextChanged = { input -> input?.let { otp.value = it.toString() } }
        )
        etMain.setOnFocusChangeListener { _, hasFocus ->
            this.hasFocus.value = hasFocus
        }
        etMain.setOnClickListener {
            etMain.placeCursorAtEnd()
            render()
        }
    }

    private fun setupObservers() {
        otp.observeForever {
            render()
            listener?.invoke(it)
        }
        hasFocus.observeForever {
            render()
        }
    }

    /**
     * Renders the dummy inputs.
     */
    private fun render() {
        // Render dummy TextViews
        displayInputs.forEachIndexed { i, input ->
            val char = try {
                otp.value?.get(i)
            } catch (e: Exception) {
                null
            }
            displayInput(input, char?.toString())
        }

        // Render carets
        displayInputs.forEachIndexed { i, view ->
            val otpLength = otp.value?.length ?: 0
            (hasFocus.value == true && otpLength == i).run {
                view.textView.isActivated = this
                view.cursorView.isGone
            }
        }
    }

    private fun displayInput(input: OtpInputDisplay, value: String?) {
        if (input.currentValue == value) {
            return
        }

        // Reset any ongoing masking job
        input.maskJob?.cancel()

        // Display the user's input
        input.currentValue = value
        input.textView.text = value

        // Start a job to mask the input after 1 seconds
        if (value?.isNotBlank() == true) {
            input.maskJob = launch {
                delay(MASK_DELAY_MILLIS)
                input.textView.text = "•"
            }
        }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        etMain.isEnabled = enabled
        displayInputs.map { it.textView.isEnabled = enabled }
        if (enabled) {
            requestFocusWithKeyboard()
        } else {
            clear()
        }
    }

    /**
     * Clears the OTP input.
     */
    fun clear() {
        this.etMain.setText("")
    }

    /**
     * Manually set an [otp] to display.
     */
    fun setOtp(otp: String) {
        this.etMain.setText(otp)
    }

    fun requestFocusWithKeyboard() {
        postDelayed({
            this.etMain.requestFocus()
            val imm = ContextCompat.getSystemService(context, InputMethodManager::class.java)
            imm?.showSoftInput(this.etMain, InputMethodManager.SHOW_IMPLICIT)
        }, 100)
    }

    /**
     * Sets the [listener] to be invoked when the OTP input changes.
     */
    fun setOnInputOtpListener(listener: (otp: String) -> Unit) {
        this.listener = listener
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        job.cancel()
        val imm = ContextCompat.getSystemService(context, InputMethodManager::class.java)
        imm?.hideSoftInputFromWindow(this.etMain.windowToken, 0)
    }

    private fun EditText.placeCursorAtEnd() {
        this.setSelection(this.text.length)
    }

    private class OtpInputDisplay(
        var currentValue: String? = null,
        val textView: TextView,
        val cursorView: View,
        var maskJob: Job? = null
    )

    companion object {
        private const val MASK_DELAY_MILLIS = 750L
    }
}
