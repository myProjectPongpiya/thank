package com.thankyou.appfoundation.lib.lifecycle

import android.view.View
import androidx.appcompat.widget.SwitchCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData

/**
 * Binds the checked state of this [View] to [liveData].
 */
fun SwitchCompat.bindChecked(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<Boolean>,
) = apply {
    liveData.observe(lifecycleOwner) { this.isChecked = it == true }
}