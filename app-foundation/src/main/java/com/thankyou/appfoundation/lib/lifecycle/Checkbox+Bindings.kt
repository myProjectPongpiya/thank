package com.thankyou.appfoundation.lib.lifecycle

import android.widget.CheckBox
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/**
 * Bind the checked of this [Checkbox] to this [liveData].
 */
fun CheckBox.bindChecked(
  lifecycleOwner: LifecycleOwner,
  liveData: LiveData<Boolean>,
  direction: BindDirection = BindDirection.TWO_WAY
) = apply {

  liveData.observe(lifecycleOwner) {
      this.isChecked = it
  }

  if (direction == BindDirection.TWO_WAY) {
    if (liveData !is MutableLiveData<Boolean>) {
      throw IllegalStateException("LiveData must be mutable for two-way bindings.")
    }

    this.setOnCheckedChangeListener { _, isChecked ->
      liveData.value = isChecked // QR = false
    }
  }
}

/**
 * Bind the enable of this [Checkbox] to this [liveData].
 */
fun CheckBox.bindEnabled(
  lifecycleOwner: LifecycleOwner,
  liveData: LiveData<Boolean>,
) = apply {
  liveData.observe(lifecycleOwner) {
    this.isEnabled = it
  }
}
