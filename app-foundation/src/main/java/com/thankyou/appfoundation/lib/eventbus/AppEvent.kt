package com.thankyou.appfoundation.lib.eventbus

sealed class AppEvent {

  /**
   * Represents login success event
   */
  object LoginSuccess : AppEvent()
}
