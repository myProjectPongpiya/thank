package com.thankyou.appfoundation.lib.lifecycle

import android.text.Html
import android.text.method.LinkMovementMethod
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.thankyou.appfoundation.text.CustomSpannable

/**
 * Bind the text of this [TextView] to this [liveData].
 */
fun TextView.bindText(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<String>
) = apply {
    liveData.observe(lifecycleOwner) { this.text = it }
}

/**
 * Bind the text of this [TextView] to this [liveData].
 */
@JvmName("bindCustomSpannable")
fun TextView.bindText(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<CustomSpannable?>
) = apply {
    liveData.observe(lifecycleOwner) { spannable ->
        this.text = spannable?.toAndroidSpannable(this.context)

        if (spannable == null) {
            return@observe
        }

        if (this.movementMethod !is LinkMovementMethod
            && spannable.styles.any { it.second is CustomSpannable.Style.Clickable }) {
            this.movementMethod = LinkMovementMethod.getInstance()
        }
    }
}

/**
 * Bind the text of this [TextView] to this [liveData].
 */
fun TextView.bindTextFromHtml(
  lifecycleOwner: LifecycleOwner,
  liveData: LiveData<String>
) = apply {
  liveData.observe(lifecycleOwner) { this.text = Html.fromHtml(it) }
}