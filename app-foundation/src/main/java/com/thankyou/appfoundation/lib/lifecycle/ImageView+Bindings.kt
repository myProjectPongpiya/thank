package com.thankyou.appfoundation.lib.lifecycle

import android.widget.ImageView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.thankyou.appfoundation.extensions.setImage
import com.thankyou.appfoundation.graphics.DrImage

/**
 * Binds the image resource of this [ImageView] to [liveData].
 */
fun ImageView.bindImageResource(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<Int>
) = apply {
    liveData.observe(lifecycleOwner) {
        this.setImageResource(it ?: 0)
    }
}

/**
 * Binds the [DrImage] of [liveData].
 */
fun ImageView.bindImage(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<DrImage>
) = apply {
    liveData.observe(lifecycleOwner) {
        this.setImage(it)
    }
}
