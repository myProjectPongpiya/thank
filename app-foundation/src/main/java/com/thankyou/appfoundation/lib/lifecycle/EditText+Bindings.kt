package com.thankyou.appfoundation.lib.lifecycle

import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.widget.EditText
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations

/**
 * Binds the value of this [EditText] to [liveData]. You may optionally also
 * have the [EditText] set the value of the [liveData] by setting [BindDirection.TWO_WAY].
 */
fun EditText.bindText(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<String>,
    direction: BindDirection = BindDirection.TWO_WAY
) = apply {
    liveData.observe(lifecycleOwner) {
        if (it == this.text?.toString()) {
            return@observe
        } else {
            this.setText(it)
        }
    }

    if (direction == BindDirection.TWO_WAY) {
        if (liveData !is MutableLiveData<String>) {
            throw IllegalStateException("LiveData must be mutable for two-way bindings.")
        }

        this.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // do nothing
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (liveData.value != s?.toString()) {
                    liveData.value = s?.toString()
                }
            }

            override fun afterTextChanged(s: Editable?) {
                // do nothing
            }
        })
    }
}

/**
 * Binds the hint of this [EditText] to [liveData].
 */
fun EditText.bindHint(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<String>,
) = apply {
    liveData.observe(lifecycleOwner) { this.hint = it }
}

/**
 * Binds the max input length of this [EditText] to [liveData].
 */
fun EditText.bindMaxLength(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<Int>,
) = apply {
    Transformations.distinctUntilChanged(liveData).observe(lifecycleOwner) { maxLength ->
        // Remove the LengthFilter if the value is null
        if (maxLength == null) {
            this.filters = filters?.filter { it !is InputFilter.LengthFilter }?.toTypedArray()
            return@observe
        }

        // If there are no filters setDeviceId, create a new list of filters
        if (this.filters.isNullOrEmpty()) {
            this.filters = arrayOf(InputFilter.LengthFilter(maxLength))
            return@observe
        }

        // Otherwise, update the existing list of filters
        if (this.filters.size > 1) {
            val indexOfFilter = this.filters.indexOfFirst { it is InputFilter.LengthFilter }

            val newFilters = if (indexOfFilter >= 0) {
                this.filters.copyOf().apply {
                    set(indexOfFilter, InputFilter.LengthFilter(maxLength))
                }
            } else {
                this.filters.copyOf(this.filters.size + 1).apply {
                    set(this@bindMaxLength.filters.size, InputFilter.LengthFilter(maxLength))
                }
            }

            this.filters = newFilters
            return@observe
        }
    }
}

/**
 * Binds the [InputType] of this [EditText] to [liveData].
 */
fun EditText.bindInputType(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<Int>,
) = apply {
    liveData.observe(lifecycleOwner) { this.inputType = it }
}
