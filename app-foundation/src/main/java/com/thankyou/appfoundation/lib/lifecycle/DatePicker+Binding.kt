package com.thankyou.appfoundation.lib.lifecycle

import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.thankyou.appfoundation.forms.DrDatePicker

/**
 * Bind the text of this [TextView] to this [liveData].
 */

fun DrDatePicker.bindText(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<String>
) = apply {
    liveData.observe(lifecycleOwner) { this.setText(it) }
}

fun DrDatePicker.bindHint(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<String>
) = apply {
    liveData.observe(lifecycleOwner) { this.setButtonAndDialogTitle(it) }
}
