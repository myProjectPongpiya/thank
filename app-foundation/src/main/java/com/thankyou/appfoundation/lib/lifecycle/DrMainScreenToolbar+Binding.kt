package com.thankyou.appfoundation.lib.lifecycle

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.thankyou.appfoundation.graphics.DrImage
import com.thankyou.appfoundation.toolbar.DrMainScreenToolbar

fun DrMainScreenToolbar.bindNotification(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<DrImage>
) = apply {
    liveData.observe(lifecycleOwner) { this.setNotificationImage(it) }
}