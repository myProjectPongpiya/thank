package com.thankyou.appfoundation.data

import android.content.SharedPreferences

/**
 * Stores session data that persists after application close.
 *
 * To persist session data for different users, provide a different [prefs]
 * instance per user.
 */
class AppPersistentSession(
    private val prefs: SharedPreferences,
) {

    var doctorId: String?
        get() = prefs.getString(DOCTOR_ID, null)
        set(value) = prefs.edit().putString(DOCTOR_ID, value).apply()

    var encryptedBiometricUuid: String?
        get() = prefs.getString(ENCRYPTED_BIOMETRICS_UUID, null)
        set(value) = prefs.edit().putString(ENCRYPTED_BIOMETRICS_UUID, value).apply()

    var fcmToken: String?
        get() = prefs.getString(FCM_TOKEN, null)
        set(value) = prefs.edit().putString(FCM_TOKEN, value).apply()

    var hmsToken: String?
        get() = prefs.getString(HMS_TOKEN, null)
        set(value) = prefs.edit().putString(HMS_TOKEN, value).apply()

    var permissionCameraCheck: Boolean
        get() = prefs.getBoolean(PERMISSION_CAMERA_CHECK, false)
        set(value) = prefs.edit().putBoolean(PERMISSION_CAMERA_CHECK, value).apply()

    var biometricFirstState: Boolean
        get() = prefs.getBoolean(BIOMETRIC_FIRST_STATE, false)
        set(value) = prefs.edit().putBoolean(BIOMETRIC_FIRST_STATE, value).apply()

    var pinFirstState: Boolean
        get() = prefs.getBoolean(PIN_FIRST_STATE, false)
        set(value) = prefs.edit().putBoolean(PIN_FIRST_STATE, value).apply()

    fun clear() {
        prefs.edit().clear().apply()
    }

    companion object {
        private const val DOCTOR_ID = "DOCTOR_ID"
        private const val ENCRYPTED_BIOMETRICS_UUID = "ENCRYPTED_BIOMETRICS_UUID"
        private const val FCM_TOKEN = "FCM_TOKEN"
        private const val HMS_TOKEN = "HMS_TOKEN"
        private const val PERMISSION_CAMERA_CHECK = "PERMISSION_CAMERA_CHECK"
        private const val BIOMETRIC_FIRST_STATE = "BIOMETRIC_FIRST_STATE"
        private const val PIN_FIRST_STATE = "PIN_FIRST_STATE"
    }
}
