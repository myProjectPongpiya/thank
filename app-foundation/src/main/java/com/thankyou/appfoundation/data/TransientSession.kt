package com.thankyou.appfoundation.data

import org.threeten.bp.Clock

open class TransientSession(val clock: Clock) {

  val keyCache = mutableMapOf<String, TransientCache<*>>()

  val typedCache = mutableMapOf<Class<*>, TransientCache<*>>()

  /**
   * Get the [TransientCache] for [T] using [key], or create one if it does
   * not yet exist.
   *
   * Use this to store any arbitrary values for your feature modules.
   */
  @Suppress("UNCHECKED_CAST")
  fun <T> forKey(key: String): TransientCache<T> {
    return keyCache.getOrPut(key) { TransientCache<T>(clock) } as TransientCache<T>
  }

  /**
   * Get the [TransientCache] for [T] or create one if it does not yet exist.
   *
   * Use this to store any arbitrary values for your feature modules.
   */
  @Suppress("UNCHECKED_CAST")
  inline fun <reified T> forType(): TransientCache<T> {
    return typedCache.getOrPut(T::class.java) { TransientCache<T>(clock) } as TransientCache<T>
  }

  /**
   * Clears all cached data in this session.
   */
  fun clearAll() {
    keyCache.forEach { it.value.clear() }
    typedCache.forEach { it.value.clear() }
  }
}
