package com.thankyou.appfoundation.data

import android.content.SharedPreferences

/**
 * Stores session data that persists after application close.
 *
 * To persist session data for different users, provide a different [prefs]
 * instance per user.
 */
class WalletSession(
    private val prefs: SharedPreferences,
) {

    var walletId: String?
        get() = prefs.getString(WALLET_ID, null)
        set(value) = prefs.edit().putString(WALLET_ID, value).apply()

    var defId: String?
        get() = prefs.getString(DEF_ID, null)
        set(value) = prefs.edit().putString(DEF_ID, value).apply()

    var requestId: String?
        get() = prefs.getString(REQUEST_ID, null)
        set(value) = prefs.edit().putString(REQUEST_ID, value).apply()

    var credManId: String?
        get() = prefs.getString(CREDMAN_ID, null)
        set(value) = prefs.edit().putString(CREDMAN_ID, value).apply()

    var birthDate: String?
        get() = prefs.getString(BIRTH_DATE, null)
        set(value) = prefs.edit().putString(BIRTH_DATE, value).apply()

    var credentialReferentId: String?
        get() = prefs.getString(CREDENTIAL_REFERENT_ID, null)
        set(value) = prefs.edit().putString(CREDENTIAL_REFERENT_ID, value).apply()

    private var gender: String?
        get() = prefs.getString(GENDER, null)
        set(value) = prefs.edit().putString(GENDER, value).apply()

    fun clear() {
        prefs.edit().clear().apply()
    }

    companion object {
        private const val WALLET_ID = "WALLET_ID"
        private const val DEF_ID = "DEF_ID"
        private const val REQUEST_ID = "REQUEST_ID"
        private const val CREDMAN_ID = "CREDMAN_ID"
        private const val GENDER = "GENDER"
        private const val BIRTH_DATE = "BIRTH_DATE"
        private const val CREDENTIAL_REFERENT_ID = "CREDENTIAL_REFERENT_ID"
    }
}
