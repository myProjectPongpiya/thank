package com.thankyou.appfoundation.data

import com.thankyou.appfoundation.i18n.models.SupportedLanguage

class CommonContext {
  var appLanguage: SupportedLanguage = SupportedLanguage.THAI
}
