package com.thankyou.appfoundation.list.notificationlist

import android.os.Parcelable
import com.thankyou.appfoundation.graphics.DrImage
import com.thankyou.appfoundation.notifications.FirebaseNotificationType
import kotlinx.android.parcel.Parcelize

sealed class NotificationItem {

    @Parcelize
    data class Item(
        val id: Int,
        val image: DrImage,
        val title: String,
        val body: String,
        val date: String,
        val notificationType: FirebaseNotificationType?
    ) : Parcelable, NotificationItem()

    data class Load(
        val placeholder: String? = null
    ) : NotificationItem()
}