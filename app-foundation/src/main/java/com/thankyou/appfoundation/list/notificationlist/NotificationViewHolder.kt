package com.thankyou.appfoundation.list.notificationlist

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.thankyou.appfoundation.extensions.setImage
import com.thankyou.appfoundation.extensions.visible
import kotlinx.android.synthetic.main.item_notification.view.ivNotificationType
import kotlinx.android.synthetic.main.item_notification.view.tvTitleNotification
import kotlinx.android.synthetic.main.item_notification.view.tvBodyNotification
import kotlinx.android.synthetic.main.item_notification.view.tvDateNotification
import kotlinx.android.synthetic.main.item_progress_bar.view.*

internal class NotificationViewHolder(
    val view: View,
    private val onClickListener: (position: Int) -> Unit
) : RecyclerView.ViewHolder(view), View.OnClickListener {

    override fun onClick(v: View?) {
        onClickListener.invoke(adapterPosition)
    }

    fun bindData(item: NotificationItem) {
        when (item) {
            is NotificationItem.Item -> bindItem(item)
            is NotificationItem.Load -> setLoading()
        }
    }

    private fun bindItem(item: NotificationItem.Item) {
        itemView.apply {
            tvTitleNotification.text = item.title
            tvBodyNotification.text = item.body
            tvDateNotification.text = item.date
            ivNotificationType.setImage(item.image)
        }
        itemView.setOnClickListener(this)
    }

    private fun setLoading() {
        itemView.progressList.visible()
    }
}