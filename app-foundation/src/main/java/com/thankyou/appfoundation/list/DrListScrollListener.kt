package com.thankyou.appfoundation.list

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class DrListScrollListener : RecyclerView.OnScrollListener() {

    var currentPage = INITIAL_PAGE
    var isLoading = false
    var isLastPage = false

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        // Check whether it's scroll down or up
        if (dy <= 0) return
        val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
        val currentLastItemPosition = linearLayoutManager.findLastVisibleItemPosition()
        val lastItemPosition = linearLayoutManager.itemCount - 1
        // Check whether the pagination is the last or not
        if (isLastPage) return

        // Check the loading status whether it's still loading or not
        if (isLoading) return

        // Check the list whether it is nearing the end of the list or not
        if (currentLastItemPosition + LOAD_MORE_THRESHOLD >= lastItemPosition
            && lastItemPosition > EMPTY_LIST) {
            currentPage++
            onLoadMore(currentPage, SIZE)
            isLoading = true
        }
    }

    abstract fun onLoadMore(page: Int, size: Int)

    companion object {
        const val EMPTY_LIST = 0
        const val INITIAL_PAGE = 1
        const val SIZE = 20
        private const val LOAD_MORE_THRESHOLD = 1
    }
}
