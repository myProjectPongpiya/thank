package com.thankyou.appfoundation.list.notificationlist

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import com.thankyou.appfoundation.list.DrBaseInfinityScrollList

class NotificationRecyclerViewList @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : DrBaseInfinityScrollList(context, attrs, defStyle) {

    private val adapter = NotificationAdapter()

    init {
        super.initRecyclerView(
            adapter = adapter,
            layout = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false),
            decor = null
        )
    }

    fun setItems(items: List<NotificationItem>?) {
        adapter.setItems(items ?: emptyList())
    }

    fun showProgressBar() {
        adapter.showProgressBar()
    }

    fun hideProgressBar() {
        adapter.hideProgressBar()
    }

    fun setOnItemClickListener(listener: ((id: Int) -> Unit)?) {
        adapter.setOnClickListener(listener)
    }
}
