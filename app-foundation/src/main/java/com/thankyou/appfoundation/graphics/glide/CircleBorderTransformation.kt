package com.thankyou.appfoundation.graphics.glide

import android.graphics.Bitmap
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation
import com.thankyou.appfoundation.graphics.BitmapUtils
import java.security.MessageDigest

data class CircleBorderTransformation(
    val borderSize: Float,
    val borderColor: Int
) : BitmapTransformation() {

    override fun transform(pool: BitmapPool, toTransform: Bitmap, outWidth: Int, outHeight: Int): Bitmap {
        return BitmapUtils.addCircleBorder(toTransform, borderSize, borderColor)
    }

    override fun updateDiskCacheKey(messageDigest: MessageDigest) {
        messageDigest.update(hashCode().toString().toByteArray())
    }
}
