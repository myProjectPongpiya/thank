package com.thankyou.appfoundation.graphics

import android.os.Parcelable
import androidx.annotation.DrawableRes
import kotlinx.android.parcel.Parcelize
import android.graphics.Bitmap as BitmapImage

/**
 * A model class representing a loadable [DrImage].
 */
sealed class DrImage : Parcelable {

    abstract val transformations: List<ImageTransformations>
    abstract val contentDescription: String?
    abstract val resizeWidth: Int
    abstract val resizeHeight: Int

    @Parcelize
    data class Url(
        val url: String,
        @DrawableRes val placeholder: Int? = null,
        @DrawableRes val error: Int? = null,
        override val transformations: List<ImageTransformations> = emptyList(),
        override val contentDescription: String? = null,
        override val resizeWidth: Int = 0,
        override val resizeHeight: Int = 0
    ) : DrImage()

    @Parcelize
    data class ResId(
        @DrawableRes val resId: Int,
        override val transformations: List<ImageTransformations> = emptyList(),
        override val contentDescription: String? = null,
        override val resizeWidth: Int = 0,
        override val resizeHeight: Int = 0
    ) : DrImage()

    @Parcelize
    data class Bitmap(
        val bitmap: BitmapImage,
        override val transformations: List<ImageTransformations> = emptyList(),
        override val contentDescription: String? = null,
        override val resizeWidth: Int = 0,
        override val resizeHeight: Int = 0
    ) : DrImage()
}
