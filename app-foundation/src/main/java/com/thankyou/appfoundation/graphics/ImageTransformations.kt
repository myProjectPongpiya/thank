package com.thankyou.appfoundation.graphics

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Parcelable
import com.bumptech.glide.load.Transformation
import com.thankyou.appfoundation.graphics.glide.CircleBorderTransformation
import kotlinx.android.parcel.Parcelize

sealed class ImageTransformations : Parcelable {

    /**
     * Applies a circle crop to the image.
     */
    @Parcelize
    object CircleCrop : ImageTransformations()

    /**
     * Adds a circled border to the image.
     */
    @Parcelize
    data class CircleBorder(val size: Float, val color: Int = Color.BLACK) : ImageTransformations()

    /**
     * Returns the equivalent Glide [Transformation] for this [ImageTransformations].
     */
    fun toGlideTransformation(): Transformation<Bitmap> {
        return when (this) {
            is CircleCrop -> com.bumptech.glide.load.resource.bitmap.CircleCrop()
            is CircleBorder -> CircleBorderTransformation(this.size, this.color)
        }
    }
}
