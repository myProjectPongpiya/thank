package com.thankyou.appfoundation.service

import android.content.Context
import com.google.android.gms.common.GoogleApiAvailabilityLight
import com.huawei.hms.api.HuaweiApiAvailability

fun Context.isGmsAvailable(): Boolean =
    GoogleApiAvailabilityLight
        .getInstance()
        .isGooglePlayServicesAvailable(this) ==
            com.google.android.gms.common.ConnectionResult.SUCCESS

fun Context.isHmsAvailable(): Boolean =
    HuaweiApiAvailability
        .getInstance()
        .isHuaweiMobileServicesAvailable(this) ==
            com.huawei.hms.api.ConnectionResult.SUCCESS

fun Context.startGmsHmsCheck(
    isGmsAvailableCallback: (() -> Unit)? = null,
    isHmsAvailableCallback: (() -> Unit)? = null,
    noServiceAvailableCallback: (() -> Unit)? = null,
) {
    when {
        this.isGmsAvailable() -> {
            isGmsAvailableCallback?.invoke()
        }
        this.isHmsAvailable() -> {
            isHmsAvailableCallback?.invoke()
        }
        else -> {
            noServiceAvailableCallback?.invoke()
        }
    }
}
