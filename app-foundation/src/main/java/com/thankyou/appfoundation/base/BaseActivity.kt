package com.thankyou.appfoundation.base

import android.Manifest
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.annotation.IdRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import com.bumptech.glide.manager.SupportRequestManagerFragment
import com.i18next.android.Operation
import com.thankyou.appfoundation.R
import com.thankyou.appfoundation.dialog.DrDialogFragment
import com.thankyou.appfoundation.dialog.DrHorizontalMultiButtonPopupDialog
import com.thankyou.appfoundation.dialog.DrPopupDialog
import com.thankyou.appfoundation.dialog.DrVerticalMultiButtonPopupDialog
import com.thankyou.appfoundation.extensions.launchDeeplink
import com.thankyou.appfoundation.i18n.T
import com.thankyou.appfoundation.permissions.PermissionResult
import com.thankyou.appfoundation.permissions.PermissionUtils
import com.thankyou.appfoundation.text.CustomSpannable
import com.thankyou.appfoundation.ui.Alert
import com.thankyou.appfoundation.ui.LoadingOverlayDialog
import com.thankyou.appfoundation.ui.ToastUtils
import com.thankyou.appfoundation.ui.UIController
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlin.coroutines.Continuation

abstract class BaseActivity : AppCompatActivity(), LifecycleOwner, CoroutineScope, UIController {

    override val coroutineContext = lifecycleScope.coroutineContext

    /**
     * The central handler used to handle privilege escalation requests.
     */

    override val disposeBag: MutableList<() -> Unit>
        get() = mutableListOf()

    private val loadingOverlay: LoadingOverlayDialog by lazy {
        LoadingOverlayDialog(context = this, coroutineContext = coroutineContext)
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    override fun onDestroy() {
        disposeBag.forEach { it.invoke() }
        super.onDestroy()
    }

    override fun setLoading(isLoading: Boolean, vararg labels: String) {
        if (isLoading) {
            loadingOverlay.show(labels.toList())
        } else {
            loadingOverlay.dismiss()
        }
    }

    override fun showToast(message: String, toastType: Alert.ToastType) {
        when (toastType) {
            Alert.ToastType.SUCCESS -> ToastUtils.showSuccess(this, message)
            Alert.ToastType.PENDING -> ToastUtils.showPending(this, message)
            Alert.ToastType.ERROR -> ToastUtils.showError(this, message)
        }
    }

    override fun showPopup(
        title: String?,
        message: String?,
        subMessage: CustomSpannable?,
        buttonLabel: String?,
        heroImage: Int?,
        onDismiss: (() -> Unit)?
    ) {
        val dialog = DrPopupDialog.newInstance(
            title = title ?: "",
            message = message ?: "",
            subMessage = subMessage,
            buttonText = buttonLabel ?: "OK",
            heroImage = heroImage,
            onDismiss = onDismiss
        )
        dialog.show(supportFragmentManager, DrPopupDialog::class.java.simpleName)
    }

    override fun showPopupWithMultiButton(
        title: String?,
        message: String?,
        heroImage: Int?,
        onDismiss: (() -> Unit)?,
        negativeButtonLabel: String?,
        negativeButtonListener: (() -> Unit)?,
        positiveButtonLabel: String?,
        positiveButtonListener: (() -> Unit)?,
        multiButtonType: Alert.MultiButtonType
    ) {
        val dialog = when (multiButtonType) {
            Alert.MultiButtonType.HORIZONTAL -> DrHorizontalMultiButtonPopupDialog.newInstance(
                title = title ?: "",
                message = message,
                heroImage = heroImage,
                onDismiss = onDismiss,
                secondaryButtonText = negativeButtonLabel ?: "Cancel",
                onCancelClick = negativeButtonListener,
                primaryButtonText = positiveButtonLabel ?: "Ok",
                onOkClick = positiveButtonListener
            )
            Alert.MultiButtonType.VERTICAL -> DrVerticalMultiButtonPopupDialog.newInstance(
                title = title ?: "",
                message = message ?: "",
                heroImage = heroImage,
                onDismiss = onDismiss,
                secondaryButtonText = negativeButtonLabel ?: "Cancel",
                onCancelClick = negativeButtonListener,
                primaryButtonText = positiveButtonLabel ?: "Ok",
                onOkClick = positiveButtonListener
            )
        }
        dialog.show(supportFragmentManager, dialog::class.java.simpleName)
    }

    override fun showNativePopup(
        title: String?,
        message: String?,
        negativeButtonLabel: String?,
        negativeButtonListener: (() -> Unit)?,
        positiveButtonLabel: String?,
        positiveButtonListener: (() -> Unit)?,
    ) {
        val builder = AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setCancelable(false)

        positiveButtonLabel?.let {
            builder.setPositiveButton(it) { dialog, _ ->
                dialog.dismiss()
                positiveButtonListener?.invoke()
            }
        }

        negativeButtonLabel?.let {
            builder.setNegativeButton(it) { dialog, _ ->
                dialog.cancel()
                negativeButtonListener?.invoke()
            }
        }

        builder.create().show()
    }

    override fun launchDeepLink(uri: String, internalOnly: Boolean, onNotSupported: (() -> Unit)?) {
        launchDeeplink(Uri.parse(uri), internalOnly, onNotSupported)
    }

    override fun showNoConnectionError(onConnected: () -> Unit) {
        showPopup(
            title = "No internet",
            message = "Please connect to internet",
            buttonLabel = "Try again",
            onDismiss = { onConnected.invoke() }
        )
    }

    override fun requestPermissions(
        vararg permissions: String,
        onDenied: (() -> Unit)?,
        onGranted: () -> Unit
    ) {
        requestPermissionsWithRationale(
            *permissions,
            onResult = {
                when (it) {
                    is PermissionResult.RationaleRequired -> PermissionUtils.onAskPermissions(it)
                    is PermissionResult.Granted -> onGranted.invoke()
                    is PermissionResult.Denied -> onDenied?.invoke()
                }
            }
        )
    }

    override fun requestPermissionsWithRationale(
        vararg permissions: String,
        onResult: (PermissionResult) -> Unit
    ) {
        PermissionUtils.checkPermissions(
            activity = this,
            permissions = permissions.toList(),
            onDenied = { onResult.invoke(PermissionResult.Denied) },
            onRationaleRequired = { onResult.invoke(it) },
            onGranted = { onResult.invoke(PermissionResult.Granted) }
        )
    }

    override fun gotoPage(navDirections: NavDirections) {
        (this as? UIController)?.gotoPage(navDirections)
    }

    override fun goBack(@IdRes destinationId: Int?) {
        (this as? UIController)?.goBack(destinationId)
    }

    override fun onCallPhoneNumber(phoneNo: String) {
        requestPermissions(Manifest.permission.CALL_PHONE) {
            val number = MOCK_TEL_MOBILE + phoneNo
            val intent = Intent(Intent.ACTION_CALL)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.data = Uri.parse(number)
            this.startActivity(intent)
        }
    }

    override fun onCallEmail(email: String) {
        val intent = Intent(Intent.ACTION_SENDTO)
        val scheme = "mailto"
        intent.data = Uri.fromParts(scheme, email, null)
        intent.putExtra(
            Intent.EXTRA_SUBJECT,
            ""
        )
        this.startActivity(intent)
    }

    override fun onCallWebBrowser(link: String) {
        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse(link)
        )
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        this.startActivity(intent)
    }

    override fun onDeeplinkPreRoute(continuation: Continuation<Unit>) {
        val topFragment = supportFragmentManager.fragments.lastOrNull {
            it !is SupportRequestManagerFragment
        }
        (topFragment as? UIController)?.onDeeplinkPreRoute(continuation)
    }

    /**
     * Set window soft input mode.
     */
    fun setWindowSoftInputMode(mode: Int) {
        window.setSoftInputMode(mode)
    }

    override fun onBackPressed() {
        val topFragment = getTopFragment()

        launch {
            // Give an opportunity for the fragment to handle the back button. If the
            // resulting value is `false`, it means that the fragment has handled the
            // back button entirely and should skip the default behaviour.
            if ((topFragment as? ScreenFragment)?.onBackPressed() == true) {
                return@launch
            }

            // Otherwise defer to the default activity back button handling
            super.onBackPressed()
        }
    }

    private fun getTopFragment(): Fragment? {
        val navHostFragment = supportFragmentManager.fragments.lastOrNull {
            it !is SupportRequestManagerFragment
        }
        return navHostFragment?.childFragmentManager?.fragments?.get(0)
    }

    companion object {
        private val GREEN_BOLD_TITLE_STYLE =
            CustomSpannable.Style.TextAppearance(R.style.DrTextAppearance_Green_Bold_Title)
        private const val MOCK_TEL_MOBILE = "tel:"
    }
}
