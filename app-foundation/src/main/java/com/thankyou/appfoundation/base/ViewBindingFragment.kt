package com.thankyou.appfoundation.base

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.viewbinding.ViewBinding

abstract class ViewBindingFragment<Binding : ViewBinding>(
    @LayoutRes contentLayoutId: Int
) : ScreenFragment(contentLayoutId) {

    private var _layout: Binding? = null

    protected val layout: Binding
        get() = _layout!!

    /**
     * Returns the [ViewBinding] class for this Fragment's layout.
     */
    abstract fun initializeLayoutBinding(view: View): Binding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _layout = initializeLayoutBinding(view)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _layout = null
    }
}
