package com.thankyou.appfoundation.authentication

import com.google.gson.annotations.SerializedName

data class RefreshTokenRequest(
    @SerializedName("refreshToken")
    val refreshToken: String? = null,
)
