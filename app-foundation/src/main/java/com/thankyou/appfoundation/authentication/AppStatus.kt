package com.thankyou.appfoundation.authentication

import com.google.gson.annotations.SerializedName

enum class AppStatus {

    @SerializedName("ACTIVE")
    ACTIVE,

    @SerializedName("INACTIVE")
    INACTIVE,

    @SerializedName("SUSPENDED")
    SUSPENDED
}