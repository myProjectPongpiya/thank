package com.thankyou.appfoundation.authentication

interface TokenService {

    fun getToken(): AuthorizationToken?

    fun setToken(token: AuthorizationToken)

    fun clear()
}
