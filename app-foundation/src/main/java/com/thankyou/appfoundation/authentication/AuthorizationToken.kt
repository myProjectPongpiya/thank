package com.thankyou.appfoundation.authentication

data class AuthorizationToken(
    val accessToken: String,
    val refreshToken: String?
)
