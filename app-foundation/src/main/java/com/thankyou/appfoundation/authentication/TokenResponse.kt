package com.thankyou.appfoundation.authentication

import com.google.gson.annotations.SerializedName

/**
 * Represents an error response from the API.
 */
data class TokenResponse(
    @SerializedName("content")
    val content: TokenResponseModel? = null,
) {

    data class TokenResponseModel(
        /**
         * The access_token code.
         */
        @SerializedName("access_token")
        val accessToken: String,

        /**
         * The refresh token code
         */
        @SerializedName("refresh_token")
        val refreshToken: String,

        @SerializedName("appStatus")
        val appStatus: AppStatus? = null,

        @SerializedName("isUpdate")
        val isUpdate: Boolean? = false,

        /**
         * The token uuid code.
         */
        @SerializedName("tokenUuid")
        val tokenUuid: String? = null,
    )

    companion object {
        const val APP_STATUS = "APP_STATUS"
    }
}
