package com.thankyou.appfoundation.logging

import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.thankyou.appfoundation.extensions.takeIfNotBlank
import com.thankyou.appfoundation.http.ApiException
import timber.log.Timber

class CrashlyticsTree : Timber.Tree() {

  override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
    if (t != null) {
      t.logApiException(tag)
      FirebaseCrashlytics.getInstance().recordException(t)
    }

    // Log top-most fragment
    if (tag == SCREEN_FRAGMENT_TAG) {
      FirebaseCrashlytics.getInstance().setCustomKey(SCREEN_FRAGMENT_TAG, message)
    }
  }

  private fun Throwable.logApiException(tag: String?) = try {
    if (this is ApiException) {
      val responseSummary = buildString {
        this@logApiException.response?.request()?.url()?.toString()?.let { append("URL:$it; ") }
        this@logApiException.response?.code()?.let { append("HTTPCODE:$it; ") }
        this@logApiException.header?.errorDisplay?.code?.let { append("APICODE:$it; ") }
      }.takeIfNotBlank()?.trim()
      responseSummary?.let { FirebaseCrashlytics.getInstance().setCustomKey(API_URL_TAG, it) }
    } else {
      FirebaseCrashlytics.getInstance().setCustomKey(tag ?: EXCEPTION, this.toString())
    }
  } catch (t: Throwable) {
    // do nothing
  }

  companion object {
    private const val SCREEN_FRAGMENT_TAG = "SCREEN_FRAGMENT"
    private const val API_URL_TAG = "API_URL"
    private const val EXCEPTION = "EXCEPTION"
  }
}
