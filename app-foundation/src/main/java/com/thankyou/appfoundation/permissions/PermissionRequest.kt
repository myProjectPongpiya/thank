package com.thankyou.appfoundation.permissions

data class PermissionRequest(
    val permissions: List<String>,
    val onResult: (PermissionResult) -> Unit
)
