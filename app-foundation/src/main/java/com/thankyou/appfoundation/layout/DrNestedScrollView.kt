package com.thankyou.appfoundation.layout

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.widget.NestedScrollView

class DrNestedScrollView : NestedScrollView, View.OnTouchListener {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    init {
        setOnTouchListener(this)
    }

    /**
     * Automatically hide keyboard when the user attempts to scroll.
     */
    override fun onTouch(v: View, event: MotionEvent?): Boolean {
        if (event == null || event.action != MotionEvent.ACTION_MOVE) {
            return false
        }

        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager

        if (imm?.isAcceptingText == true) {
            imm.hideSoftInputFromWindow(v.windowToken, 0)
        }

        return false
    }
}
