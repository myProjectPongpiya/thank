package com.thankyou.appfoundation.http.interceptor

import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.thankyou.appfoundation.authentication.TokenService
import com.thankyou.appfoundation.http.ApiException
import com.thankyou.appfoundation.http.ApiException.Companion.ERROR_CODE_TOKEN_EXPIRED
import com.thankyou.appfoundation.http.ApiException.Companion.ERROR_CODE_TOKEN_MISMATCH
import com.thankyou.appfoundation.http.TokenMisMatchException
import com.thankyou.appfoundation.http.TokenRefreshException
import okhttp3.Interceptor
import okhttp3.Response

class TokenRefreshInterceptor(
    private val tokenService: TokenService,
    private val tokenRefreshService: TokenRefreshService,
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)

        if (response.isMissMatchToken()) {
            tokenService.clear()
            return chain.proceed(throw TokenMisMatchException())
        }

        // Return if the response does not indicate that the token needs to be refreshed
        if (!response.isExpiredToken()) {
            return response
        }

        val url = chain.request().url()
        FirebaseCrashlytics.getInstance().log("token refresh expired url:$url")

        // Otherwise attempt to refresh the token and try again
        val newRequest = try {
            tokenRefreshService.refresh(request)
        } catch (e: Exception) {
            throw when (e is ApiException) {
                true -> TokenRefreshException.IsApiException(e)
                else -> TokenRefreshException.IsSystemError(e)
            }
        }

        return chain.proceed(newRequest)
    }

    private fun Response.isExpiredToken(): Boolean {
        return this.code() == ERROR_CODE_TOKEN_EXPIRED
    }

    private fun Response.isMissMatchToken(): Boolean {
        return this.code() == ERROR_CODE_TOKEN_MISMATCH
    }
}
