package com.thankyou.appfoundation.http

import com.google.gson.annotations.SerializedName

/**
 * Represents an error response from the API.
 */
data class ApiHeader(
    @SerializedName("reqID")
    val reqID: String? = null,
    @SerializedName("reqDtm")
    val reqDtm: String? = null,
    @SerializedName("service")
    val service: String? = null,
    @SerializedName("txnRefID")
    val txnRefID: String? = null,
    @SerializedName("errorDisplay")
    val errorDisplay: ErrorDisplay? = null,
    @SerializedName("appVersion")
    val appVersion: String? = null,
    @SerializedName("clientOS")
    val clientOS: String? = null
)
