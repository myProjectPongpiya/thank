package com.thankyou.appfoundation.http.interceptor

import java.io.IOException

class NoConnectivityException : IOException()
