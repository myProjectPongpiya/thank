package com.thankyou.appfoundation.http

import java.io.IOException

class TokenMisMatchException : IOException()
