package com.thankyou.appfoundation.http

import java.io.IOException

sealed class TokenRefreshException : IOException() {
    data class IsApiException(val exception: ApiException) : TokenRefreshException()
    data class IsSystemError(val exception: Exception) : TokenRefreshException()
}
