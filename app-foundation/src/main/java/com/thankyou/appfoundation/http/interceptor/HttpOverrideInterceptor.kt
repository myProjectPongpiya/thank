package com.thankyou.appfoundation.http.interceptor

import com.thankyou.appfoundation.http.HttpOverride
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import retrofit2.Invocation
import java.util.concurrent.TimeUnit

/**
 * The [HttpOverrideInterceptor] applies the necessary overrides to methods that are
 * annotated with [HttpOverride]. Uses [Invocation] to dynamically parse custom annotations
 * at runtime.
 *
 * See [Stack Overflow](https://stackoverflow.com/a/53557137).
 *
 * @requires Retrofit >= 2.5.0
 */
class HttpOverrideInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        // Read annotations
        val tag = request.tag(Invocation::class.java) ?: return chain.proceed(request)
        val timeoutOverride = tag.method().getAnnotation(HttpOverride.Timeout::class.java)
        val baseUrlOverride = tag.method().getAnnotation(HttpOverride.BaseUrl::class.java)

        // Proceed as usual if there are no overrides
        if (timeoutOverride == null && baseUrlOverride == null) {
            return chain.proceed(request)
        }

        var newChain = chain
        var newRequest = request

        // Override timeout
        if (timeoutOverride != null) {
            newChain = overrideTimeout(chain, timeoutOverride)
        }

        // Override base url
        if (baseUrlOverride != null) {
            newRequest = overrideBaseUrl(newRequest, baseUrlOverride)
        }

        return newChain.proceed(newRequest)
    }

    /**
     * Returns a [Interceptor.Chain] with the new timeout values-hdpi.
     */
    private fun overrideTimeout(
        chain: Interceptor.Chain,
        timeoutOverride: HttpOverride.Timeout
    ): Interceptor.Chain {
        return chain
            .withConnectTimeout(timeoutOverride.millis, TimeUnit.MILLISECONDS)
            .withReadTimeout(timeoutOverride.millis, TimeUnit.MILLISECONDS)
            .withWriteTimeout(timeoutOverride.millis, TimeUnit.MILLISECONDS)
    }

    /**
     * Returns a new [Request] with the updated base url.
     */
    private fun overrideBaseUrl(request: Request, baseUrlOverride: HttpOverride.BaseUrl): Request {
        val newUrl = request.url().newBuilder()
            .scheme(baseUrlOverride.scheme)
            .host(baseUrlOverride.baseUrl)
            .port(baseUrlOverride.port)
            .build()
        return request.newBuilder()
            .url(newUrl)
            .build()
    }
}
