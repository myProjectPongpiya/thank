package com.thankyou.appfoundation.http.interceptor

import com.thankyou.appfoundation.authentication.TokenService
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

/**
 * [TokenInjectorInterceptor] injects access tokens to outgoing requests.
 *
 * You must supply it with an implementation of [TokenService] for it to
 * retrieve the appropriate tokens.
 */
class TokenInjectorInterceptor(
    private val tokenService: TokenService
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val authToken = tokenService.getToken() ?: return chain.proceed(request)

        return chain.proceed(request.injectAuthorizationHeader("Bearer ${authToken.accessToken}"))
    }

    private fun Request.injectAuthorizationHeader(header: String) = newBuilder()
        .header("Authorization", header)
        .build()
}
