package com.thankyou.appfoundation.http

import okhttp3.Response
import java.io.IOException

/**
 * [ApiException] is thrown when the API fails to return a successful response.
 *
 * A response is deemed not successful when the HTTP status code is not from any of
 * the 2XX or 3XX series.
 *
 * It may optionally include an [ApiHeader] object containing the relevant error
 * codes and messages produced by the API. It may also optionally include the
 * [Response] and [Request] that resulted in this exception.
 */
class ApiException(
    val header: ApiHeader? = null,
    val response: Response? = null
) : IOException("There was a problem processing the request.") {

    constructor(header: ApiHeader?) : this(header, null)
    constructor() : this(null, null)

    val title: String?
        get() = header?.errorDisplay?.titleTh

    override val message: String?
        get() = header?.errorDisplay?.messageTh ?: super.message

    companion object {
        const val ERROR_CODE_TOKEN_EXPIRED = 401
        const val ERROR_CODE_TOKEN_MISMATCH = 403

        //        104XX
        const val ERROR_CODE_VERIFY_PIN_COUNT = "10401"
        const val ERROR_CODE_VERIFY_PIN_MAXIMUM_COUNT = "10402"
        const val ERROR_CODE_MISTAKE_OVER_LIMIT_FOR_FORGOT_PIN = "10414"
        const val ERROR_CODE_DUPLICATE_USER = "10433"

        //        105XX
        const val ERROR_CODE_ACCEPT_CREDENTIAL_ERROR = "10511"
        const val ERROR_CODE_REQUEST_OTP_OVER_LIMIT = "10541"
        const val ERROR_CODE_VERIFY_OTP_COUNT = "10542"
        const val ERROR_CODE_VERIFY_OTP_MAXIMUM_COUNT = "10543"
        const val ERROR_CODE_VERIFY_OTP_EXPIRED = "10544"
        const val ERROR_CODE_FORMAT = "10560"

        //        114XX
        const val ERROR_CODE_DOCTOR_NOT_ALLOW_FOR_THIS_IP = "11410"
        const val ERROR_CODE_DOCTOR_INVALID_AUTHORIZATION = "11411"
        const val ERROR_CODE_DOCTOR_THIS_API_KEY_IS_DISABLED = "11412"
        const val ERROR_CODE_ROLE_OR_BY_ACCESS_FIELD = "11413"
        const val ERROR_CODE_SOME_PARAMETER_IS_EMPTY = "11414"
        const val ERROR_CODE_MISSING_NAME_OR_LASTNAME = "11415"
        const val ERROR_CODE_MISSING_CODE_OR_PIN = "11416"
        const val ERROR_CODE_NOT_FOUND_MD = "11417"
        const val ERROR_CODE_CHARACTER_NOT_ALLOW = "11418"
        const val ERROR_CODE_MD_CONNECT_AUTH = "11419"
        const val ERROR_CODE_REQUIRE_ADDRESS_ID = "11420"
        const val ERROR_CODE_REQUIRE_ADDRESS_NO = "11421"
        const val ERROR_CODE_REQUIRE_ADDRESS_SUB_DISTRICT_NAME = "11422"
        const val ERROR_CODE_REQUIRE_ADDRESS_DISTRICT_NAME = "11423"
        const val ERROR_CODE_REQUIRE_ADDRESS_PROVINCE_ID = "11424"
        const val ERROR_CODE_REQUIRE_ADDRESS_PROVINCE_NAME = "11425"
        const val ERROR_CODE_REQUIRE_ADDRESS_POST_CODE = "11426"
        const val ERROR_CODE_REQUIRE_ADDRESS_CONTRACT_PHONE = "11427"
        const val ERROR_CODE_THIS_ADDRESS_DO_NOT_EXIST = "11428"
        const val ERROR_CODE_METHOD_NOT_ALLOW = "11429"
        const val ERROR_CODE_MISSING_PERSONAL_ID = "11430"
        const val ERROR_CODE_MISSING_BIRTH_DATE = "11431"
        const val ERROR_CODE_INVALID_DATE_OR_FORMAT = "11432"
        const val ERROR_CODE_NO_MATCHING_USER_FOUND = "11433"
        const val ERROR_CODE_NO_MATCH_PERSONAL_ID_PID = "11434"
        const val ERROR_CODE_NO_MATCH_BIRTH_DATE = "11435"
        const val ERROR_CODE_NO_MATCH_NAME = "11436"
        const val ERROR_CODE_NO_MATCH_SURNAME = "11437"
        const val ERROR_CODE_REQUIRE_MD_CODE = "11438"
        const val ERROR_CODE_REQUIRE_PRIMARY_CONTACT_INFORMATION = "11439"
        const val ERROR_CODE_INVALID_MOBILE_NUMBER = "11440"
        const val ERROR_CODE_INVALID_EMAIL_FORMAT = "11441"
        const val ERROR_CODE_MISTAKE_OVER_LIMIT = "11443"
        const val ERROR_CODE_DOCTOR_STATUS = "11444"

        //        115XX
        const val ERROR_CODE_NO_API_CODE = "11510"
        const val ERROR_CODE_INCORRECT_CODE_OR_PIN_CODE = "11511"
        const val ERROR_CODE_PIN_CODE_NOT_SET = "11512"
        const val ERROR_CODE_TEMPORARILY_SUSPENDED = "11513"
        const val ERROR_CODE_DOES_NOT_MEET_ETHICAL_CONDITION = "11514"
        const val ERROR_CODE_IS_DEAD = "11515"
        const val ERROR_CODE_IMAGE_DOSE_NOT_EXISTS = "11516"
        const val ERROR_CODE_ADDRESS_UPDATE_FAILED = "11517"
        const val ERROR_CODE_PIN_CODE_HAS_BEEN_DISABLED = "11518"
        const val ERROR_CODE_UPDATE_PRIMARY_CONTACT_FAILED = "11519"

        //        116XX
        const val ERROR_CODE_PATCH_UPDATE_MOBILE_FAILED = "11601"
        const val ERROR_CODE_PATCH_UPDATE_EMAIL_FAILED = "11602"
        const val ERROR_CODE_PATCH_UPDATE_ADDRESS_FAILED = "11603"
        const val ERROR_CODE_PATCH_UPDATE_MOBILE_AND_EMAIL_FAILED = "11604"
        const val ERROR_CODE_PATCH_UPDATE_ADDRESS_FAILED_11605 = "11605"

        //        125XX
        const val ERROR_CODE_INVALID_OR_INACTIVE_SESSION = "12510"
        const val ERROR_CODE_INVALID_OR_INACTIVE_API_KEY = "12511"
        const val ERROR_CODE_EMPTY_PARAMETER = "12512"
        const val ERROR_CODE_NOT_FOUND = "12513"
        const val ERROR_CODE_PERMISSION_DENIED = "12514"
        const val ERROR_CODE_UNKNOWN_ERRORR = "12515"
    }
}
