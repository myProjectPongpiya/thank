package com.thankyou.appfoundation.http.interceptor

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.thankyou.appfoundation.authentication.AuthorizationToken
import com.thankyou.appfoundation.authentication.TokenResponse
import com.thankyou.appfoundation.authentication.TokenService
import com.thankyou.appfoundation.http.ApiHeader
import com.thankyou.appfoundation.http.ApiHeaderResp
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody

/**
 * The [TokenResponseInterceptor] intercepts all incoming responses from the API
 * and checks the [accesstoken,refreshtoken,tokentype and scope] if available and store them in Token Manager.
 *
 */
class TokenResponseInterceptor(
    private val gson: Gson,
    private val tokenService: TokenService
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())

        val tokenResponse = parseForToken(response.peekBody(Long.MAX_VALUE))

        if (tokenResponse?.content?.accessToken == null) {
            return response
        }

        val apiHeader = parseForError(response.peekBody(Long.MAX_VALUE))

        if (apiHeader?.headerResp?.service == AVOID_SERVICE) {
            return response
        }

        val authToken = AuthorizationToken(
            accessToken = tokenResponse.content.accessToken,
            refreshToken = tokenResponse.content.refreshToken
        )

        tokenService.setToken(authToken)

        return response
    }

    companion object {
        private const val AVOID_SERVICE = "VerifyPinCodeHandler"
    }

    /**
     * Maps the response body to an [TokenResponse] object. Returns `null` if it is
     * unable to perform the mapping.
     */
    private fun parseForToken(body: ResponseBody?): TokenResponse? {
        if (body == null) return null
        return try {
            this.gson.fromJson(body.string(), TokenResponse::class.java)
        } catch (e: JsonSyntaxException) {
            null
        }
    }

    /**
     * Maps the response body to an [ApiHeader] object. Returns `null` if it is
     * unable to perform the mapping.
     */
    private fun parseForError(body: ResponseBody?): ApiHeaderResp? {
        if (body == null) return null
        return try {
            this.gson.fromJson(body.string(), ApiHeaderResp::class.java)
        } catch (e: JsonSyntaxException) {
            null
        }
    }
}
