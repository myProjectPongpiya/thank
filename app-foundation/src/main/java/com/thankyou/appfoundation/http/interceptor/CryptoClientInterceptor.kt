package com.thankyou.appfoundation.http.interceptor

import com.thankyou.appfoundation.crypto.encryption.EncryptionService
import okhttp3.Interceptor
import okhttp3.Response

class CryptoClientInterceptor(private val encryption: EncryptionService) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = encryption.encryptRequest(chain.request())
        val response = chain.proceed(request.first)
        return encryption.decryptResponse(response, request.second)
    }
}