package com.thankyou.appfoundation.http.interceptor

import okhttp3.Request

interface TokenRefreshService {

    /**
     * Injects the refreshed access.
     */
    fun refresh(request: Request): Request
}
