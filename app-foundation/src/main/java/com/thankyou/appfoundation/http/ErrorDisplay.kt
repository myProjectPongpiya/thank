package com.thankyou.appfoundation.http

import com.google.gson.annotations.SerializedName

data class ErrorDisplay(
    @SerializedName("code")
    val code: String? = null,
    @SerializedName("titleTh")
    val titleTh: String? = null,
    @SerializedName("messageTh")
    val messageTh: String? = null,
    @SerializedName("titleEn")
    val titleEn: String? = null,
    @SerializedName("messageEn")
    val messageEn: String? = null,
)
