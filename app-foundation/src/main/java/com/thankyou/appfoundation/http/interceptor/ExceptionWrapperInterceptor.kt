package com.thankyou.appfoundation.http.interceptor

import com.thankyou.appfoundation.http.DataLayerException
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class ExceptionWrapperInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        // With the exception of IOException, any exceptions thrown by the OkHttp
        // layers will be treated as a fatal exception -- causing a fatal crash to
        // the app. To remedy this, we wrap all exceptions with our custom exception
        // that subclasses IOException
        return try {
            chain.proceed(chain.request())
        } catch (t: Throwable) {
            if (t !is IOException) {
                throw DataLayerException(t)
            } else {
                throw t
            }
        }
    }
}
