package com.thankyou.appfoundation.extensions

import androidx.lifecycle.LifecycleOwner
import com.thankyou.appfoundation.base.BaseViewModel
import com.thankyou.appfoundation.ui.Alert
import com.thankyou.appfoundation.ui.UIController

/**
 * Binds a [BaseViewModel] with a [UIController]
 */
@SuppressWarnings("LongMethod")
fun BaseViewModel.bindToUiController(
    lifecycleOwner: LifecycleOwner,
    controller: UIController
) {
    if (controller is BaseViewModel) {
        throw IllegalStateException("Cannot bind UIController to another view model.")
    }

    loadingState.observe(lifecycleOwner, {
        controller.setLoading(it.isLoading, *it.labels.toTypedArray())
    })

    gotoPage.observe(lifecycleOwner, {
        controller.gotoPage(it)
    })

    goBack.observe(lifecycleOwner, {
        controller.goBack(it)
    })

    onCallPhoneNumber.observe(lifecycleOwner, {
        controller.onCallPhoneNumber(it)
    })

    onCallEmail.observe(lifecycleOwner, {
        controller.onCallEmail(it)
    })

    onCallWebBrowser.observe(lifecycleOwner, {
        controller.onCallWebBrowser(it)
    })

    onAlertEvent.observe(lifecycleOwner, {
        when (it) {
            is Alert.Toast -> controller.showToast(it.message, it.type)
            is Alert.Popup -> controller.showPopup(
                title = it.title,
                message = it.message,
                buttonLabel = it.buttonLabel,
                heroImage = it.heroImage,
                onDismiss = it.onDismiss
            )
            is Alert.PopupMultiButton -> controller.showPopupWithMultiButton(
                title = it.title,
                message = it.message,
                heroImage = it.heroImage,
                onDismiss = it.onDismiss,
                negativeButtonLabel = it.negativeButtonLabel,
                negativeButtonListener = it.negativeButtonListener,
                positiveButtonLabel = it.positiveButtonLabel,
                positiveButtonListener = it.positiveButtonListener,
                multiButtonType = it.type
            )
            is Alert.NativePopup -> controller.showNativePopup(
                title = it.title,
                message = it.message,
                positiveButtonLabel = it.positiveButtonLabel,
                positiveButtonListener = it.positiveButtonListener,
                negativeButtonLabel = it.negativeButtonLabel,
                negativeButtonListener = it.negativeButtonListener,
            )
        }
    })

    onLaunchDeepLinkEvent.observe(lifecycleOwner) {
        controller.launchDeepLink(it.uri, it.internalOnly, it.onNotSupported)
    }

    onNoConnectionEvent.observe(lifecycleOwner) {
        controller.showNoConnectionError { it.onConnected.invoke() }
    }

    onRestartSession.observe(lifecycleOwner) {
        controller.restartSession()
    }

    onRequestPermissionEvent.observe(lifecycleOwner) {
        controller.requestPermissionsWithRationale(
            *it.permissions.toTypedArray(),
            onResult = it.onResult
        )
    }
}
