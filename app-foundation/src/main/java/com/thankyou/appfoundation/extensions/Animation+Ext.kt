package com.thankyou.appfoundation.extensions

import android.view.animation.Animation

fun Animation.setAnimationListener(
    onStart: ((Animation?) -> Unit)? = null,
    onEnd: ((Animation?) -> Unit)? = null,
    onRepeat: ((Animation?) -> Unit)? = null
) {
    this.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationStart(anim: Animation?) {
            onStart?.invoke(anim)
        }

        override fun onAnimationEnd(anim: Animation?) {
            onEnd?.invoke(anim)
        }

        override fun onAnimationRepeat(anim: Animation?) {
            onRepeat?.invoke(anim)
        }
    })
}
