package com.thankyou.appfoundation.extensions

import android.content.Intent
import android.net.Uri
import androidx.activity.viewModels
import androidx.annotation.MainThread
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.thankyou.appfoundation.base.BaseActivity
import com.thankyou.appfoundation.base.BaseViewModel

/**
 * Launches a deep link [uri]. You may optionally restrict the deep link to
 * the current application via the [internalOnly] flag.
 *
 * If there are no applications that can support the deep link, [onNotSupported]
 * will be invoked.
 */
fun AppCompatActivity.launchDeeplink(
    uri: Uri,
    internalOnly: Boolean = false,
    onNotSupported: (() -> Unit)? = null
) {
    val intent = Intent(Intent.ACTION_VIEW, uri)

    if (internalOnly) {
        intent.setPackage(packageName)
    }

    // Verify that there's an app that can support this deep link.
    if (packageManager.queryIntentActivities(intent, 0).size < 1) {
        onNotSupported?.invoke()
        return
    }

    startActivity(intent)
}

/**
 * Creates a [BaseViewModel] bound to this fragment's lifecycle and UI controller
 * using the instance returned from the [viewModelProducer].
 *
 * If the [viewModelProducer] obtains its instance from a Dagger component, this
 * will allow you to perform constructor injection.
 */
@Suppress("UNCHECKED_CAST")
@MainThread
inline fun <reified VM : BaseViewModel> BaseActivity.bindViewModel(
    noinline viewModelProducer: () -> VM
) = viewModels<VM>(
    factoryProducer = {
        return@viewModels object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                if (modelClass != VM::class.java) {
                    throw IllegalArgumentException("Unexpected argument $modelClass")
                }

                return (viewModelProducer.invoke() as T).also { vm ->
                    (vm as? BaseViewModel)?.bindToUiController(this@bindViewModel, this@bindViewModel)
                }
            }
        }
    }
)
