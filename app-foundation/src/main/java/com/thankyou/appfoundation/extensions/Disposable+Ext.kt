package com.thankyou.appfoundation.extensions

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Adds this disposable to the `disposableBag`.
 */
fun Disposable.disposedBy(disposableBag: CompositeDisposable) {
    disposableBag.add(this)
}
