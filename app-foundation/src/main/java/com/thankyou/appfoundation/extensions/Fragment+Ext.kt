package com.thankyou.appfoundation.extensions

import android.net.Uri
import androidx.annotation.MainThread
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.createViewModelLazy
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.thankyou.appfoundation.base.BaseFragment
import com.thankyou.appfoundation.base.BaseViewModel
import com.thankyou.appfoundation.dialog.DrBottomSheetDialogFragment
import com.thankyou.appfoundation.dialog.DrDialogFragment
import kotlin.reflect.KProperty

/**
 * Creates a [BaseViewModel] bound to this fragment's lifecycle and UI controller
 * using the instance returned from the [viewModelProducer].
 *
 * If the [viewModelProducer] obtains its instance from a Koin component, this
 * will allow you to perform constructor injection.
 */
@Suppress("UNCHECKED_CAST")
@MainThread
inline fun <reified VM : BaseViewModel> BaseFragment.bindViewModel(
    noinline ownerProducer: () -> ViewModelStoreOwner = { this },
    noinline viewModelProducer: (() -> VM)
) = createViewModelLazy(
    viewModelClass = VM::class,
    storeProducer = { ownerProducer().viewModelStore },
    factoryProducer = {
        return@createViewModelLazy object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                if (modelClass != VM::class.java) {
                    throw IllegalArgumentException("Unexpected argument $modelClass")
                }

                return (viewModelProducer.invoke() as T).also { vm ->
                    (vm as? BaseViewModel)?.bindToUiController(
                        viewLifecycleOwner,
                        this@bindViewModel
                    )
                    this@bindViewModel.onViewCreatedListeners.add { _, _ ->
                        (vm as? BaseViewModel)?.bindToUiController(
                            viewLifecycleOwner,
                            this@bindViewModel
                        )
                    }
                }
            }
        }
    }
)

/**
 * Creates a [BaseViewModel] bound to this dialog fragment's lifecycle and UI controller
 * using the instance returned from the [viewModelProducer].
 *
 * If the [viewModelProducer] obtains its instance from a Dagger component, this
 * will allow you to perform constructor injection.
 */
@Suppress("UNCHECKED_CAST")
@MainThread
inline fun <reified VM : BaseViewModel> DrBottomSheetDialogFragment.bindViewModel(
    noinline ownerProducer: () -> ViewModelStoreOwner = { this },
    noinline viewModelProducer: (() -> VM)
) = createViewModelLazy(
    viewModelClass = VM::class,
    storeProducer = { ownerProducer().viewModelStore },
    factoryProducer = {
        return@createViewModelLazy object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                if (modelClass != VM::class.java) {
                    throw IllegalArgumentException("Unexpected argument $modelClass")
                }

                return (viewModelProducer.invoke() as T).also { vm ->
                    (vm as? BaseViewModel)?.bindToUiController(
                        viewLifecycleOwner,
                        this@bindViewModel
                    )
                    this@bindViewModel.onViewCreatedListeners.add { _, _ ->
                        (vm as? BaseViewModel)?.bindToUiController(
                            viewLifecycleOwner,
                            this@bindViewModel
                        )
                    }
                }
            }
        }
    }
)

/**
 * Creates a [BaseViewModel] bound to this dialog fragment's lifecycle and UI controller
 * using the instance returned from the [viewModelProducer].
 *
 * If the [viewModelProducer] obtains its instance from a Dagger component, this
 * will allow you to perform constructor injection.
 */
@Suppress("UNCHECKED_CAST")
@MainThread
inline fun <reified VM : BaseViewModel> DrDialogFragment.bindViewModel(
    noinline ownerProducer: () -> ViewModelStoreOwner = { this },
    noinline viewModelProducer: (() -> VM)
) = createViewModelLazy(
    viewModelClass = VM::class,
    storeProducer = { ownerProducer().viewModelStore },
    factoryProducer = {
        return@createViewModelLazy object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                if (modelClass != VM::class.java) {
                    throw IllegalArgumentException("Unexpected argument $modelClass")
                }

                return (viewModelProducer.invoke() as T).also { vm ->
                    (vm as? BaseViewModel)?.bindToUiController(
                        viewLifecycleOwner,
                        this@bindViewModel
                    )
                    this@bindViewModel.onViewCreatedListeners.add { _, _ ->
                        (vm as? BaseViewModel)?.bindToUiController(
                            viewLifecycleOwner,
                            this@bindViewModel
                        )
                    }
                }
            }
        }
    }
)

/**
 * Launches a deep link [uri]. You may optionally restrict the deep link to
 * the current application via the [internalOnly] flag.
 */
fun Fragment.launchDeeplink(
    uri: Uri,
    internalOnly: Boolean = false,
    onNotSupported: (() -> Unit)? = null
) {
    (activity as? AppCompatActivity)?.launchDeeplink(uri, internalOnly, onNotSupported)
}

class ViewModelDelegate<VM : ViewModel>(private val vm: VM) {
    operator fun getValue(thisRef: BaseFragment, property: KProperty<*>): VM {
        return vm
    }
}
