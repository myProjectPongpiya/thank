package com.thankyou.appfoundation.flatlist

import com.thankyou.appfoundation.graphics.DrImage
import com.thankyou.appfoundation.text.CustomSpannable

sealed class DrFlatListItem {
    data class Header(
        val label: String
    ) : DrFlatListItem()

    data class Item(
        val id: String,
        val title: CustomSpannable? = null,
        val description: List<CustomSpannable>? = null,
        val icon: DrImage? = null,
        val iconEnd: DrImage? = null,
        val isClickable: Boolean = true
    ) : DrFlatListItem() {
        constructor(
            id: String,
            title: String? = null,
            description: List<CustomSpannable>? = null,
            icon: DrImage? = null,
            iconEnd: DrImage? = null,
            isClickable: Boolean = true
        ) : this(
            id, title?.let { CustomSpannable(it) }, description, icon, iconEnd, isClickable
        )
    }

    data class Load(
        val placeholder: String? = null
    ) : DrFlatListItem()
}
