package com.thankyou.appfoundation.flatlist

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.view.View
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.view.forEach
import androidx.recyclerview.widget.RecyclerView
import com.thankyou.appfoundation.R

internal class DrFlatListDivider(
    context: Context,
    private val horizontalPadding: Float,
    @DrawableRes dividerResId: Int = R.drawable.bg_common_divider,
    private val skipLastItem: Boolean = true,
) : RecyclerView.ItemDecoration() {

    private val divider = ContextCompat.getDrawable(context, dividerResId)
        ?: throw NullPointerException("Unable to load divider drawable.")

    override fun onDrawOver(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        parent.forEach { child ->
            onDrawOverItem(parent.getChildAdapterPosition(child), canvas, child, parent)
        }
    }

    @Suppress("FoldInitializerAndIfToElvis", "ComplexCondition")
    private fun onDrawOverItem(position: Int, canvas: Canvas, itemView: View, parent: RecyclerView) {
        val adapter = parent.adapter

        if (adapter == null) {
            throw IllegalStateException("No adapter found.")
        }

        if (position == RecyclerView.NO_POSITION) {
            return
        }

        // No need to draw any divider at the bottom if it's the last item
        if (position == adapter.itemCount - 1 && skipLastItem) {
            return
        }

        // Only draw for items
        if (adapter.getItemViewType(position) != DrFlatListAdapter.ITEM_TYPE
        ) {
            return
        }

        // Only draw if the next item is also a NextFlatListItem.Item
        if (position != adapter.itemCount - 1 &&
            adapter.getItemViewType(position + 1) != DrFlatListAdapter.ITEM_TYPE
        ) {
            return
        }

        val padding = horizontalPadding.toInt()

        // Draw a divider at the bottom [left, top, right, bottom]
        divider.setBounds(
            padding,
            itemView.bottom,
            itemView.width - padding,
            itemView.bottom + divider.intrinsicHeight
        )
        divider.draw(canvas)
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val position = parent.getChildAdapterPosition(view)
        if (parent.adapter != null && position == parent.adapter!!.itemCount - 1) {
            outRect.set(0, 0, 0, divider.intrinsicHeight)
        } else {
            super.getItemOffsets(outRect, view, parent, state)
        }
    }
}
