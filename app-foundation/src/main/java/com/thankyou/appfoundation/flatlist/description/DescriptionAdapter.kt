package com.thankyou.appfoundation.flatlist.description

import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.thankyou.appfoundation.R
import com.thankyou.appfoundation.text.CustomSpannable
import kotlinx.android.synthetic.main.item_description.view.*

class DescriptionAdapter : RecyclerView.Adapter<DescriptionAdapter.ViewHolder>() {

    private var items: List<CustomSpannable> = emptyList()

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_description, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    fun setItems(items: List<CustomSpannable>) {
        this.items = items
        notifyDataSetChanged()
    }

    class ViewHolder(
        view: View
    ) : RecyclerView.ViewHolder(view) {
        fun bind(item: CustomSpannable) {
            itemView.tvLabel.linksClickable = true
            itemView.tvLabel.isClickable = true
            itemView.tvLabel.text = item.toAndroidSpannable(itemView.context) {
                itemView.tvLabel.movementMethod = LinkMovementMethod.getInstance()
            }
        }
    }
}
