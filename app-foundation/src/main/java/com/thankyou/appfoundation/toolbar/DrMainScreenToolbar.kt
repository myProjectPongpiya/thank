package com.thankyou.appfoundation.toolbar

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import com.thankyou.appfoundation.R
import com.thankyou.appfoundation.extensions.setImage
import com.thankyou.appfoundation.graphics.DrImage
import kotlinx.android.synthetic.main.layout_dr_main_screen_toolbar.view.*
import kotlinx.android.synthetic.main.layout_dr_toolbar.view.divStatusBar

class DrMainScreenToolbar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyle, defStyleRes) {

    init {
        View.inflate(context, R.layout.layout_dr_main_screen_toolbar, this)
        setStatusBarHeight()
    }

    private fun setStatusBarHeight() {
        // We need these flags otherwise the WindowInsetListener will not be called
        systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

        this.setOnApplyWindowInsetsListener { _, insets ->
            val statusBarHeight = insets.systemWindowInsetTop
            this.divStatusBar.layoutParams = LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                statusBarHeight
            )
            return@setOnApplyWindowInsetsListener insets.consumeSystemWindowInsets()
        }
    }

    fun setNotificationImage(image: DrImage) {
        ivNotificationIcon.setImage(image)
    }

    fun setOnClickNotification(listener: OnClickListener) {
        ivNotificationIcon.setOnClickListener(listener)
    }

    fun setOnClickSetting(listener: OnClickListener) {
        ivSettingIcon.setOnClickListener(listener)
    }
}