package com.thankyou.appfoundation.toolbar

enum class DrToolbarNavigationType {
    BACK,
    CLOSE,
    BOTH
}
