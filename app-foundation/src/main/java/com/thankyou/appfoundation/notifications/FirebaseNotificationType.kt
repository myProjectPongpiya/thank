package com.thankyou.appfoundation.notifications

enum class FirebaseNotificationType {
    MD_CARD_EXPIRE,
    DOCTOR_STATUS,
    CREDENTIAL,
    LOGIN_HIE
}
