package com.thankyou.appfoundation.text

object CommonRegex {
    const val EMAIL =
        "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})\$"

    const val THAI_CHARACTERS = "^(?=.*[\\u0E00-\\u0E3A\\u0E40-\\u0E4E\\u0E5A]).+$"
    const val SPECIAL_CHARACTERS = "^(?=.*[!#$%&';<=>?^'{|}~\\[\\]]).+$"
    const val NO_SPACE = "^(?=.*[\\s]).+$"
    const val NO_BLANK_SPACE = ".*\\s+.*"
    const val CID = "^(.{1})-(.{4})-(.{5})-(.{2})-(.{1})$"
    const val PHONE_BKK_NUMBER = "^[0](.{1})-(.{3})-(.{4})$"
    const val PHONE_NUMBER = "^[0](.{2})-(.{3})-(.{3})$"
    const val MOBILE_NUMBER = "^[0][689](.{1})-(.{3})-(.{4})$"
    const val MOBILE_NUMBER_WITHOUT_SPECIAL_CHARACTERS = "^[\\d\\s,]*"
    const val EMOJI = "[\\s\n\r]*(?:(?:[\u00a9\u00ae\u203c\u2049\u2122\u2139\u2194-" +
        "\u2199\u21a9-\u21aa\u231a-\u231b\u2328\u23cf\u23e9-\u23f3\u23f8-" +
        "\u23fa\u24c2\u25aa-\u25ab\u25b6\u25c0\u25fb-\u25fe\u2600-" +
        "\u2604\u260e\u2611\u2614-\u2615\u2618\u261d\u2620\u2622-" +
        "\u2623\u2626\u262a\u262e-\u262f\u2638-\u263a\u2648-\u2653\u2660\u2663\u2665-" +
        "\u2666\u2668\u267b\u267f\u2692-\u2694\u2696-\u2697\u2699\u269b-\u269c\u26a0-" +
        "\u26a1\u26aa-\u26ab\u26b0-\u26b1\u26bd-\u26be\u26c4-\u26c5\u26c8\u26ce-" +
        "\u26cf\u26d1\u26d3-\u26d4\u26e9-\u26ea\u26f0-\u26f5\u26f7-\u26fa\u26fd\u2702\u2705\u2708-" +
        "\u270d\u270f\u2712\u2714\u2716\u271d\u2721\u2728\u2733-\u2734\u2744\u2747\u274c\u274e\u2753-" +
        "\u2755\u2757\u2763-\u2764\u2795-\u2797\u27a1\u27b0\u27bf\u2934-\u2935\u2b05-" +
        "\u2b07\u2b1b-\u2b1c\u2b50\u2b55\u3030\u303d\u3297\u3299\ud83c\udc04\ud83c\udccf\ud83c\udd70-" +
        "\ud83c\udd71\ud83c\udd7e-\ud83c\udd7f\ud83c\udd8e\ud83c\udd91-\ud83c\udd9a\ud83c\ude01-" +
        "\ud83c\ude02\ud83c\ude1a\ud83c\ude2f\ud83c\ude32-\ud83c\ude3a\ud83c\ude50-" +
        "\ud83c\ude51\u200d\ud83c\udf00-\ud83d\uddff\ud83d\ude00-\ud83d\ude4f\ud83d\ude80-" +
        "\ud83d\udeff\ud83e\udd00-\ud83e\uddff\udb40\udc20-\udb40\udc7f]|\u200d[\u2640\u2642]|" +
        "[\ud83c\udde6-\ud83c\uddff]{2}|.[\u20e0\u20e3\ufe0f]+)+[\\s\n\r]*)+"
}

object PinRegex {
    const val REPEATING_DIGITS = "\\b(\\d+)\\1+\\b"
    const val MIRRORED_DIGITS = "(\\d)(\\d)(\\d)\\3\\2\\1"
    const val SEQUENTIAL_DIGIT =
        "((?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)|9(?=0)){5}\\d$|(?:0(?=9)|1(?=0)|2(?=1)|3(?=2)|4(?=3)|5(?=4)|6(?=5)|7(?=6)|8(?=7)|9(?=8)){5}\\d$)"
    const val REPEATED_SEQUENTIAL_DIGITS =
        "((?:00(?=11)|11(?=22)|22(?=33)|33(?=44)|44(?=55)|55(?=66)|66(?=77)|77(?=88)|88(?=99)|99(?=00)){2}\\d\\d|(?:00(?=99)|11(?=00)|22(?=11)|33(?=22)|44(?=33)|55(?=44)|66(?=55)|77(?=66)|88(?=77)|99(?=88)){2}\\d\\d)"
}
