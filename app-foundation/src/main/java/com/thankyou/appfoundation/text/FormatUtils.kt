package com.thankyou.appfoundation.text

object FormatUtils {

    fun formatInputByType(input: String, autoFormatType: AutoFormatType): String {
        val builder = StringBuilder()

        val inputText = input.replace(autoFormatType.formatChar, "")

        inputText.forEachIndexed { index, char ->
            builder.append(autoFormatType.getAppendedText(char.toString(), index))
        }
        return builder.toString()
    }

    const val FORMAT_BLANK = " "
    const val FORMAT_DASH = "-"
}
