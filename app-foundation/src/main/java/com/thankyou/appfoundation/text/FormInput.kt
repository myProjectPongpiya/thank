package com.thankyou.appfoundation.text

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

/**
 * [FormInput] is an utility class to encapsulate common behaviours regarding
 * form inputs, especially in regards to the handling of validation.
 */
abstract class FormInput<T> : CoroutineScope {

    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private val disposeBag: MutableList<() -> Unit> = mutableListOf()

    /**
     * The form value.
     */
    val input = MutableLiveData<T>()

    /**
     * Specifies whether this form is "touched". A touched form indicates that the
     * user has attempted to edit this field at least once.
     *
     * Typically this is used to prevent validation errors from appearing before
     * the user has made any changes.
     */
    private var isTouched = false

    /**
     * The current error message updated in realtime as [input] changes. However,
     * it is not necessarily what is shown to the user.
     *
     * @see error
     */
    private val realtimeError by lazy { setupRealtimeError() }

    /**
     * The error that is displayed to the user.
     */
    val error = MutableLiveData<String>()

    /**
     * Returns `true` if the current [value] has no errors.
     */
    val isValid: Boolean
        get() = realtimeError.value.isNullOrBlank()

    /**
     * Emits `true` if the current [value] has no errors.
     */
    val isValidObs: LiveData<Boolean> by lazy {
        Transformations.map(realtimeError) { it.isNullOrBlank() }
    }

    /**
     * Returns `true` if the field is currently displaying an error message.
     */
    val isShowingError: Boolean
        get() = error.value?.isNotBlank() == true

    /**
     * Emits `true` if the field is currently displaying an error message.
     */
    val isShowingErrorObs: LiveData<Boolean> by lazy {
        Transformations.map(error) { it?.isNotBlank() == true }
    }

    /**
     * Returns `true` if error should be clear on text change
     */
    var clearErrorOnChange: Boolean = true

    private fun setupRealtimeError(): MediatorLiveData<String> {
        val liveData = MediatorLiveData<String>()

        liveData.addSource(input) { liveData.value = onValidate(input.value) }

        // By default, MediatorLiveData does not update itself if there are no
        // observers. Hence we need to add an observer so that it executes itself.
        val observer = Observer<String?> { }
        liveData.observeForever(observer)
        disposeBag.add { liveData.removeObserver(observer) }

        // Run initial validation
        liveData.value = onValidate(input.value)

        return liveData
    }

    /**
     * Sets whether this [FormInput] is touched.
     */
    fun setTouched(isTouched: Boolean) {
        this.isTouched = isTouched
    }

    fun setValue(value: T?) {
        this.isTouched = true
        this.input.value = value
        this.updateError()
    }

    /**
     * Cleans up listeners.
     */
    fun destroy() {
        disposeBag.forEach { it.invoke() }
    }

    /**
     * Clears [error] from any error messages.
     */
    fun clearError() {
        error.value = null
    }

    /**
     * Triggers the error message to update itself with the latest message.
     */
    fun updateError() {
        if (!isTouched) return
        realtimeError.value = onValidate(input.value)
        error.value = realtimeError.value
    }

    /**
     * Add additional [LiveData] sources to trigger the [validationFn] when its
     * value changes.
     */
    fun addValidationTriggers(vararg sources: LiveData<*>) {
        val observer = Observer<Any> {
            realtimeError.value = onValidate(input.value)
            updateError()
        }
        sources.forEach { realtimeError.addSource(it, observer) }
    }

    abstract fun onValidate(value: T?): String?
}
