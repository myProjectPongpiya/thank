package com.thankyou.appfoundation.text

import androidx.lifecycle.LiveData
import com.thankyou.appfoundation.ui.UIController

/**
 * [TextFormInput] is a [FormInput] for `String` based inputs.
 *
 * This implementation is needed (as opposed to using the generic [FormInput]
 * directly) because Android's Databinding does not support generic classes.
 */
class TextFormInput internal constructor(
    private val validationFn: (String?) -> String? = { null }
) : FormInput<String>() {

    override fun onValidate(value: String?): String? {
        return validationFn.invoke(value)
    }

    class Builder(
        private var uiController: UIController
    ) {

        private var initialValue: String? = null
        private var validationFn: (String?) -> String? = { null }
        private var additionalValidationTriggers: MutableList<LiveData<*>> = mutableListOf()

        fun setInitialValue(initialValue: String?) = apply {
            this.initialValue = initialValue
        }

        fun setValidator(validationFn: (String?) -> String?) = apply {
            this.validationFn = validationFn
        }

        fun addAdditionalValidationTriggers(vararg sources: LiveData<*>) = apply {
            this.additionalValidationTriggers.addAll(sources)
        }

        fun build(): TextFormInput {
            val formInput = TextFormInput(validationFn)

            initialValue?.let {
                formInput.input.value = it
                formInput.setTouched(true)
            }

            if (additionalValidationTriggers.isNotEmpty()) {
                formInput.addValidationTriggers(*additionalValidationTriggers.toTypedArray())
            }

            uiController.disposeBag.add { formInput.destroy() }

            return formInput
        }
    }
}
