package com.thankyou.appfoundation.text

enum class DateFormat(val format: String) {
  // Date formats specific to KTB BizNext
  DR_DATE("dd MMM yyyy"),
  DR_DATE_BIRTH("yyyyMMdd"),
  DR_DATE_DASH_REVERT("yyyy-MM-dd"),
  DR_YEAR_MONTH_DAY("yyyy-MM-dd"),
  DR_FULL_DATE_TIME_SECONDS_SLASH("yyyy-MM-dd HH:mm:ss"),
  DR_DATETIME_HOUR_IN_AM_PM("dd MMM yy, HH:mm"),
}
