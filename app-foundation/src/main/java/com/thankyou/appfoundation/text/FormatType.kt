package com.thankyou.appfoundation.text

import java.util.regex.Pattern

sealed class FormatType {

    internal abstract fun format(input: String, isMask: Boolean = false): String

    object MobileNo : FormatType() {
        override fun format(input: String, isMask: Boolean): String {
            val pattern = Pattern.compile("(.)(.{2})(.{3})(.{4,})")
            val m = pattern.matcher(input)
            return if (m.matches()) {
                val group1 = m.group(NUM_1).orEmpty()
                val group2 = m.group(NUM_2).orEmpty()
                val group3 = m.group(NUM_3).orEmpty()
                val group4 = m.group(NUM_4).orEmpty()
                if (input.contains("*")) {
                    return "$group1$group2 $group3 $group4"
                }
                val formatGroup2 =
                    group2.replace(Regex(group2), MASK_CHARACTER.repeat(group2.length))
                        .takeIf { isMask }
                val formatGroup3 =
                    group3.replace(Regex(group3), MASK_CHARACTER.repeat(group3.length))
                        .takeIf { isMask }
                "$group1${formatGroup2 ?: group2} ${formatGroup3 ?: group3} $group4"
            } else {
                input
            }
        }
    }

    object MobileNoWithDashes : FormatType() {
        override fun format(input: String, isMask: Boolean): String {
            val pattern = Pattern.compile("(.)(.{2})(.{3})(.{4,})")
            val m = pattern.matcher(input)
            return if (m.matches()) {
                val group1 = m.group(NUM_1).orEmpty()
                val group2 = m.group(NUM_2).orEmpty()
                val group3 = m.group(NUM_3).orEmpty()
                val group4 = m.group(NUM_4).orEmpty()
                if (input.contains("*")) {
                    return "$group1$group2-$group3-$group4"
                }
                val formatGroup2 =
                    group2.replace(Regex(group2), MASK_CHARACTER.repeat(group2.length))
                        .takeIf { isMask }
                val formatGroup3 =
                    group3.replace(Regex(group3), MASK_CHARACTER.repeat(group3.length))
                        .takeIf { isMask }
                "$group1${formatGroup2 ?: group2}-${formatGroup3 ?: group3}-$group4"
            } else {
                input
            }
        }
    }

    object PhoneNoWithDashes : FormatType() {
        override fun format(input: String, isMask: Boolean): String {
            val pattern = when (input.length) {
                MOBILE_LENGTH -> Pattern.compile("(.{3})(.{3})(.{4})")
                else -> Pattern.compile("(.{2})(.{3})(.{4})")
            }
            val matcher = pattern.matcher(input.replace("-", ""))
            return if (matcher.matches()) {
                "${matcher.group(1)}-${matcher.group(2)}-${matcher.group(3)}"
            } else {
                input
            }
        }
    }

    object Email : FormatType() {
        override fun format(input: String, isMask: Boolean): String {
            val pattern = Pattern.compile("(.*)([@])(.*)([.])(.*)")
            val m = pattern.matcher(input)
            return if (m.matches()) {
                val group1 = m.group(NUM_1).orEmpty()
                val group2 = m.group(NUM_2).orEmpty()
                val group3 = m.group(NUM_3).orEmpty()
                val group4 = m.group(NUM_4).orEmpty()
                val group5 = m.group(NUM_5).orEmpty()
                val sub = if (group1.length > NUM_4) {
                    group1.substring(group1.length - NUM_3, group1.length)
                } else {
                    group1
                }
                val sub3 = if (group3.length > NUM_4) {
                    group3.substring(NUM_0, NUM_4)
                } else {
                    group3
                }
                val formatGroup1 = group1.replace(Regex(sub), MASK_CHARACTER.repeat(sub.length))
                    .takeIf { isMask }
                val formatGroup3 =
                    group3.replace(Regex(sub3), MASK_CHARACTER.repeat(sub3.length))
                        .takeIf { isMask }
                "${formatGroup1 ?: group1}$group2${formatGroup3 ?: group3}$group4$group5"
            } else {
                input
            }
        }
    }

    object Citizen : FormatType() {
        override fun format(input: String, isMask: Boolean): String {
            val pattern = Pattern.compile("(.)(.{3})(.)(.{5})(.{2})(.)")
            val matcher = pattern.matcher(input)
            return if (matcher.matches()) {
                val group1 = matcher.group(NUM_1).orEmpty()
                val group2 = matcher.group(NUM_2).orEmpty()
                val group3 = matcher.group(NUM_3).orEmpty()
                val group4 = matcher.group(NUM_4).orEmpty()
                val group5 = matcher.group(NUM_5).orEmpty()
                val group6 = matcher.group(NUM_6).orEmpty()
                if (input.contains("*")) {
                    return "$group1-$group2$group3-$group4-$group5-$group6"
                }
                val formatGroup3 =
                    group3.replace(Regex(group3), MASK_CHARACTER.repeat(group3.length))
                        .takeIf { isMask }
                val formatGroup4 =
                    group4.replace(Regex(group4), MASK_CHARACTER.repeat(group4.length))
                        .takeIf { isMask }
                val formatGroup5 =
                    group5.replace(Regex(group5), MASK_CHARACTER.repeat(group5.length))
                        .takeIf { isMask }
                "$group1-$group2${formatGroup3 ?: group3}-${formatGroup4 ?: group4}-${formatGroup5 ?: group5}-$group6"
            } else {
                input
            }
        }
    }

    class Custom(private val formatter: (input: String) -> String) : FormatType() {
        override fun format(input: String, isMask: Boolean): String {
            return formatter.invoke(input)
        }
    }

    companion object {
        private const val MASK_CHARACTER = "*"
        private const val MOBILE_LENGTH = 10
        private const val NUM_0 = 0
        private const val NUM_1 = 1
        private const val NUM_2 = 2
        private const val NUM_3 = 3
        private const val NUM_4 = 4
        private const val NUM_5 = 5
        private const val NUM_6 = 6
    }
}
