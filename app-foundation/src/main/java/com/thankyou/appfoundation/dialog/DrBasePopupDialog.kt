package com.thankyou.appfoundation.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.ContextThemeWrapper
import com.thankyou.appfoundation.R
import kotlinx.android.synthetic.main.layout_blank_custom_popup.view.*

abstract class DrBasePopupDialog : DrBottomSheetDialogFragment(disablePeek = true) {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val themedInflater = inflater.cloneInContext(ContextThemeWrapper(context, R.style.DrAppTheme))
        return themedInflater.inflate(R.layout.layout_blank_custom_popup, container, false).apply {
            divContainer.addView(onCreateInnerView(themedInflater, divContainer, savedInstanceState))
        }
    }

    abstract fun onCreateInnerView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View
}
