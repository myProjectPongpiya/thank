package com.thankyou.appfoundation.dialog

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.thankyou.appfoundation.permissions.PermissionResult
import com.thankyou.appfoundation.text.CustomSpannable
import com.thankyou.appfoundation.ui.Alert
import com.thankyou.appfoundation.ui.UIController
import kotlin.coroutines.Continuation
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.resume

abstract class DrBottomSheetDialogFragment(

    /**
     * Disables the `peeking` state of the bottom sheet dialog.
     */
    private val disablePeek: Boolean = false,

    /**
     * Forces the bottom sheet dialog to renders its children in a full height
     * container.
     */
    private val forceFullHeight: Boolean = false,

    /**
     * Disables the dragging behaviour of the dialog.
     */
    private val disableDrag: Boolean = false,

    /**
     * Bottom Sheet Height in percentage
     * Default to be 100% height
     */
    private val bottomSheetHeightPercentage: Int = MAX_DIALOG_HEIGHT,

    ) : BottomSheetDialogFragment(), UIController {

    private val bottomSheetBehavior: BottomSheetBehavior<View>?
        get() {
            val parent = dialog?.let { getContainerLayout(it) }
            return parent?.let { BottomSheetBehavior.from(it) }
        }

    private val bottomSheetCallback = NextBottomSheetCallback()

    private val percentageRange = IntRange(PERCENTAGE_0, PERCENTAGE_100)

    val onViewCreatedListeners = mutableListOf<(View, Bundle?) -> Unit>()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        dialog.setOnShowListener {
            bottomSheetBehavior?.setBottomSheetCallback(bottomSheetCallback)

            if (disablePeek) {
                bottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
                bottomSheetBehavior?.skipCollapsed = true
                bottomSheetBehavior?.isHideable = true
            }

            if (disableDrag) {
                bottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
            }
        }

        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewHeight(view)

        onViewCreatedListeners.forEach { it.invoke(view, savedInstanceState) }
    }

    fun setupViewHeight(view: View) {
        val observer = object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                val parent = getContainerLayout(dialog)
                val layoutParams = parent?.layoutParams ?: return

                // By default, the parent's height is setDeviceId to WRAP_CONTENT, which is why
                // child layouts do not expand to full height
                layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
                layoutParams.height = getHeight(parent)
                parent.layoutParams = layoutParams
                // We have to remove this listener after the first call, otherwise this
                // will be invoked repeatedly causing the app to stutter
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        }
        view.viewTreeObserver.addOnGlobalLayoutListener(observer)
    }

    /**
     * Return new height of bottom sheet, depends on conditions.
     * New height is calculated from percentage of fragment height.
     *
     * @param containerLayout the bottom sheet container
     * @return LayoutParams.MATCH_PARENT, if forceFullHeight is TRUE
     *         LayoutParams.WRAP_CONTENT, if content height less than new height
     *         newHeight, if content height more than new height
     */
    private fun getHeight(containerLayout: FrameLayout): Int {
        val newHeight = (containerLayout.parent as View).height
            .times(bottomSheetHeightPercentage)
            .div(PERCENTAGE_100)

        return when {
            forceFullHeight -> ViewGroup.LayoutParams.MATCH_PARENT
            !isHeightPercentageValid(bottomSheetHeightPercentage) ||
                    containerLayout.height < newHeight -> ViewGroup.LayoutParams.WRAP_CONTENT
            else -> newHeight
        }
    }

    private fun isHeightPercentageValid(heightPercentage: Int): Boolean =
        percentageRange.contains(heightPercentage)

    /**
     * Returns the dialog's containing parent.
     */
    private fun getContainerLayout(dialog: Dialog?): FrameLayout? {
        val id = com.google.android.material.R.id.design_bottom_sheet
        return dialog?.findViewById(id)
    }

    private inner class NextBottomSheetCallback : BottomSheetBehavior.BottomSheetCallback() {
        override fun onStateChanged(v: View, state: Int) {
            val behavior = BottomSheetBehavior.from(v)

            if (state == BottomSheetBehavior.STATE_HIDDEN) {
                onStateHidden()
                dismissAllowingStateLoss()
            }

            if (disableDrag && state == BottomSheetBehavior.STATE_DRAGGING) {
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }

            if (disablePeek) {
                // We need to force the layout to re-render, otherwise layouts with
                // dynamic height might be positioned incorrectly.
                // eg. while keyboard is open, launch a calendar picker dialog.
                //     without this, the dialog will be stuck at the top.
                //
                // This is a bug in the BottomSheetBehavior itself:
                // @see https://issuetracker.google.com/issues/37090839
                v.post {
                    v.requestLayout()
                    v.invalidate()
                }
            }
        }

        override fun onSlide(v: View, offset: Float) {
            // do nothing
        }
    }

    override val disposeBag: MutableList<() -> Unit>
        get() = mutableListOf()

    override val coroutineContext: CoroutineContext
        get() = lifecycleScope.coroutineContext

    override fun setLoading(isLoading: Boolean, vararg labels: String) {
        (this.activity as? UIController)?.setLoading(isLoading, *labels)
    }

    override fun showToast(message: String, toastType: Alert.ToastType) {
        (this.activity as? UIController)?.showToast(message, toastType)
    }

    override fun showPopup(
        title: String?,
        message: String?,
        subMessage: CustomSpannable?,
        buttonLabel: String?,
        heroImage: Int?,
        onDismiss: (() -> Unit)?
    ) {
        (this.activity as? UIController)?.showPopup(
            title,
            message,
            subMessage,
            buttonLabel,
            heroImage,
            onDismiss
        )
    }

    override fun showPopupWithMultiButton(
        title: String?,
        message: String?,
        heroImage: Int?,
        onDismiss: (() -> Unit)?,
        negativeButtonLabel: String?,
        negativeButtonListener: (() -> Unit)?,
        positiveButtonLabel: String?,
        positiveButtonListener: (() -> Unit)?,
        multiButtonType: Alert.MultiButtonType
    ) {
        (this.activity as? UIController)?.showPopupWithMultiButton(
            title,
            message,
            heroImage,
            onDismiss,
            negativeButtonLabel,
            negativeButtonListener,
            positiveButtonLabel,
            positiveButtonListener,
            multiButtonType
        )
    }

    override fun showNativePopup(
        title: String?,
        message: String?,
        negativeButtonLabel: String?,
        negativeButtonListener: (() -> Unit)?,
        positiveButtonLabel: String?,
        positiveButtonListener: (() -> Unit)?
    ) {
        (this.activity as? UIController)?.showNativePopup(
            title,
            message,
            positiveButtonLabel,
            positiveButtonListener,
            negativeButtonLabel,
            negativeButtonListener
        )
    }

    override fun launchDeepLink(uri: String, internalOnly: Boolean, onNotSupported: (() -> Unit)?) {
        (this.activity as? UIController)?.launchDeepLink(uri, internalOnly, onNotSupported)
    }

    override fun showNoConnectionError(onConnected: () -> Unit) {
        (this.activity as? UIController)?.showNoConnectionError(onConnected)
    }

    override fun restartSession() {
        (this.activity as? UIController)?.restartSession()
    }

    override fun requestPermissions(
        vararg permissions: String,
        onDenied: (() -> Unit)?,
        onGranted: () -> Unit,
    ) {
        (this.activity as? UIController)?.requestPermissions(
            *permissions,
            onDenied = onDenied,
            onGranted = onGranted
        )
    }

    override fun requestPermissionsWithRationale(
        vararg permissions: String,
        onResult: (PermissionResult) -> Unit,
    ) {
        (this.activity as? UIController)?.requestPermissionsWithRationale(
            *permissions,
            onResult = onResult
        )
    }

    override fun gotoPage(navDirections: NavDirections) {
        (this.activity as? UIController)?.gotoPage(navDirections)
    }

    override fun goBack(destinationId: Int?) {
        (this.activity as? UIController)?.goBack(destinationId)
    }

    override fun onCallPhoneNumber(phoneNo: String) {
        (this.activity as? UIController)?.onCallPhoneNumber(phoneNo)
    }

    override fun onCallEmail(email: String) {
        (this.activity as? UIController)?.onCallEmail(email)
    }

    override fun onCallWebBrowser(link: String) {
        (this.activity as? UIController)?.onCallWebBrowser(link)
    }

    override fun onDeeplinkPreRoute(continuation: Continuation<Unit>) {
        continuation.resume(Unit)
    }

    protected open fun onStateHidden(): Boolean = true

    companion object {
        const val PERCENTAGE_0 = 0
        const val PERCENTAGE_100 = 100
        const val MAX_DIALOG_HEIGHT = 100
    }
}
