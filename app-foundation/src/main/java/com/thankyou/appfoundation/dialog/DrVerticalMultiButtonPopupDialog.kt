package com.thankyou.appfoundation.dialog

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import com.thankyou.appfoundation.R
import com.thankyou.appfoundation.extensions.gone
import com.thankyou.appfoundation.extensions.inflateWithTheme
import com.thankyou.appfoundation.extensions.visible
import kotlinx.android.synthetic.main.layout_custom_popup.ivDialog
import kotlinx.android.synthetic.main.layout_custom_popup.tvMessage
import kotlinx.android.synthetic.main.layout_custom_popup.tvRefId
import kotlinx.android.synthetic.main.layout_vertical_multi_select_popup.*

/**
 * A reusable alert dialog that appears from the bottom of the screen. The
 * dialog's title, message, hero image can be configured via the arguments
 * bundle.
 *
 * You may optionally setDeviceId a callback to be invoked when the dialog is dismissed
 * using [setOnDismissListener].
 */
class DrVerticalMultiButtonPopupDialog : DrDialogFragment(disableClickOutside = true) {

    private var onDismissListener: (() -> Unit)? = null
    private var onOkClickListener: (() -> Unit)? = null
    private var onCancelClickListener: (() -> Unit)? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflateWithTheme(
            context,
            R.layout.layout_vertical_multi_select_popup,
            container
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    private fun setupView() {
        if (arguments?.getString(ARGS_TITLE) == ARGS_TITLE_NULL) {
            tvRefId.gone()
        } else {
            tvRefId.visible()
            arguments?.getString(ARGS_TITLE)?.let { title -> tvRefId.text = title }
        }
        if (arguments?.getString(ARGS_MESSAGE) == ARGS_MESSAGE_NULL) {
            tvMessage.gone()
        } else {
            tvMessage.visible()
            arguments?.getString(ARGS_MESSAGE)?.let { message -> tvMessage.text = message }
        }
        if (arguments?.getInt(ARGS_HERO_IMAGE) == ARGS_HERO_IMAGE_NULL) {
            ivDialog.gone()
        } else {
            ivDialog.visible()
            arguments?.getInt(ARGS_HERO_IMAGE)?.let { ivDialog.setImageResource(it) }
        }
        btnOk.setOnClickListener {
            dismiss()
            onOkClickListener?.invoke()
        }
        btnCancel.setOnClickListener {
            dismiss()
            onCancelClickListener?.invoke()
        }
        arguments?.getString(ARGS_PRIMARY_BUTTON_TEXT)?.let { primaryText ->
            btnOk.text = primaryText
        }
        arguments?.getString(ARGS_SECONDARY_BUTTON_TEXT)?.let { secondaryText ->
            btnCancel.text = secondaryText
        }
    }

    /**
     * Set a [listener] to be invoked when the dialog is dismissed.
     */
    fun setOnDismissListener(listener: () -> Unit) {
        onDismissListener = listener
    }

    private fun onPrimaryButtonClick(listener: () -> Unit) {
        onOkClickListener = listener
    }

    private fun onSecondaryButtonClick(listener: () -> Unit) {
        onCancelClickListener = listener
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onDismissListener?.invoke()
    }

    companion object {

        /**
         * Returns a new instance of [DrVerticalMultiButtonPopupDialog].
         */
        fun newInstance(
            title: String? = null,
            message: String? = null,
            @DrawableRes heroImage: Int? = null,
            onDismiss: (() -> Unit)? = null,
            onOkClick: (() -> Unit)? = null,
            onCancelClick: (() -> Unit)? = null,
            primaryButtonText: String? = null,
            secondaryButtonText: String? = null
        ): DrVerticalMultiButtonPopupDialog {
            val bundle = Bundle().apply {
                if (title == null) {
                    putString(ARGS_TITLE, ARGS_TITLE_NULL)
                } else {
                    putString(ARGS_TITLE, title)
                }
                if (message == null) {
                    putString(ARGS_MESSAGE, ARGS_MESSAGE_NULL)
                } else {
                    putString(ARGS_MESSAGE, message)
                }
                if (heroImage == null) {
                    putInt(ARGS_HERO_IMAGE, ARGS_HERO_IMAGE_NULL)
                } else {
                    putInt(ARGS_HERO_IMAGE, heroImage)
                }
                putString(ARGS_PRIMARY_BUTTON_TEXT, primaryButtonText)
                putString(ARGS_SECONDARY_BUTTON_TEXT, secondaryButtonText)
            }

            val dialog = DrVerticalMultiButtonPopupDialog().apply {
                arguments = bundle
            }

            if (onDismiss !== null) {
                dialog.setOnDismissListener(onDismiss)
            }
            if (onOkClick !== null) {
                dialog.onPrimaryButtonClick(onOkClick)
            }
            if (onCancelClick !== null) {
                dialog.onSecondaryButtonClick(onCancelClick)
            }
            return dialog
        }

        private const val ARGS_TITLE = "ARGS_TITLE"
        private const val ARGS_TITLE_NULL = ""
        private const val ARGS_MESSAGE = "ARGS_MESSAGE"
        private const val ARGS_MESSAGE_NULL = ""
        private const val ARGS_HERO_IMAGE = "ARGS_DIALOG_IMAGE"
        private const val ARGS_HERO_IMAGE_NULL = 0
        private const val ARGS_PRIMARY_BUTTON_TEXT = "ARGS_PRIMARY_BUTTON_TEXT"
        private const val ARGS_SECONDARY_BUTTON_TEXT = "ARGS_SECONDARY_BUTTON_TEXT"
    }
}
