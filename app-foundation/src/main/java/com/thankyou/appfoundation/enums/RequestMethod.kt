package com.thankyou.appfoundation.enums

enum class RequestMethod {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE
}