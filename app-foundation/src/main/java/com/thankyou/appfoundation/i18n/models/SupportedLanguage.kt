package com.thankyou.appfoundation.i18n.models

enum class SupportedLanguage {

    ENGLISH {
        override var iso639_1 = "en"
        override var iso639_2 = "eng"
        override var locale = "en-TH"
    },

    THAI {
        override var iso639_1 = "th"
        override var iso639_2 = "tha"
        override var locale = "th-TH"
    };

    abstract var iso639_1: String
    abstract var iso639_2: String

    /**
     * Returns the default string representation of this language (ISO639-1).
     */
    override fun toString() = iso639_1
    abstract var locale: String

    companion object {
        /**
         * Converts a ISO639-1 language to [SupportedLanguage]. Returns `null` if
         * the language is not supported.
         */
        fun fromIso639_1(lang: String): SupportedLanguage? {
            return values().find { it.iso639_1 == lang }
        }

        /**
         * Converts a ISO639-2 language to [SupportedLanguage]. Returns `null` if
         * the language is not supported.
         */
        fun fromIso639_2(lang: String): SupportedLanguage? {
            return values()?.find { it.iso639_2 == lang }
        }
    }
}
