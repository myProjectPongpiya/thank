package com.thankyou.appfoundation.crypto

interface AESService {

    fun getAES(): String

    fun setAES(token: String)

    fun clear()
}
