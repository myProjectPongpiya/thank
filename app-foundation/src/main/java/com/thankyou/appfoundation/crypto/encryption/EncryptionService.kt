package com.thankyou.appfoundation.crypto.encryption

import okhttp3.Request
import okhttp3.Response
import javax.crypto.spec.SecretKeySpec

interface EncryptionService {

    fun encryptAES(plainData: String, encryptionKey: String): String

    fun decryptAES(encryptData: String, encryptionKey: String): String

    fun encryptRSA(plainData: String, salt: String): String

    fun setKey(keyColumn: String): SecretKeySpec

    fun encryptRequest(request: Request): Pair<Request, String>

    fun decryptResponse(response: Response, encryptionKey: String): Response
}
