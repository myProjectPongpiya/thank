package com.thankyou.appfoundation.crypto.manager

import java.security.PublicKey

interface KeyService {

    fun getRandomSeed(): CharArray

    fun getRandomKey(length: Int): String

    fun getPEMPublicKey(): PublicKey

    fun getIteration(): Int
}
