package com.thankyou.appfoundation.crypto

class AESManager : AESService {

    private var aes: String? = null

    override fun getAES(): String {
        return aes ?: ""
    }

    override fun setAES(token: String) {
        clear()
        this.aes = token
    }

    override fun clear() {
        aes = null
    }
}
