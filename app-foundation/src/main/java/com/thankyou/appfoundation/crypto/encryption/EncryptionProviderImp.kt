package com.thankyou.appfoundation.crypto.encryption

import android.annotation.SuppressLint
import android.os.Build
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import com.thankyou.appfoundation.crypto.AESService
import com.thankyou.appfoundation.crypto.manager.KeyService
import com.thankyou.appfoundation.enums.RequestMethod
import okhttp3.MediaType
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.Response
import okhttp3.ResponseBody
import okio.Buffer
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.util.encoders.Base64
import org.json.JSONObject
import timber.log.Timber
import java.io.BufferedReader
import java.io.InputStreamReader
import java.security.PublicKey
import java.security.Security
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec
import kotlin.collections.HashMap

class EncryptionProviderImp(
    private val keyManager: KeyService,
    private val aESService: AESService
) : EncryptionService {

    init {
        aESService.setAES(keyManager.getRandomKey(RANDOM_KEY_LENGTH))
    }

    override fun encryptAES(plainData: String, encryptionKey: String): String {
        val cipher = getAESCipher(Cipher.ENCRYPT_MODE, encryptionKey)
        val encryptedBytes = cipher.doFinal(plainData.toByteArray())
        val encryptedString = String(Base64.encode(encryptedBytes))
        return encryptedString.replace("(\\r|\\n|\\r\\n)+", "")
    }

    override fun decryptAES(encryptData: String, encryptionKey: String): String {
        return try {
            val decodeData = Base64.decode(encryptData)
            val cipher = getAESCipher(Cipher.DECRYPT_MODE, encryptionKey)
            val plainDataBytes = cipher.doFinal(decodeData)
            String(plainDataBytes)
        } catch (e: Exception) {
            encryptData
        }
    }

    override fun encryptRSA(plainData: String, salt: String): String {
        val cipher = getRSACipher(Cipher.ENCRYPT_MODE, keyManager.getPEMPublicKey())
        var encodedData: String = plainData
        repeat(keyManager.getIteration()) {
            encodedData = salt + encodedData
            val encodedDataByte = cipher.doFinal(encodedData.toByteArray())
            encodedData = String(Base64.encode(encodedDataByte))
        }

        return encodedData.replace("(\\r|\\n|\\r\\n)+", "")
    }

    private fun getAESCipher(operationMode: Int, encryptionKey: String): Cipher {
        Security.addProvider(BouncyCastleProvider())
        val secretKey = SecretKeySpec(encryptionKey.toByteArray(Charsets.UTF_8), "AES")
        val cipher = when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.P -> Cipher.getInstance("AES/ECB/PKCS5Padding")
            else -> Cipher.getInstance("AES/ECB/PKCS7Padding", "BC")
        }
        cipher.init(operationMode, secretKey)
        return cipher
    }

    private fun getRSACipher(operationMode: Int, publicKey: PublicKey): Cipher {
        val cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding")
        cipher.init(operationMode, publicKey)
        return cipher
    }

    override fun setKey(keyColumn: String): SecretKeySpec {
        return SecretKeySpec(adjustKeyLength(keyColumn).toByteArray(), "AES")
    }

    private fun adjustKeyLength(key: String): String {
        var res = key

        while (res.length < 32) {
            res += key
        }

        if (res.length > 32) {
            res = res.substring(0, 32)
        }

        return res
    }

    override fun encryptRequest(request: Request): Pair<Request, String> {
        return try {
            // Encrypt data
            val keyParameter = encryptRSA(aESService.getAES(), "")

            var newRequest = request
            val requestBuilder = newRequest.newBuilder()
            request.body()?.let {
                val requestBody = readRequestBody(it)
                val dataParameter = encryptAES(requestBody, aESService.getAES())
                log("[REQUEST]", requestBody)

                // Create request json
                val requestMap = HashMap<String, String>()
                requestMap["param1"] = dataParameter
                requestMap["param2"] = keyParameter

                val requestJSON = JSONObject(requestMap as HashMap<*, *>)

                val newRequestBody =
                    RequestBody.create(
                        MediaType.parse("application/json;charset=UTF-8"),
                        requestJSON.toString()
                    )
                newRequest = setRequestMethod(newRequest, requestBuilder, newRequestBody).build()
            }
            Pair(newRequest, aESService.getAES())
        } catch (e: Exception) {
            Pair(request, "")
        }
    }

    @SuppressLint("TimberArgCount")
    override fun decryptResponse(response: Response, encryptionKey: String): Response {
        if (encryptionKey.isEmpty()) return response
        response.body()?.let {
            return try {
                val rawData = readResponseBody(it)
                try {
                    val decryptedData = decryptAES(rawData, encryptionKey)
                    log("[RESPONSE]", decryptedData)
                    val responseBuilder = response.newBuilder()
                    val newResponseBody =
                        ResponseBody.create(
                            MediaType.parse("JSON"),
                            decryptedData
                        )
                    responseBuilder.body(newResponseBody).build()
                } catch (e: Exception) {
                    run {
                        val responseBuilder = response.newBuilder()
                        val newResponseBody =
                            ResponseBody.create(
                                MediaType.parse("JSON"),
                                rawData
                            )
                        responseBuilder.body(newResponseBody).build()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                response
            }
        } ?: return response
    }

    private fun log(type: String, data: String) {
        val json = JsonParser.parseString(data).asJsonObject
        val print = GsonBuilder().setPrettyPrinting().create().toJson(json)
        Timber.d("$type --> \n$print")
    }

    private fun readRequestBody(request: RequestBody): String {
        val buffer = Buffer()
        request.writeTo(buffer)
        return buffer.readUtf8()
    }

    private fun readResponseBody(response: ResponseBody): String {
        val source = response.source()
        val inputStreamReader = InputStreamReader(source.inputStream())
        val bufferReader = BufferedReader(inputStreamReader)
        bufferReader.use { reader ->
            val stringBuilder = StringBuilder()
            while (true) {
                val line = reader.readLine() ?: break
                stringBuilder.append(line)
            }
            return stringBuilder.toString()
        }
    }

    private fun setRequestMethod(
        newRequest: Request,
        requestBuilder: Request.Builder,
        newRequestBody: RequestBody
    ): Request.Builder {
        return when (newRequest.method()) {
            RequestMethod.POST.name -> requestBuilder.post(newRequestBody)
            RequestMethod.PUT.name -> requestBuilder.put(newRequestBody)
            RequestMethod.PATCH.name -> requestBuilder.patch(newRequestBody)
            RequestMethod.DELETE.name -> requestBuilder.delete(newRequestBody)
            else -> requestBuilder.get()
        }
    }

    companion object {
        private const val RANDOM_KEY_LENGTH = 32
    }
}
