package com.thankyou.appfoundation.ui

import android.os.SystemClock
import android.view.View

class SingleClickListener(

    /**
     * The listener to be invoked when the view is clicked.
     */
    private val listener: View.OnClickListener,

    /**
     * The interval before a listener can be invoked again.
     */
    private val throttleInterval: Long = 300,

    /**
     * If setDeviceId to `true`, then all instances of [SingleClickListener] will be
     * throttled collectively.
     */
    private val globalThrottle: Boolean = true

) : View.OnClickListener {

    private var localLastClickTimestamp: Long? = null

    override fun onClick(v: View?) {
        val lastClickTimestamp = when (globalThrottle) {
            true -> globalLastClickTimestamp
            false -> localLastClickTimestamp
        }

        if (lastClickTimestamp != null && SystemClock.elapsedRealtime() - lastClickTimestamp < throttleInterval) {
            return
        }

        if (globalThrottle) {
            globalLastClickTimestamp = SystemClock.elapsedRealtime()
        } else {
            localLastClickTimestamp = SystemClock.elapsedRealtime()
        }

        listener.onClick(v)
    }

    companion object {
        private var globalLastClickTimestamp: Long? = null
    }
}
