package com.thankyou.appfoundation.ui

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import com.thankyou.appfoundation.R
import kotlinx.android.synthetic.main.layout_toast.view.*

object ToastUtils {

    fun showSuccess(context: Context, message: String) {
        showToast(
            context,
            R.drawable.bg_toast_success,
            R.color.color_white,
            R.drawable.ic_toast_success,
            message
        )
    }

    fun showPending(context: Context, message: String) {
        showToast(
            context,
            R.drawable.bg_toast_pending,
            R.color.color_white,
            R.drawable.ic_toast_pending,
            message
        )
    }

    fun showError(context: Context, message: String) {
        showToast(
            context,
            R.drawable.bg_toast_error,
            R.color.color_white,
            R.drawable.ic_toast_error,
            message
        )
    }

    private fun showToast(
        context: Context,
        @DrawableRes background: Int,
        textColor: Int,
        image: Int,
        message: String
    ) {

        val toastView = LayoutInflater
            .from(context)
            .inflate(R.layout.layout_toast, null)

        toastView.findViewById<LinearLayout>(R.id.divToast).apply {
            ViewCompat.setBackground(this, ContextCompat.getDrawable(context, background))
            ivToastImage.setImageDrawable(ContextCompat.getDrawable(context, image))
            tvToastMessage.text = message
            tvToastMessage.setTextColor(ContextCompat.getColor(context, textColor))
        }

        val toast = Toast(context).apply {
            duration = Toast.LENGTH_SHORT
            view = toastView
            setGravity(Gravity.TOP or Gravity.FILL_HORIZONTAL, 0, 0)
        }

        toast.show()
    }
}
